package work.connor.zorcs.server.grids.quadhex;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.provider.Arguments;
import work.connor.zorcs.server.grids.common.Sys;
import work.connor.zorcs.server.grids.flat.Angle;
import work.connor.zorcs.server.grids.flat.FlatGridTest;

import java.util.stream.Stream;

@DisplayName("In a ?:QuadHexGrid grid,")
public abstract class QuadHexGridTest<G extends QuadHexGrid<G,CC,GC,FC,A>,
                                      CC extends CellCoord<G,CC,GC,FC,A>,
                                      GC extends GapCoord<G,CC,GC,FC,A>,
                                      FC extends FCoord<G,CC,GC,FC,A>,
                                      A  extends Angle<G,CC,GC,FC,A>,
                                      Sy extends Sys<G,Sy,CC,GC,FC,A,A>>
        extends FlatGridTest<G,CC,GC,FC,A,Sy>
        implements QuadHexGrid.Based<G,CC,GC,FC,A> {
    @Override protected Stream<? extends CC> cellCoords() {
        return Stream.of(g().cellCoord(7,3), g().cellCoord(0,0), g().cellCoord(8,2));
    }
    @Override protected Stream<? extends GC> gapCoords() {
        return Stream.of(g().gapCoord(-4,5), g().gapCoord(1,0), g().gapCoord(3, -2));
    }
    @Override protected Stream<Arguments> cellCoordTranslationResultVecs() {
        return Stream.of(
                Arguments.of(g().cellCoord(-2,1),g().cellCoord(5,-3), g().cellCoord(3,-2))
        );
    }
    @Override protected Stream<Arguments> gapCoordTranslationResultVecs() {
        return Stream.of(
                Arguments.of(g().gapCoord(-2,1), g().cellCoord(5,-3), g().gapCoord(8,-5))
        );
    }
}