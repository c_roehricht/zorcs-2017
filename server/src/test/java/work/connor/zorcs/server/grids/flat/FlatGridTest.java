package work.connor.zorcs.server.grids.flat;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.TestInstance;
import work.connor.zorcs.server.grids.common.CommonGridTest;
import work.connor.zorcs.server.grids.common.FCoord;
import work.connor.zorcs.server.grids.common.Sys;

import java.util.stream.Stream;

@DisplayName("In a ?:FlatGrid grid,") @TestInstance(TestInstance.Lifecycle.PER_CLASS)
public abstract class FlatGridTest<G  extends FlatGrid<G,CC,GC,FC,A>,
                                   CC extends CellCoord<G,CC,GC,FC,A>,
                                   GC extends GapCoord<G,CC,GC,FC,A>,
                                   FC extends FCoord<G,CC,GC,FC,A,A>,
                                   A  extends Angle<G,CC,GC,FC,A>,
                                   Sy extends Sys<G,Sy,CC,GC,FC,A,A>>
        extends CommonGridTest<G,CC,GC,FC,A,A,Sy>
        implements FlatGrid.Based<G,CC,GC,FC,A> {
}