package work.connor.zorcs.server.grid.general;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

@DisplayName("directions") @TestInstance(TestInstance.Lifecycle.PER_CLASS)
public abstract class DirectionTest<G extends Grid<G>,
                                    D extends Direction<G,D>> {
    @DisplayName("as rotatables") @Nested @TestInstance(TestInstance.Lifecycle.PER_CLASS)
    class AsRotatables extends RotatableTest<G,D> {
        @Override protected Stream<? extends D> rotatables() {return directions();}
        @Override protected Stream<? extends Orientation<G,?>> orientations() {
            return DirectionTest.this.orientations();
        }
        @Override protected Stream<Arguments> rotationResultVecs() {
            return DirectionTest.this.rotationResultVecs();
        }
    }
    @DisplayName("as spatial Values") @Nested @TestInstance(TestInstance.Lifecycle.PER_CLASS)
    class AsSpatialValues extends SpatialValueTest<G,D> {
        @Override protected Stream<? extends D> values() {return directions();}
        @Override protected Stream<? extends Sys<G,?>> systems() {return DirectionTest.this.systems();}
    }
    @DisplayName("are the opposite of their opposite") @ParameterizedTest @MethodSource("directions")
    void areTheOppositeOfTheirOpposite(D direction) {
        assertEquals(direction, direction.opposite().opposite());
    }
    protected abstract Stream<? extends D> directions();
    protected abstract Stream<? extends Orientation<G,?>> orientations();
    protected abstract Stream<? extends Sys<G,?>> systems();
    protected abstract Stream<Arguments> rotationResultVecs();
}