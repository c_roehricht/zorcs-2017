package work.connor.zorcs.server.grid.general;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.params.provider.Arguments;

import java.util.stream.Stream;

@DisplayName("gap coordinates")
public abstract class GapCoordTest<G   extends Grid<G>,
                                   GC  extends GapCoord<G,GC>> {
    @DisplayName("as translatables") @Nested @TestInstance(TestInstance.Lifecycle.PER_CLASS)
    class AsTranslatables extends TranslatableTest<G,GC> {
        @Override protected Stream<? extends GC> translatables() {return gapCoords();}
        @Override protected Stream<? extends CellCoord<G,?>> cellCoords() {
            return GapCoordTest.this.cellCoords();
        }
        @Override protected Stream<Arguments> translationResultVecs() {
            return GapCoordTest.this.translationResultVecs();
        }
    }
    @DisplayName("as rotatables") @Nested @TestInstance(TestInstance.Lifecycle.PER_CLASS)
    class AsRotatables extends RotatableTest<G,GC> {
        @Override protected Stream<? extends GC> rotatables() {return gapCoords();}
        @Override protected Stream<? extends Orientation<G,?>> orientations() {return GapCoordTest.this.orientations();}
        @Override protected Stream<Arguments> rotationResultVecs() {
            return GapCoordTest.this.rotationResultVecs();
        }
    }
    @DisplayName("as spatial values") @Nested @TestInstance(TestInstance.Lifecycle.PER_CLASS)
    class AsSpatialValues extends SpatialValueTest<G,GC> {
        @Override protected Stream<? extends GC> values() {return gapCoords();}
        @Override protected Stream<? extends Sys<G,?>> systems() {return GapCoordTest.this.systems();}
    }
    protected abstract Stream<? extends GC> gapCoords();
    protected abstract Stream<? extends CellCoord<G,?>> cellCoords();
    protected abstract Stream<? extends Orientation<G,?>> orientations();
    protected abstract Stream<? extends Sys<G,?>> systems();
    protected abstract Stream<Arguments> translationResultVecs();
    protected abstract Stream<Arguments> rotationResultVecs();
}