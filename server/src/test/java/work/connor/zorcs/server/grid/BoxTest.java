package work.connor.zorcs.server.grid;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import work.connor.zorcs.server.utility.CompU;

import java.util.Comparator;
import java.util.function.IntFunction;
import java.util.stream.Stream;

@DisplayName("the ? box") @TestInstance(TestInstance.Lifecycle.PER_CLASS)
public abstract class BoxTest<G extends Grid<G>, B> {
    @DisplayName("is ordered") @Test
    void IsOrdered() {assert(CompU.<B>isInOrder(box().apply(size()).iterator(), comparator()));}
    protected abstract IntFunction<Stream<? extends B>> box();
    protected abstract Comparator<? super B> comparator();
    protected abstract int size();
}