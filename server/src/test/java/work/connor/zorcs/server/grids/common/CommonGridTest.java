package work.connor.zorcs.server.grids.common;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.params.provider.Arguments;
import work.connor.zorcs.server.grid.BoxTest;
import work.connor.zorcs.server.grid.general.CellCoordTest;
import work.connor.zorcs.server.grid.general.DirectionTest;
import work.connor.zorcs.server.grid.general.GapCoordTest;
import work.connor.zorcs.server.grid.general.GridTest;
import work.connor.zorcs.server.utility.StreamU;

import java.util.Comparator;
import java.util.function.IntFunction;
import java.util.stream.Stream;

@DisplayName("In a ?:CommonGrid grid,") @TestInstance(TestInstance.Lifecycle.PER_CLASS)
public abstract class CommonGridTest<G  extends CommonGrid<G,CC,GC,FC,D,O>,
                                     CC extends CellCoord<G,CC,GC,FC,D,O>,
                                     GC extends GapCoord<G,CC,GC,FC,D,O>,
                                     FC extends FCoord<G,CC,GC,FC,D,O>,
                                     D  extends Direction<G,CC,GC,FC,D,O>,
                                     O  extends Orientation<G,CC,GC,FC,D,O>,
                                     Sy extends Sys<G,Sy,CC,GC,FC,D,O>>
        extends GridTest<G,CC,GC,FC,D,O,Sy>
        implements CommonGrid.Based<G,CC,GC,FC,D,O> {
    @DisplayName("the euclidean box") @Nested @TestInstance(TestInstance.Lifecycle.PER_CLASS)
    class EuclideanBoxTest extends BoxTest<G,CC> {
        @DisplayName("lies inside the chebyshev box") @Test
        void LiesInsideTheChebyshevBox() {
            int size = Math.min(size(), chebyshevBoxSize());
            assert(StreamU.containsAll(g().chebyshevBox(size), box().apply(size)));
        }
        @Override protected IntFunction<Stream<? extends CC>> box() {return g()::euclideanBox;}
        @Override protected Comparator<? super CC> comparator() {
            return Comparator.comparingDouble(CC::euclidean);
        }
        @Override protected int size() {return euclideanBoxSize();}
    }
    @DisplayName("the manhattan box") @Nested @TestInstance(TestInstance.Lifecycle.PER_CLASS)
    class ManhattanBoxTest extends BoxTest<G,CC> {
        @DisplayName("lies inside the euclidean box") @Test
        void LiesInsideTheEuclideanBox() {
            int size = Math.min(size(), euclideanBoxSize());
            assert(StreamU.containsAll(g().euclideanBox(size), box().apply(size)));
        }
        @Override protected IntFunction<Stream<? extends CC>> box() {return g()::manhattanBox;}
        @Override protected Comparator<? super CC> comparator() {
            return Comparator.comparingInt(CC::manhattan);
        }
        @Override protected int size() {return manhattanBoxSize();}
    }
    @DisplayName("the chebyshev box") @Nested @TestInstance(TestInstance.Lifecycle.PER_CLASS)
    class ChebyshevBoxTest extends BoxTest<G,CC> {
        @Override protected IntFunction<Stream<? extends CC>> box() {return g()::chebyshevBox;}
        @Override protected Comparator<? super CC> comparator() {
            return Comparator.comparingInt(CC::chebyshev);
        }
        @Override protected int size() {return chebyshevBoxSize();}
    }
    @DisplayName("the euclidean coordinate box") @Nested @TestInstance(TestInstance.Lifecycle.PER_CLASS)
    class EuclideanCoordBoxTest extends BoxTest<G,LocationCoord<G,?,CC,GC,FC,D,O>> {
        @DisplayName("lies inside the chebyshev coordinate box") @Test
        void LiesInsideTheChebyshevCoordBox() {
            int size = Math.min(size(), chebyshevCoordBoxSize());
            assert(StreamU.containsAll(g().chebyshevCoordBox(size), box().apply(size)));
        }
        @Override protected IntFunction<Stream<? extends LocationCoord<G,?,CC,GC,FC,D,O>>> box() {
            return g()::euclideanCoordBox;
        }
        @Override protected Comparator<? super LocationCoord<G,?,CC,GC,FC,D,O>> comparator() {
            return Comparator.comparingDouble(LocationCoord::euclidean);
        }
        @Override protected int size() {return euclideanCoordBoxSize();}
    }
    @DisplayName("the manhattan coordinate box") @Nested @TestInstance(TestInstance.Lifecycle.PER_CLASS)
    class ManhattanCoordBoxTest extends BoxTest<G,LocationCoord<G,?,CC,GC,FC,D,O>> {
        @DisplayName("lies inside the euclidean coordinate box") @Test
        void LiesInsideTheEuclideanCoordBox() {
            int size = Math.min(size(), euclideanCoordBoxSize());
            assert(StreamU.containsAll(g().euclideanCoordBox(size), box().apply(size)));
        }
        @Override protected IntFunction<Stream<? extends LocationCoord<G,?,CC,GC,FC,D,O>>> box() {
            return g()::manhattanCoordBox;
        }
        @Override protected Comparator<? super LocationCoord<G,?,CC,GC,FC,D,O>> comparator() {
            return Comparator.comparingDouble(LocationCoord::floatingManhattan);
        }
        @Override protected int size() {return manhattanCoordBoxSize();}
    }
    @DisplayName("the chebyshev coordinate box") @Nested @TestInstance(TestInstance.Lifecycle.PER_CLASS)
    class ChebyshevCoordBoxTest extends BoxTest<G,LocationCoord<G,?,CC,GC,FC,D,O>> {
        @Override protected IntFunction<Stream<? extends LocationCoord<G,?,CC,GC,FC,D,O>>> box() {
            return g()::chebyshevCoordBox;
        }
        @Override protected Comparator<? super LocationCoord<G,?,CC,GC,FC,D,O>> comparator() {
            return Comparator.comparingDouble(LocationCoord::floatingChebyshev);
        }
        @Override protected int size() {return chebyshevCoordBoxSize();}
    }
    @DisplayName("cell coordinates") @Nested @TestInstance(TestInstance.Lifecycle.PER_CLASS)
    class CellCoordTest_ extends CellCoordTest<G,CC> {
        @Override protected Stream<? extends CC> cellCoords() {return CommonGridTest.this.cellCoords();}
        @Override protected Stream<? extends O> orientations() {return CommonGridTest.this.orientations();}
        @Override protected Stream<? extends Sy> systems() {return CommonGridTest.this.systems();}
        @Override protected Stream<Arguments> translationResultVecs() {return cellCoordTranslationResultVecs();}
        @Override protected Stream<Arguments> rotationResultVecs() {return cellCoordRotationResultVecs();}
    }
    @DisplayName("gap coordinates") @Nested @TestInstance(TestInstance.Lifecycle.PER_CLASS)
    class GapCoordTest_ extends GapCoordTest<G,GC> {
        @Override protected Stream<? extends GC> gapCoords() {return CommonGridTest.this.gapCoords();}
        @Override protected Stream<? extends CC> cellCoords() {return CommonGridTest.this.cellCoords();}
        @Override protected Stream<? extends O> orientations() {return CommonGridTest.this.orientations();}
        @Override protected Stream<? extends Sy> systems() {return CommonGridTest.this.systems();}
        @Override protected Stream<Arguments> translationResultVecs() {return gapCoordTranslationResultVecs();}
        @Override protected Stream<Arguments> rotationResultVecs() {return gapCoordRotationResultVecs();}
    }
    @DisplayName("directions") @Nested @TestInstance(TestInstance.Lifecycle.PER_CLASS)
    class DirectionTest_ extends DirectionTest<G,D> {
        @Override protected Stream<? extends D> directions() {return CommonGridTest.this.directions();}
        @Override protected Stream<? extends O> orientations() {return CommonGridTest.this.orientations();}
        @Override protected Stream<? extends Sy> systems() {return CommonGridTest.this.systems();}
        @Override protected Stream<Arguments> rotationResultVecs() {return directionRotationResultVecs();}
    }
    protected int euclideanBoxSize() {return 20;}
    protected int manhattanBoxSize() {return 20;}
    protected int chebyshevBoxSize() {return 20;}
    protected int euclideanCoordBoxSize() {return 20;}
    protected int manhattanCoordBoxSize() {return 20;}
    protected int chebyshevCoordBoxSize() {return 20;}
    @Override protected Stream<? extends O> orientations() {return g().orientations().stream();}
    protected Stream<? extends D> directions() {return g().directions().stream();}
    protected abstract Stream<? extends GC> gapCoords();
    protected abstract Stream<Arguments> cellCoordTranslationResultVecs();
    protected abstract Stream<Arguments> cellCoordRotationResultVecs();
    protected abstract Stream<Arguments> gapCoordTranslationResultVecs();
    protected abstract Stream<Arguments> gapCoordRotationResultVecs();
    protected abstract Stream<Arguments> directionRotationResultVecs();
}