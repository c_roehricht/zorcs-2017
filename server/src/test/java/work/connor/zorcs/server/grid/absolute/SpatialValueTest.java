package work.connor.zorcs.server.grid.absolute;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import work.connor.zorcs.server.utility.TestU;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

@DisplayName("spatial values") @TestInstance(TestInstance.Lifecycle.PER_CLASS)
public abstract class SpatialValueTest<G  extends Grid<G>,
                                       V  extends SpatialValue<G,V>> {
    @DisplayName("are the absolute version of their relative version") @ParameterizedTest @MethodSource("vecs")
    void AreTheAbsoluteVersionOfTheirRelativeVersion(V v, Sys<G,?> system) {
        work.connor.zorcs.server.grid.relative.SpatialValue<?,?> vr = v.toRelative(system);
        assertEquals(v, vr.toAbsolute(system));
    }
    private Stream<Arguments> vecs() {return TestU.combine(values(), systems());}
    protected abstract Stream<? extends V> values();
    protected abstract Stream<? extends Sys<G,?>> systems();
}