package work.connor.zorcs.server.grid.general;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.TestInstance;

import java.util.stream.Stream;

@DisplayName("spatial values") @TestInstance(TestInstance.Lifecycle.PER_CLASS)
public abstract class SpatialValueTest<G extends Grid<G>,
                                       V extends SpatialValue<G,V>> {
    @DisplayName("as relative values") @Nested @TestInstance(TestInstance.Lifecycle.PER_CLASS)
    class AsRelativeValues extends work.connor.zorcs.server.grid.relative.SpatialValueTest<G,G,V> {
        @Override protected Stream<? extends V> values() {return SpatialValueTest.this.values();}
        @Override protected Stream<? extends Sys<G,?>> systems() {return SpatialValueTest.this.systems();}
    }
    @DisplayName("as absolute values") @Nested @TestInstance(TestInstance.Lifecycle.PER_CLASS)
    class AsAbsoluteValues extends work.connor.zorcs.server.grid.absolute.SpatialValueTest<G,V> {
        @Override protected Stream<? extends V> values() {return SpatialValueTest.this.values();}
        @Override protected Stream<? extends Sys<G,?>> systems() {return SpatialValueTest.this.systems();}
    }
    protected abstract Stream<? extends V> values();
    protected abstract Stream<? extends Sys<G,?>> systems();
}