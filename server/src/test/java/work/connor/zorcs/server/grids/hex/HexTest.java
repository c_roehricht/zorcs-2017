package work.connor.zorcs.server.grids.hex;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.provider.Arguments;
import work.connor.zorcs.server.grids.quadhex.QuadHexGridTest;

import java.util.stream.Stream;

@DisplayName("In a hex grid,")
class HexTest extends QuadHexGridTest<Hex,CellCoord,GapCoord,FCoord,Angle,Sys>
        implements Hex.Based {
    @Override protected Stream<Arguments> cellCoordRotationResultVecs() {
        return Stream.of(
                Arguments.of(g().cellCoord(-2, 1), g().oLeft(), g().cellCoord(-1, 2))
        );
    }
    @Override protected Stream<Arguments> gapCoordRotationResultVecs() {
        return Stream.of(
                Arguments.of(g().cellCoord(-2,1), g().oLeft(), g().cellCoord(-1,2))
        );
    }
    @Override protected Stream<Arguments> directionRotationResultVecs() {
        return Stream.of(
                Arguments.of(g().behind(), g().behind(), g().forward())
        );
    }
}