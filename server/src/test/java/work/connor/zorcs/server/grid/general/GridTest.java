package work.connor.zorcs.server.grid.general;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.TestInstance;
import work.connor.zorcs.server.utility.StreamU;

import java.util.stream.Stream;

@DisplayName("In a ?:general grid,") @TestInstance(TestInstance.Lifecycle.PER_CLASS)
public abstract class GridTest<G   extends Grid<G>,
                               CC  extends CellCoord<G,CC>,
                               GC  extends GapCoord<G,GC>,
                               FC  extends FCoord<G,FC>,
                               D   extends Direction<G,D>,
                               O   extends Orientation<G,O>,
                               Sy  extends Sys<G,Sy>>
        implements Grid.Based<G> {
    @DisplayName("systems") @Nested @TestInstance(TestInstance.Lifecycle.PER_CLASS)
    class SysTest_ extends SysTest<G,Sy> {
        @Override protected Stream<? extends Sy> systems() {return GridTest.this.systems();}
    }
    @SuppressWarnings("unchecked")
    protected Stream<? extends Sy> systems() {
        return StreamU.cartesian(cellCoords(), orientations(), (cc, o) -> (Sy) g().system(cc,o));
    }
    protected abstract Stream<? extends CC> cellCoords();
    protected abstract Stream<? extends O> orientations();
}