package work.connor.zorcs.server.grids.quad;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.provider.Arguments;
import work.connor.zorcs.server.grids.quadhex.QuadHexGridTest;

import java.util.stream.Stream;

@DisplayName("In a quad grid,")
class QuadTest extends QuadHexGridTest<Quad,CellCoord,GapCoord,FCoord,Angle,Sys>
        implements Quad.Based {
    @Override protected Stream<Arguments> cellCoordRotationResultVecs() {
        return Stream.of(
                Arguments.of(g().cellCoord(-2, 1), g().oLeft(), g().cellCoord(1, 2))
        );
    }
    @Override protected Stream<Arguments> gapCoordRotationResultVecs() {
        return Stream.of(
                Arguments.of(g().cellCoord(-2,1), g().oLeft(), g().cellCoord(1,2))
        );
    }
    @Override protected Stream<Arguments> directionRotationResultVecs() {
        return Stream.of(
                Arguments.of(g().behind(), g().behind(), g().forward())
        );
    }
}