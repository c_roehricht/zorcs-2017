package work.connor.zorcs.server.grid.relative;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import work.connor.zorcs.server.grid.absolute.Sys;
import work.connor.zorcs.server.utility.TestU;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

@DisplayName("spatial values") @TestInstance(TestInstance.Lifecycle.PER_CLASS)
public abstract class SpatialValueTest<G  extends Grid<G>,
                                       GA extends work.connor.zorcs.server.grid.absolute.Grid<GA>,
                                       V  extends SpatialValue<G,V>> {
    @SuppressWarnings("unchecked")
    @DisplayName("are the relative version of their absolute version") @ParameterizedTest @MethodSource("vecs")
    void AreTheRelativeVersionOfTheirAbsoluteVersion(V v, Sys<GA,?> system) {
        work.connor.zorcs.server.grid.absolute.SpatialValue<GA,?> va =
                (work.connor.zorcs.server.grid.absolute.SpatialValue<GA, ?>) v.toAbsolute(system);
        assertEquals(v, va.toRelative(system));
    }
    private Stream<Arguments> vecs() {return TestU.combine(values(), systems());}
    protected abstract Stream<? extends V> values();
    protected abstract Stream<? extends Sys<GA,?>> systems();
}