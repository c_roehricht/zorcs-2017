package work.connor.zorcs.server.grids.cube;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.provider.Arguments;
import work.connor.zorcs.server.grids.common.CommonGridTest;

import java.util.stream.Stream;

@DisplayName("In a cube grid,")
class CubeTest extends CommonGridTest<Cube,CellCoord,GapCoord,FCoord,Direction,Orientation,Sys>
        implements Cube.Based {
    @Override protected Stream<CellCoord> cellCoords() {
        return Stream.of(g().cellCoord(-1,5,8), g().cellCoord(0,0,0), g().cellCoord(3,-2,-10));
    }
    @Override protected Stream<GapCoord> gapCoords() {
        return Stream.of(g().gapCoord(-4,3,2), g().gapCoord(1,0,0), g().gapCoord(22,-8,13));
    }
    @Override protected Stream<Arguments> cellCoordTranslationResultVecs() {
        return Stream.of(
                Arguments.of(g().cellCoord(3, -2,1), g().cellCoord(-7, 5, -3), g().cellCoord(-4, 3, -2))
        );
    }
    @Override protected Stream<Arguments> cellCoordRotationResultVecs() {
        return Stream.of(
                Arguments.of(g().cellCoord(3, -2, 1), Orientation.CLOCKWISE, g().cellCoord(3, 1, 2))
        );
    }
    @Override protected Stream<Arguments> gapCoordTranslationResultVecs() {
        return Stream.of(
                Arguments.of(g().gapCoord(3, -2, 4), g().cellCoord(-7, 5, -3), g().gapCoord(-11, 8, -2))
        );
    }
    @Override protected Stream<Arguments> gapCoordRotationResultVecs() {
        return Stream.of(
                Arguments.of(g().gapCoord(3, -2, 4), Orientation.CLOCKWISE, g().gapCoord(3, 4, 2))
        );
    }
    @Override protected Stream<Arguments> directionRotationResultVecs() {
        return Stream.of(
                Arguments.of(g().behind(), g().oLeft(), g().right())
        );
    }
}