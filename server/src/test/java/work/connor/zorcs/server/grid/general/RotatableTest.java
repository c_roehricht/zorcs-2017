package work.connor.zorcs.server.grid.general;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import work.connor.zorcs.server.utility.TestU;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

@DisplayName("rotatables") @TestInstance(TestInstance.Lifecycle.PER_CLASS)
public abstract class RotatableTest<G extends Grid<G>,
                                    V extends SpatialValue<G,V>> {
    @DisplayName("are rotated") @Nested @TestInstance(TestInstance.Lifecycle.PER_CLASS)
    class AreRotated {
        @DisplayName("onto themselves") @ParameterizedTest @MethodSource("rotationVecs")
        void ontoThemselves(V v, Orientation<G,?> o) {assertEquals(v, v.rotate(o).rotate(o.negate()));}
        @DisplayName("with a known result") @ParameterizedTest @MethodSource("rotationResultVecs")
        void areRotated(V before, Orientation<G,?> o, V after) {assertEquals(after, before.rotate(o));}
        private Stream<Arguments> rotationVecs() {return TestU.combine(rotatables(), orientations());}
        private Stream<Arguments> rotationResultVecs() {return RotatableTest.this.rotationResultVecs();}
    }
    @DisplayName("are mirrored") @Nested @TestInstance(TestInstance.Lifecycle.PER_CLASS)
    class AreMirrored {
        @DisplayName("onto themselves") @ParameterizedTest @MethodSource("rotatables")
        void ontoThemselves(V v) {assertEquals(v, v.mirror().mirror());}
        private Stream<? extends V> rotatables() {return RotatableTest.this.rotatables();}
    }
    protected abstract Stream<? extends V> rotatables();
    protected abstract Stream<? extends Orientation<G,?>> orientations();
    protected abstract Stream<Arguments> rotationResultVecs();
}