package work.connor.zorcs.server.grid.general;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.params.provider.Arguments;

import java.util.stream.Stream;

@DisplayName("cell coordinates")
public abstract class CellCoordTest<G   extends Grid<G>,
                                    CC  extends CellCoord<G,CC>> {
    @DisplayName("as translatables") @Nested @TestInstance(TestInstance.Lifecycle.PER_CLASS)
    class AsTranslatables extends TranslatableTest<G,CC> {
        @Override protected Stream<? extends CC> translatables() {return cellCoords();}
        @Override protected Stream<? extends CC> cellCoords() {return CellCoordTest.this.cellCoords();}
        @Override protected Stream<Arguments> translationResultVecs() {
            return CellCoordTest.this.translationResultVecs();
        }
    }
    @DisplayName("as rotatables") @Nested @TestInstance(TestInstance.Lifecycle.PER_CLASS)
    class AsRotatables extends RotatableTest<G,CC> {
        @Override protected Stream<? extends CC> rotatables() {return cellCoords();}
        @Override protected Stream<? extends Orientation<G,?>> orientations() {
            return CellCoordTest.this.orientations();
        }
        @Override protected Stream<Arguments> rotationResultVecs() {
            return CellCoordTest.this.rotationResultVecs();
        }
    }
    @DisplayName("as spatial values") @Nested @TestInstance(TestInstance.Lifecycle.PER_CLASS)
    class AsSpatialValues extends SpatialValueTest<G,CC> {
        @Override protected Stream<? extends CC> values() {return cellCoords();}
        @Override protected Stream<? extends Sys<G,?>> systems() {return CellCoordTest.this.systems();}
    }
    protected abstract Stream<? extends CC> cellCoords();
    protected abstract Stream<? extends Orientation<G,?>> orientations();
    protected abstract Stream<? extends Sys<G,?>> systems();
    protected abstract Stream<Arguments> translationResultVecs();
    protected abstract Stream<Arguments> rotationResultVecs();
}