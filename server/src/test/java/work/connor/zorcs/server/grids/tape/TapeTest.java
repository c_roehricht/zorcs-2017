package work.connor.zorcs.server.grids.tape;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.provider.Arguments;
import work.connor.zorcs.server.grids.flat.FlatGridTest;

import java.util.stream.Stream;

@DisplayName("In a tape grid,")
class TapeTest extends FlatGridTest<Tape,CellCoord,GapCoord,FCoord,Angle,Sys>
        implements Tape.Based {
    @Override protected Stream<? extends CellCoord> cellCoords() {
        return Stream.of(new CellCoord(-7), new CellCoord(0), new CellCoord(20));
    }
    @Override protected Stream<? extends GapCoord> gapCoords() {
        return Stream.of(new GapCoord(new CellCoord(-1)), new GapCoord(new CellCoord(1)), new GapCoord(new CellCoord(3)));
    }
    @Override protected Stream<Arguments> cellCoordTranslationResultVecs() {
        return Stream.of(
                Arguments.of(new CellCoord(1), new CellCoord(-3), new CellCoord(-2))
        );
    }
    @Override protected Stream<Arguments> cellCoordRotationResultVecs() {
        return Stream.of(
                Arguments.of(new CellCoord(-2), g().behind(), new CellCoord(2))
        );
    }
    @Override protected Stream<Arguments> gapCoordTranslationResultVecs() {
        return Stream.of(
                Arguments.of(new GapCoord(new CellCoord(1)), new CellCoord(-3), new GapCoord(new CellCoord(-5)))
        );
    }
    @Override protected Stream<Arguments> gapCoordRotationResultVecs() {
        return Stream.of(
                Arguments.of(new GapCoord(new CellCoord(-3)), g().behind(), new GapCoord(new CellCoord(3)))
        );
    }
    @Override protected Stream<Arguments> directionRotationResultVecs() {
        return Stream.of(
                Arguments.of(g().behind(), g().behind(), g().forward())
        );
    }
}