package work.connor.zorcs.server.grid.general;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import work.connor.zorcs.server.utility.TestU;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

@DisplayName("translatables") @TestInstance(TestInstance.Lifecycle.PER_CLASS)
public abstract class TranslatableTest<G extends Grid<G>,
                                       V extends SpatialValue<G,V>> {
    @DisplayName("are translated") @Nested @TestInstance(TestInstance.Lifecycle.PER_CLASS)
    class AreTranslated {
        @DisplayName("onto themselves") @ParameterizedTest @MethodSource("translationVecs")
        void ontoThemselves(V v, CellCoord<G,?> cc) { assertEquals(v, v.translate(cc).translate(cc.negate()));}
        @DisplayName("with a known result") @ParameterizedTest @MethodSource("translationResultVecs")
        void areTranslated(V before, CellCoord<G,?> cc, V after) {assertEquals(after, before.translate(cc));}
        private Stream<Arguments> translationVecs() {return TestU.combine(translatables(), cellCoords());}
        private Stream<Arguments> translationResultVecs() {return TranslatableTest.this.translationResultVecs();}
    }
    protected abstract Stream<? extends V> translatables();
    protected abstract Stream<? extends CellCoord<G,?>> cellCoords();
    protected abstract Stream<Arguments> translationResultVecs();
}