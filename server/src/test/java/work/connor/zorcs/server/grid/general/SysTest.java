package work.connor.zorcs.server.grid.general;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

@DisplayName("systems") @TestInstance(TestInstance.Lifecycle.PER_CLASS)
public abstract class SysTest<G  extends Grid<G>,
                              Sy extends Sys<G,Sy>> {
    @DisplayName("are the negation of their negation") @ParameterizedTest @MethodSource("systems")
    void areTheNegationOfTheirNegation(Sy system) {assertEquals(system, system.negate().negate());}
    protected abstract Stream<? extends Sy> systems();
}