package work.connor.zorcs.server.grids.quad;

import work.connor.zorcs.server.grids.quadhex.QuadHexGrid;

public interface Quad extends QuadHexGrid<Quad,CellCoord,GapCoord,FCoord,Angle> {
    Quad QUAD = new Quad() {};
    interface Based extends QuadHexGrid.Based<Quad,CellCoord,GapCoord,FCoord,Angle> {
        @Override default Quad g() {return QUAD;}
    }
    @Override default CellCoord cellCoord(int a, int b) {return new CellCoord(a,b);}
    @Override default FCoord fCoord(double a, double b) {return new FCoord(a,b);}
    @Override default Angle angle(int r) {return new Angle(r);}
    @Override default int circle() {return 4;}
    @Override default GapCoord gapCoord(CellCoord cc) {return new GapCoord(cc);}
    @Override default boolean gInvariant(CellCoord cc) {return (cc.a % 2 == 0) ^ (cc.b % 2 == 0);}
    @Override default Sys system(CellCoord cc, Angle a) {return new Sys(cc,a);}
}
