package work.connor.zorcs.server.game.protocol;

import org.json.JSONObject;

public interface RemoteOutput {
    default JSONObject toRemote(JSONObject output) {
        if (output == null) return new JSONObject();
        return output;
    }
    default JSONObject put(JSONObject output, String key, RemoteOutput obj) {
        if (obj != null) output.put(key, obj.toRemote(null));
        return output;
    }
    default JSONObject put(JSONObject output, String key, JSONObject obj) {
        if (obj != null) output.put(key, obj);
        return output;
    }
}
