package work.connor.zorcs.server.game.actor;

import work.connor.zorcs.server.game.intent.Intent;
import work.connor.zorcs.server.game.space.Space;
import work.connor.zorcs.server.grid.general.Grid;

import java.util.function.Consumer;

public interface Actor<G extends Grid<G>> {
    void act(Space<G> space, Consumer<Intent<G>> intents);
}
