package work.connor.zorcs.server.game.action;

import work.connor.zorcs.server.grid.general.Grid;
import work.connor.zorcs.server.game.space.Space;

public interface Action<G extends Grid<G>> {
    void perform(Space<G> space) throws ActionFailedException;
}
