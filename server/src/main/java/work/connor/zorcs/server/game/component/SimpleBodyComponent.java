package work.connor.zorcs.server.game.component;

import work.connor.zorcs.server.game.emitter.LocalEmitter;
import work.connor.zorcs.server.game.event.Event;
import work.connor.zorcs.server.game.event.VisualEvent;
import work.connor.zorcs.server.game.space.Cell;
import work.connor.zorcs.server.game.space.LocalObserver;
import work.connor.zorcs.server.grid.general.Grid;
import work.connor.zorcs.server.grid.absolute.*;

import java.util.function.Consumer;

public class SimpleBodyComponent<G extends Grid<G>> extends DefaultComponent<G>
        implements BodyComponent<G>,
                   EmitterComponent<G>,
                   LocalEmitter<G> {
    private Cell<G> cell;
    private String appearance;
    public SimpleBodyComponent(Cell<G> cell, Orientation<G,?> o, String appearance) {
        super();
        setCell(cell);
        setOrientation(o);
        this.appearance = appearance;
    }
    @Override public Cell<G> getBodyCell() {return cell;}
    @Override public void setBodyCell(Cell<G> cell) {this.cell = cell;}
    @Override public void getLocalEvents(Consumer<Event<G>> events) {
        events.accept(new VisualEvent<G>(getOrientation(),"body",appearance,null));
    }
    @Override public LocalObserver<G> move(work.connor.zorcs.server.grid.relative.CellCoord<G,?> coord) {
        //TODO emit visual event
        LocalObserver<G> result = BodyComponent.super.move(coord);
        //TODO emit visual event
        return result;
    }
}
