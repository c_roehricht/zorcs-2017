package work.connor.zorcs.server.game.emitter;

import work.connor.zorcs.server.game.event.Event;
import work.connor.zorcs.server.game.space.Location;
import work.connor.zorcs.server.game.space.Space;
import work.connor.zorcs.server.grid.general.Grid;

import java.util.function.Consumer;

public interface LocalEmitter<G extends Grid<G>>
        extends Emitter<G>,
                Location.Based<G>
{
    default void getLocalEvents(Consumer<Event<G>> events) {}
    @Override default void emitEvents(Space<G> space) {getLocalEvents(getLocation()::addEvent);}
}
