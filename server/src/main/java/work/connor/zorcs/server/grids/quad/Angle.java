package work.connor.zorcs.server.grids.quad;

public class Angle extends work.connor.zorcs.server.grids.flat.Angle<Quad,CellCoord,GapCoord,FCoord,Angle>
        implements SpatialValue<Angle> {
    public Angle(int r) {super(r);}
}
