package work.connor.zorcs.server.grid;

public interface Direction<G extends Grid<G>,
                           D extends Direction<G,D>>
        extends SpatialValue<G,D> {
    D opposite();
    CellCoord<G,?> unit();
    GapCoord<G,?> unitGap();
}
