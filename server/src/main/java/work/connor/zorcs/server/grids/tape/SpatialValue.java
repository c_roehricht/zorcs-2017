package work.connor.zorcs.server.grids.tape;

public interface SpatialValue<V extends SpatialValue<V>>
        extends work.connor.zorcs.server.grids.flat.SpatialValue<Tape,V,CellCoord,GapCoord,FCoord,Angle>,
                Tape.Based {
}
