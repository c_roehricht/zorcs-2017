package work.connor.zorcs.server.grid;

public interface GapCoord<G  extends Grid<G>,
                          GC extends GapCoord<G,GC>>
        extends LocationCoord<G,GC> {
    @Override default FCoord<G,?> multiply(int factor) {return floating().multiply(factor);}
}
