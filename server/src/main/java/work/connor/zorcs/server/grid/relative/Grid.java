package work.connor.zorcs.server.grid.relative;

import org.json.JSONObject;

public interface Grid<G extends Grid<G>>
        extends work.connor.zorcs.server.grid.Grid<G> {
    interface Based<G extends Grid<G>> extends work.connor.zorcs.server.grid.Grid.Based<G> {}
    @Override Direction<G,?> forward();
    CellCoord<G,?> cellCoord(JSONObject input);
    GapCoord<G,?> gapCoord(JSONObject input);
    Direction<G,?> direction(JSONObject input);
    Orientation<G,?> orientation(JSONObject input);
}
