package work.connor.zorcs.server.grids.quad;

public class GapCoord
        extends work.connor.zorcs.server.grids.quadhex.GapCoord<Quad,CellCoord,GapCoord,FCoord,Angle>
        implements LocationCoord<GapCoord> {
    public GapCoord(CellCoord cellCoord) {super(cellCoord);}
}
