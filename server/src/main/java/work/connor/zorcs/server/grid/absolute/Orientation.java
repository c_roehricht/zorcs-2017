package work.connor.zorcs.server.grid.absolute;

public interface Orientation<G extends Grid<G>,
                             O extends Orientation<G,O>>
        extends work.connor.zorcs.server.grid.Orientation<G,O>,
                SpatialValue<G,O> {
    @Override work.connor.zorcs.server.grid.relative.Orientation<?,?> toRelative(Sys<G,?> sy);
}
