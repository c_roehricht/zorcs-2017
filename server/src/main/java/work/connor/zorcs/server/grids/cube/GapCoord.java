package work.connor.zorcs.server.grids.cube;

public class GapCoord
        extends work.connor.zorcs.server.grids.common.GapCoord<Cube,CellCoord,GapCoord,FCoord,Direction,Orientation>
        implements LocationCoord<GapCoord> {
    public GapCoord(CellCoord cellCoord) {super(cellCoord);}
}
