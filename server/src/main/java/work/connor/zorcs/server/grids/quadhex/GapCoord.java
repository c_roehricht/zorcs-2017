package work.connor.zorcs.server.grids.quadhex;

import work.connor.zorcs.server.grids.flat.*;

public abstract class GapCoord<G  extends QuadHexGrid<G,CC,GC,FC,A>,
                               CC extends CellCoord<G,CC,GC,FC,A>,
                               GC extends GapCoord<G,CC,GC,FC,A>,
                               FC extends FCoord<G,CC,GC,FC,A>,
                               A  extends Angle<G,CC,GC,FC,A>>
        extends work.connor.zorcs.server.grids.flat.GapCoord<G,CC,GC,FC,A>
        implements LocationCoord<G,GC,CC,GC,FC,A> {
    protected GapCoord(CC cc) {super(cc);}
}
