package work.connor.zorcs.server.game.component;

import org.json.JSONObject;
import work.connor.zorcs.server.game.action.Action;
import work.connor.zorcs.server.game.action.ActionFailedException;
import work.connor.zorcs.server.game.intent.BrainIntent;
import work.connor.zorcs.server.game.intent.Intent;
import work.connor.zorcs.server.game.stimulus.ActionFailedStimulus;
import work.connor.zorcs.server.game.stimulus.ActionStimulus;
import work.connor.zorcs.server.grid.general.Grid;

import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;

public abstract class DefaultActionComponent<G extends Grid<G>>
        extends DefaultBrainMappedComponent<G,ActionStimulus<G>>
        implements ActionComponent<G> {
    private List<String> actionNames;
    public DefaultActionComponent(BrainComponent<G,? super ActionStimulus<G>> brainComp, String... actionNames) {
        super(brainComp);
        this.actionNames = Arrays.asList(actionNames);
    }
    @Override public List<String> getActionNames() {return actionNames;}
    @Override public void act(Consumer<Intent<G>> intents, JSONObject command) {
        try {
            Action<G> action = makeAction(command);
            intents.accept(new BrainIntent<>(getBrainComp(), action, command));
        } catch (ActionFailedException e) {
            getBrainComp().accept(new ActionFailedStimulus<>(command, e));
        }
    }
    protected abstract Action<G> makeAction(JSONObject command) throws ActionFailedException;
}
