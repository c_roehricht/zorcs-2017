package work.connor.zorcs.server.utility;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Arrays;

public class IntMatrix3 {
    public static final IntMatrix3 identity = new IntMatrix3(1, 0, 0,
                                                             0, 1, 0,
                                                             0, 0, 1);

    private final int[][] values = new int[3][3]; // [Row][Col]
    private IntMatrix3() {}
    public IntMatrix3(int a00, int a01, int a02,
                      int a10, int a11, int a12,
                      int a20, int a21, int a22) {
        values[0][0] = a00; values[0][1] = a01; values[0][2] = a02;
        values[1][0] = a10; values[1][1] = a11; values[1][2] = a12;
        values[2][0] = a20; values[2][1] = a21; values[2][2] = a22;
    }
    public IntMatrix3(JSONArray input) {
        this(input.getInt(0),input.getInt(1),input.getInt(2),
             input.getInt(3),input.getInt(4),input.getInt(5),
             input.getInt(6),input.getInt(7),input.getInt(8));
    }
    public IntMatrix3 transpose() {
        return new IntMatrix3(values[0][0], values[1][0], values[2][0],
                              values[0][1], values[1][1], values[2][1],
                              values[0][2], values[1][2], values[2][2]);
    }

    public IntMatrix3 multiply(IntMatrix3 other) {
        IntMatrix3 result = new IntMatrix3();
        for (int row = 0; row < 3; row++) {
            for (int col = 0; col < 3; col++) {
                result.values[row][col] = 0;
                for (int i = 0; i < 3; i++) {
                    result.values[row][col] += values[row][i] * other.values[i][col];
                }
            }
        }
        return result;
    }

    public int[] multiply(int v0, int v1, int v2) {
        int[] result = new int[3];
        for (int i = 0; i < 3; i++) {
            result[i] = values[i][0] * v0
                      + values[i][1] * v1
                      + values[i][2] * v2;
        }
        return result;
    }

    public double[] multiply(double v0, double v1, double v2) {
        double[] result = new double[3];
        for (int i = 0; i < 3; i++) {
            result[i] = values[i][0] * v0
                      + values[i][1] * v1
                      + values[i][2] * v2;
        }
        return result;
    }

    @Override public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        IntMatrix3 that = (IntMatrix3) o;
        return Arrays.deepEquals(values, that.values);
    }
    @Override public int hashCode() {return Arrays.deepHashCode(values);}
    @Override public String toString() {
        return "{" + Arrays.toString(values[0])
                   + Arrays.toString(values[1])
                   + Arrays.toString(values[2])+ "}";
    }
    public JSONArray toJSONArray() {
        JSONArray array = new JSONArray();
        array.put(values[0][0]);array.put(values[0][1]);array.put(values[0][2]);
        array.put(values[1][0]);array.put(values[1][1]);array.put(values[1][2]);
        array.put(values[2][0]);array.put(values[2][1]);array.put(values[2][2]);
        return array;
    }
}
