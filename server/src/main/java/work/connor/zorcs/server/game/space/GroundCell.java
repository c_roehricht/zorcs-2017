package work.connor.zorcs.server.game.space;

import work.connor.zorcs.server.game.emitter.LocalEmitter;
import work.connor.zorcs.server.game.event.Event;
import work.connor.zorcs.server.game.event.VisualEvent;
import work.connor.zorcs.server.grid.general.Grid;
import work.connor.zorcs.server.grid.absolute.CellCoord;

import java.util.function.Consumer;

public class GroundCell<G extends Grid<G>> extends DefaultCell<G>
        implements LocalEmitter<G> {
    private String appearance;

    public GroundCell(Space<G> space, CellCoord<G,?> coord, String appearance) {
        super(space, coord);
        this.appearance = appearance;
    }

    @Override public void getLocalEvents(Consumer<Event<G>> events) {
        events.accept(new VisualEvent<>(null,"cell",appearance,null));
    }
    @Override public void installAsCell(Space<G> space, CellCoord<G,?> coord) {
        super.installAsCell(space, coord);
        installAsEmitter(space);
    }
    @Override public void uninstallAsCell() {
        uninstallAsEmitter(getSpace());
        super.uninstallAsCell();
    }

    @Override
    public Location<G> getLocation() {
        return this;
    }
}
