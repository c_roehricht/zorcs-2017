package work.connor.zorcs.server.grids.tape;

public class Angle extends work.connor.zorcs.server.grids.flat.Angle<Tape,CellCoord,GapCoord,FCoord,Angle>
        implements SpatialValue<Angle> {
    public Angle(int r) {super(r);}
}
