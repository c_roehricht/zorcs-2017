package work.connor.zorcs.server.game.action;

import work.connor.zorcs.server.game.component.BodyComponent;
import work.connor.zorcs.server.grid.general.Grid;

public abstract class BodyAction<G extends Grid<G>> implements Action<G>,
                                           BodyComponent.Based<G> {
    protected BodyComponent<G> body;
    protected BodyAction(BodyComponent<G> body) {this.body = body;}
    @Override public BodyComponent<G> getBodyComp() {return body;}
}
