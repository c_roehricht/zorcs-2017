package work.connor.zorcs.server.utility;

import java.util.function.Function;

public class ClassU {
    public static <C> Function<Object,C> as(Class<C> c) {return o -> c.isInstance(o) ? c.cast(o) : null;}
}
