package work.connor.zorcs.server.grids.quad;

public class Sys
        extends work.connor.zorcs.server.grids.common.Sys<Quad,Sys,CellCoord,GapCoord,FCoord,Angle,Angle>
        implements Quad.Based {
    public Sys(CellCoord cc, Angle a) {super(cc, a);}
}
