package work.connor.zorcs.server.grid.general;

import java.util.Set;
import java.util.stream.Stream;

public interface Grid<G extends Grid<G>>
        extends work.connor.zorcs.server.grid.absolute.Grid<G>,
                work.connor.zorcs.server.grid.relative.Grid<G> {
    interface Based<G extends Grid<G>>
            extends work.connor.zorcs.server.grid.absolute.Grid.Based<G>,
                    work.connor.zorcs.server.grid.relative.Grid.Based<G> {}
    @Override CellCoord<G,?> unitForward();
    @Override Orientation<G,?> noRotation();
    @Override Set<? extends Direction<G,?>> directions();
    @Override Sys<G,?> system(work.connor.zorcs.server.grid.absolute.CellCoord<G,?> cc,
                              work.connor.zorcs.server.grid.absolute.Orientation<G,?> o);
    @Override Stream<? extends CellCoord<G,?>> euclideanBox(double max);
    @Override Stream<? extends LocationCoord<G,?>> euclideanCoordBox(double max);
}
