package work.connor.zorcs.server.game.stimulus;

import org.json.JSONObject;
import work.connor.zorcs.server.grid.Grid;

public abstract class ActionStimulus<G extends Grid<G>> implements Stimulus<G> {
    private JSONObject command;
    public ActionStimulus(JSONObject command) {this.command = command;}
    @Override public JSONObject toRemote(JSONObject output) {
        output = Stimulus.super.toRemote(output);
        return put(output, "command", command);
    }
}
