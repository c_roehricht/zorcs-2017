package work.connor.zorcs.server.grids.tape;

import org.json.JSONObject;
import work.connor.zorcs.server.grids.flat.FlatGrid;
import work.connor.zorcs.server.utility.MathU;
import work.connor.zorcs.server.utility.SetU;

import java.util.Set;

public interface Tape extends FlatGrid<Tape,CellCoord,GapCoord,FCoord,Angle> {
    Tape TAPE = new Tape() {};
    interface Based extends FlatGrid.Based<Tape,CellCoord,GapCoord,FCoord,Angle> {
        @Override default Tape g() {return TAPE;}
    }
    default CellCoord cellCoord(int a) {return new CellCoord(a);}
    default FCoord fCoord(double a) {return new FCoord(a);}
    @Override default Angle angle(int r) {return new Angle(r);}
    @Override default int circle() {return 2;}
    @Override default GapCoord gapCoord(CellCoord cc) {return new GapCoord(cc);}
    @Override default boolean gInvariant(CellCoord cc) {return cc.a % 2 != 0;}
    @Override default CellCoord unitForward() {return cellCoord(1);}
    @Override default CellCoord origin() {return cellCoord(0);}
    @Override default Set<? extends CellCoord> euclideanSquareSector(int dSq) {
        return SetU.map(SetU.ofOpt(MathU.intSqrt(dSq)), this::cellCoord);
    }
    @Override default Set<? extends CellCoord> manhattanSector(int d) {return Set.of(cellCoord(d));}
    @Override default Set<? extends CellCoord> chebyshevSector(int d) {return Set.of(cellCoord(d));}
    @Override default Sys system(CellCoord cc, Angle a) {return new Sys(cc,a);}
    @Override default CellCoord cellCoord(JSONObject input) {return cellCoord(input.getInt("a"));}
}
