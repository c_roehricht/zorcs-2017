package work.connor.zorcs.server.utility;

import java.util.Optional;
import java.util.function.Supplier;

public class OptU {
    public static <U, D extends U> Optional<U> up(Optional<D> subOpt) {return subOpt.map(d -> (U) d);}
    public static <U, D extends U> Optional<U> or(Optional<D> o1, Supplier<? extends Optional<? extends U>> o2Supp) {
        return OptU.<U,D>up(o1).or(o2Supp);
    }
}
