package work.connor.zorcs.server.game.space;

import work.connor.zorcs.server.game.event.Event;
import work.connor.zorcs.server.grid.general.Grid;

import java.util.LinkedHashSet;
import java.util.Set;

public abstract class DefaultLocation<G extends Grid<G>> implements Location<G> {
    protected Space<G> space;
    protected Set<Event<G>> events = new LinkedHashSet<>();
    protected DefaultLocation(Space<G> space) {this.space = space;}

    @Override public Space<G> getSpace() {return space;}
    @Override public void clearEvents() {events.clear();}
    @Override public void addEvent(Event<G> event) {events.add(event);}
    @Override public Set<Event<G>> getEvents() {return events;}
}
