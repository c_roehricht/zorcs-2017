package work.connor.zorcs.server.grid.relative;

import work.connor.zorcs.server.grid.absolute.Sys;

public interface CellCoord<G  extends Grid<G>,
                           CC extends CellCoord<G,CC>>
        extends work.connor.zorcs.server.grid.CellCoord<G,CC>,
                LocationCoord<G,CC> {
    @Override work.connor.zorcs.server.grid.absolute.CellCoord<?,?> toAbsolute(Sys<?,?> sy);
}
