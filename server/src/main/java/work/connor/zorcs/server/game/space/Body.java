package work.connor.zorcs.server.game.space;

import work.connor.zorcs.server.grid.general.Grid;
import work.connor.zorcs.server.grid.absolute.*;

public interface Body<G extends Grid<G>> extends LocalObserver.Mobile<G> {
    Cell<G> getBodyCell();
    void setBodyCell(Cell<G> cell);
    @Override default Cell<G> getCell() {return getBodyCell();}
    @Override default Orientation<G,?> getOrientation() {
        Cell<G> cell = getCell();
        return cell == null ? null : cell.getOrientation(this);
    }
    @Override default void setCell(Cell<G> cell) {
        Orientation<G,?> orientation = getOrientation();
        if (getCell() != null) getCell().removeBody(this);
        setBodyCell(cell);
        if (cell != null) cell.addBody(this, orientation);
    }
    @Override default void setOrientation(Orientation<G,?> orientation) {getCell().addBody(this, orientation);}

    interface Based<G extends Grid<G>> extends LocalObserver.Based<G> {
        Body<G> getBody();
        @Override default LocalObserver<G> getLocalObserver() {return getBody();}
    }
    interface Mapped<G extends Grid<G>> extends Based<G> {
        void setBody(Body<G> body);
    }
}
