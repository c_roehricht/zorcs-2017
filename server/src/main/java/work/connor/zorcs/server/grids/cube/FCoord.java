package work.connor.zorcs.server.grids.cube;

public class FCoord
        implements work.connor.zorcs.server.grids.common.FCoord<Cube,CellCoord,GapCoord, FCoord,Direction,Orientation>,
                   Coord<FCoord> {
    public final double a, b, c;
    public FCoord(double a, double b, double c) {this.a = a; this.b = b; this.c = c;}
    @Override public FCoord add(FCoord fc) {return new FCoord(a + fc.a, b + fc.b, c + fc.c);}
    @Override public FCoord multiply(double factor) {return new FCoord(a * factor, b * factor, c * factor);}
    @Override public FCoord negate() {return new FCoord(-a, -b, -c);}
    @Override public double floatingManhattan() {return Math.abs(a) + Math.abs(b) + Math.abs(c);}
    @Override public double euclidean() {return Math.sqrt(a * a + b * b + c * c);}
    @Override public double floatingChebyshev() {return Math.max(Math.abs(a), Math.max(Math.abs(b), Math.abs(c)));}
    @Override public FCoord rotate(Orientation o) {
        double[] vec = o.matrix.multiply(a, b, c);
        return new FCoord(vec[0], vec[1], vec[2]);
    }
    @Override public FCoord mirror() {return new FCoord(a, -b, -c);}
}
