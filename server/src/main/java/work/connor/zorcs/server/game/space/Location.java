package work.connor.zorcs.server.game.space;

import work.connor.zorcs.server.game.event.Event;
import work.connor.zorcs.server.game.event.VisualEvent;
import work.connor.zorcs.server.grid.general.Grid;
import work.connor.zorcs.server.grid.absolute.*;
import work.connor.zorcs.server.utility.SetU;

import java.util.Set;

public interface Location<G extends Grid<G>> extends Space.Based<G> {
    LocationCoord<G,?> getCoord();
    void clearEvents();
    void addEvent(Event<G> event);
    Set<Event<G>> getEvents();
    default Set<VisualEvent<G>> getVisualEvents() {return SetU.filterDown(getEvents(), e -> e instanceof VisualEvent);}

    interface Based<G extends Grid<G>> extends Space.Based<G> {
        Location<G> getLocation();
        @Override default Space<G> getSpace() {return getLocation().getSpace();}
    }
}
