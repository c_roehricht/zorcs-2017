package work.connor.zorcs.server.game.space;

import work.connor.zorcs.server.game.actor.Actor;
import work.connor.zorcs.server.game.emitter.Emitter;
import work.connor.zorcs.server.game.processor.Processor;
import work.connor.zorcs.server.grid.general.Grid;
import work.connor.zorcs.server.grid.absolute.*;
import work.connor.zorcs.server.utility.IterU;

import java.util.Iterator;
import java.util.Random;

public interface Space<G extends Grid<G>> extends Iterable<Location<G>>,
                                                  Grid.Based<G> {
    Random getRandom();
    void nextTick();
    int getTick();
    Cell<G> getCell(CellCoord<G,?> coord);
    default boolean hasCell(CellCoord<G,?> coord) {return getCell(coord) != null;}
    Gap<G> getGap(GapCoord<G,?> coord);
    default boolean hasGap(GapCoord<G,?> coord) {return getGap(coord) != null;}
    @SuppressWarnings("unchecked")
    default Location<G> getLocation(LocationCoord<G,?> coord) {
        return coord instanceof CellCoord ? getCell((CellCoord<G,?>) coord)
                                          : getGap((GapCoord<G,?>) coord);
    }
    Iterable<Cell<G>> cells();
    Iterable<Gap<G>> gaps();
    void addCell(Cell<G> cell, CellCoord<G,?> coord);
    void removeCell(Cell<G> cell);
    default void removeCell(CellCoord<G,?> coord) {removeCell(getCell(coord));}
    void addGap(Gap<G> gap, GapCoord<G,?> coord);
    void removeGap(Gap<G> gap);
    default void removeGap(GapCoord<G,?> coord) {removeGap(getGap(coord));}
    void addEmitter(Emitter<G> emitter);
    void removeEmitter(Emitter<G> emitter);
    void addProcessor(Processor<G> processor);
    void removeProcessor(Processor<G> processor);
    void schedule(Actor<G> actor, int delay);
    @Override default Iterator<Location<G>> iterator() {return IterU.concat(cells(), gaps());}

    interface Based<G extends Grid<G>> extends Grid.Based<G> {
        Space<G> getSpace();
        @Override default G g() {return getSpace().g();}
    }
    interface Mapped<G extends Grid<G>> extends Based<G> {
        void setSpace(Space<G> space);
    }
}
