package work.connor.zorcs.server.grid;

import java.util.Set;
import java.util.stream.Stream;

public interface Grid<G extends Grid<G>> {
    interface Based<G extends Grid<G>> {G g();}
    CellCoord<G,?> unitForward();
    CellCoord<G,?> origin();
    Direction<G,?> forward();
    Direction<G,?> behind();
    Orientation<G,?> noRotation();
    Set<? extends Direction<G,?>> directions();
    Set<? extends Orientation<G,?>> orientations();
    Set<? extends CellCoord<G,?>> euclideanSquareSector(int dSq);
    Stream<? extends CellCoord<G,?>> euclideanBox();
    Stream<? extends CellCoord<G,?>> euclideanBox(double max);
    Stream<? extends LocationCoord<G,?>> euclideanCoordBox(double max);
    Set<? extends CellCoord<G,?>> manhattanSector(int d);
    Stream<? extends CellCoord<G,?>> manhattanBox();
    Stream<? extends CellCoord<G,?>> manhattanBox(int max);
    Stream<? extends LocationCoord<G,?>> manhattanCoordBox(int max);
    Set<? extends CellCoord<G,?>> chebyshevSector(int d);
    Stream<? extends CellCoord<G,?>> chebyshevBox();
    Stream<? extends CellCoord<G,?>> chebyshevBox(int max);
    Stream<? extends LocationCoord<G,?>> chebyshevCoordBox(int max);
}
