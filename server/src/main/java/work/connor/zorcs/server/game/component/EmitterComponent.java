package work.connor.zorcs.server.game.component;

import work.connor.zorcs.server.game.emitter.Emitter;
import work.connor.zorcs.server.game.space.Space;
import work.connor.zorcs.server.grid.general.Grid;

public interface EmitterComponent<G extends Grid<G>> extends Component<G>,
                                                             Emitter<G>,
                                                             Space.Based<G> {
    @Override default void componentInstaller() {getSpace().addEmitter(this);}
    @Override default void componentUninstaller() {getSpace().removeEmitter(this);}
}
