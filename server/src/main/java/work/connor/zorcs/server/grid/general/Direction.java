package work.connor.zorcs.server.grid.general;

public interface Direction<G extends Grid<G>,
                           D extends Direction<G,D>>
        extends work.connor.zorcs.server.grid.absolute.Direction<G,D>,
                work.connor.zorcs.server.grid.relative.Direction<G,D>,
                SpatialValue<G,D> {
}
