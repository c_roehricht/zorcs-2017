package work.connor.zorcs.server.grid.relative;

import work.connor.zorcs.server.grid.absolute.Sys;

public interface Coord<G extends Grid<G>,
                       C extends Coord<G,C>>
        extends work.connor.zorcs.server.grid.Coord<G,C>,
                SpatialValue<G,C> {
    @Override work.connor.zorcs.server.grid.absolute.Coord<?,?> toAbsolute(Sys<?,?> sy);
}
