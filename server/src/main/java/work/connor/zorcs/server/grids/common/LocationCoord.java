package work.connor.zorcs.server.grids.common;

import work.connor.zorcs.server.grid.absolute.Sys;

public interface LocationCoord<G   extends CommonGrid<G,CC,GC,FC,D,O>,
                               LCs extends LocationCoord<G,LCs,CC,GC,FC,D,O>,
                               CC  extends CellCoord<G,CC,GC,FC,D,O>,
                               GC  extends GapCoord<G,CC,GC,FC,D,O>,
                               FC  extends FCoord<G,CC,GC,FC,D,O>,
                               D   extends Direction<G,CC,GC,FC,D,O>,
                               O   extends Orientation<G,CC,GC,FC,D,O>>
        extends work.connor.zorcs.server.grid.general.LocationCoord<G,LCs>,
                Coord<G,LCs,CC,GC,FC,D,O> {
    @Override default LCs toAbsolute(Sys<?, ?> sy) {return Coord.super.toAbsolute(sy);}
    @Override default LCs toRelative(Sys<G, ?> sy) {return Coord.super.toRelative(sy);}
    @SuppressWarnings("unchecked")
    @Override default LCs add(work.connor.zorcs.server.grid.CellCoord<G,?> cc) {return add((CC) cc);}
    LCs add(CC cc);
    @SuppressWarnings("unchecked")
    @Override default FC multiply(double factor) {
        return (FC) work.connor.zorcs.server.grid.general.LocationCoord.super.multiply(factor);
    }
}
