package work.connor.zorcs.server.grid.general;

public interface GapCoord<G  extends Grid<G>,
                          GC extends GapCoord<G,GC>>
        extends work.connor.zorcs.server.grid.absolute.GapCoord<G,GC>,
                work.connor.zorcs.server.grid.relative.GapCoord<G,GC>,
                LocationCoord<G,GC> {
}
