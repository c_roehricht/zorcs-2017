package work.connor.zorcs.server.grids.quad;

public interface Coord<C extends Coord<C>>
        extends work.connor.zorcs.server.grids.quadhex.Coord<Quad,C,CellCoord,GapCoord,FCoord,Angle>,
                SpatialValue<C> {
}
