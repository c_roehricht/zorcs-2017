package work.connor.zorcs.server.grids.quadhex;

import work.connor.zorcs.server.grids.flat.*;

public interface LocationCoord<G  extends QuadHexGrid<G,CC,GC,FC,A>,
                               LC extends LocationCoord<G,LC,CC,GC,FC,A>,
                               CC extends CellCoord<G,CC,GC,FC,A>,
                               GC extends GapCoord<G,CC,GC,FC,A>,
                               FC extends FCoord<G,CC,GC,FC,A>,
                               A  extends Angle<G,CC,GC,FC,A>>
        extends work.connor.zorcs.server.grids.flat.LocationCoord<G,LC,CC,GC,FC,A>,
                Coord<G,LC,CC,GC,FC,A> {
}
