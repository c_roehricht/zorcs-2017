package work.connor.zorcs.server.debug;

public abstract class PrettyObject implements PrettyString {
    @Override public String toString() {return prettyString();}
}
