package work.connor.zorcs.server.game.intent;

import org.json.JSONObject;
import work.connor.zorcs.server.game.action.Action;
import work.connor.zorcs.server.game.action.ActionFailedException;
import work.connor.zorcs.server.game.component.BrainComponent;
import work.connor.zorcs.server.game.stimulus.ActionCompleteStimulus;
import work.connor.zorcs.server.game.stimulus.ActionFailedStimulus;
import work.connor.zorcs.server.game.stimulus.ActionStimulus;
import work.connor.zorcs.server.grid.general.Grid;

public class BrainIntent<G extends Grid<G>> implements Intent<G> {
    private BrainComponent<G,? super ActionStimulus<G>> brain;
    private Action<G> action;
    private JSONObject command;
    public BrainIntent(BrainComponent<G, ? super ActionStimulus<G>> brain, Action<G> action, JSONObject command) {
        this.brain = brain;
        this.action = action;
        this.command = command;
    }
    @Override public Action<G> getAction() {return action;}
    @Override public boolean isTracked() {return true;}
    @Override public void complete(ActionFailedException cause) {
        if (cause != null) brain.accept(new ActionFailedStimulus<G>(command, cause));
        else brain.accept(new ActionCompleteStimulus<>(command));
    }
}
