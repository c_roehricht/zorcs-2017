package work.connor.zorcs.server.grid;

import work.connor.zorcs.server.utility.SetU;

import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

public interface CellCoord<G  extends Grid<G>,
                           CC extends CellCoord<G,CC>>
    extends LocationCoord<G,CC> {
    @Override CC multiply(int factor);
    Optional<? extends CC> halfCell();
    Optional<? extends GapCoord<G,?>> halfGap();
    Optional<? extends LocationCoord<G,?>> half();
    GapCoord<G,?> nextGap(Direction<G,?> d);
    int manhattan();
    int euclideanSquare();
    int chebyshev();
    default CC neighbour(Direction<G,?> d) {return translate(d.unit());}
    default Set<? extends CC> neighbours(Set<? extends Direction<G,?>> ds) {return SetU.map(ds, this::neighbour);}
    default Set<? extends CC> neighbours() {return neighbours(g().directions());}
    Set<? extends GapCoord<G,?>> neighbourGaps(Set<? extends Direction<G,?>> ds);
    Set<? extends GapCoord<G,?>> neighbourGaps();
    default Set<? extends CC> chebyshevBox(int max) {
        return g().chebyshevBox(max).map(this::translate).collect(Collectors.toSet());
    }
}
