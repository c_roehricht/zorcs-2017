package work.connor.zorcs.server.grid.relative;

import work.connor.zorcs.server.game.protocol.RemoteOutput;
import work.connor.zorcs.server.grid.absolute.Sys;

public interface LocationCoord<G  extends Grid<G>,
                               LC extends LocationCoord<G,LC>>
        extends work.connor.zorcs.server.grid.LocationCoord<G,LC>,
                Coord<G,LC>,
                RemoteOutput {
    @Override work.connor.zorcs.server.grid.absolute.LocationCoord<?,?> toAbsolute(Sys<?,?> sy);
}
