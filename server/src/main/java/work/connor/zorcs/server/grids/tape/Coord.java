package work.connor.zorcs.server.grids.tape;

public interface Coord<C extends Coord<C>>
        extends work.connor.zorcs.server.grids.flat.Coord<Tape,C,CellCoord,GapCoord,FCoord,Angle>,
                SpatialValue<C> {
}
