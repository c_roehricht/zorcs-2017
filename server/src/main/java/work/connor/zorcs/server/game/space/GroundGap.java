package work.connor.zorcs.server.game.space;

import work.connor.zorcs.server.game.emitter.LocalEmitter;
import work.connor.zorcs.server.game.event.Event;
import work.connor.zorcs.server.game.event.VisualEvent;
import work.connor.zorcs.server.grid.general.Grid;
import work.connor.zorcs.server.grid.absolute.GapCoord;

import java.util.function.Consumer;

public class GroundGap<G extends Grid<G>> extends DefaultGap<G>
        implements LocalEmitter<G> {
    private String appearance;
    public GroundGap(Space<G> space, GapCoord<G,?> coord, String appearance) {
        super(space, coord);
        this.appearance = appearance;
    }

    @Override public Location<G> getLocation() {return this;}
    @Override public void getLocalEvents(Consumer<Event<G>> events) {
        events.accept(new VisualEvent<>(null,"gap",appearance,null));
    }
    @Override public void installAsGap(Space<G> space, GapCoord<G,?> coord) {
        super.installAsGap(space, coord);
        installAsEmitter(space);
    }
    @Override public void uninstallAsGap() {
        uninstallAsEmitter(getSpace());
        super.uninstallAsGap();
    }
}
