package work.connor.zorcs.server.utility;

import org.junit.jupiter.params.provider.Arguments;

import java.util.stream.Stream;

public class TestU {
    public static <A,B> Stream<Arguments> combine(Stream<A> argsA, Stream<B> argsB) {
        return StreamU.cartesian(argsA, argsB, (a,b) -> Arguments.of(a,b));
    }
}
