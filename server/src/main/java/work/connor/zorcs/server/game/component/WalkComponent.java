package work.connor.zorcs.server.game.component;

import org.json.JSONObject;
import work.connor.zorcs.server.game.action.*;
import work.connor.zorcs.server.game.stimulus.ActionStimulus;
import work.connor.zorcs.server.grid.general.Grid;

public class WalkComponent<G extends Grid<G>> extends DefaultBodyActionComponent<G> {
    public WalkComponent(BrainComponent<G,? super ActionStimulus<G>> brainComp, BodyComponent<G> bodyComp) {
        super(brainComp, bodyComp, "step", "turn");
    }
    @Override protected Action<G> makeAction(JSONObject command) throws ActionFailedException {
        switch (command.getString("action")) {
            case "step": return makeStepAction(command);
            case "turn": return makeTurnAction(command);
            default: throw new ActionFailedException(ActionFailedException.FailureCause.InvalidParams);
        }
    }
    protected StepAction<G> makeStepAction(JSONObject command) {
        return new StepAction<>(getBodyComp(),g().direction(command.getJSONObject("direction")));
    }
    protected TurnAction<G> makeTurnAction(JSONObject command) {
        return new TurnAction<>(getBodyComp(),g().orientation(command.getJSONObject("orientation")));
    }
}
