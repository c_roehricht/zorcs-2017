package work.connor.zorcs.server.grids.hex;

public interface Coord<C extends Coord<C>>
        extends work.connor.zorcs.server.grids.quadhex.Coord<Hex,C,CellCoord,GapCoord,FCoord,Angle>,
                SpatialValue<C> {
}
