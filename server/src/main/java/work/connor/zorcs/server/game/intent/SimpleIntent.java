package work.connor.zorcs.server.game.intent;

import work.connor.zorcs.server.game.action.Action;
import work.connor.zorcs.server.grid.general.Grid;

public class SimpleIntent<G extends Grid<G>> implements Intent<G> {
    private Action<G> action;
    public SimpleIntent(Action<G> action) {this.action = action;}
    @Override public Action<G> getAction() {return action;}
}
