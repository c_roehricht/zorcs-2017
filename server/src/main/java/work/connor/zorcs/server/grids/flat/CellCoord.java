package work.connor.zorcs.server.grids.flat;

import work.connor.zorcs.server.grids.common.*;

public interface CellCoord<G  extends FlatGrid<G,CC,GC,FC,A>,
                           CC extends CellCoord<G,CC,GC,FC,A>,
                           GC extends GapCoord<G,CC,GC,FC,A>,
                           FC extends FCoord<G,CC,GC,FC,A,A>,
                           A  extends Angle<G,CC,GC,FC,A>>
        extends work.connor.zorcs.server.grids.common.CellCoord<G,CC,GC,FC,A,A>,
                LocationCoord<G,CC,CC,GC,FC,A> {
}
