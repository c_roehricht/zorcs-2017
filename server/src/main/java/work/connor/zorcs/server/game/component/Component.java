package work.connor.zorcs.server.game.component;

import work.connor.zorcs.server.game.entity.Entity;
import work.connor.zorcs.server.grid.general.Grid;

public interface Component<G extends Grid<G>> {
    Entity<G> getEntity();
    void setEntity(Entity<G> entity);
    default void installAsComponent(Entity<G> entity) {
        setEntity(entity);
        entity.addComponent(this);
        componentInstaller();
    }
    default void componentInstaller() {}
    default void uninstallAsComponent() {
        componentUninstaller();
        getEntity().removeComponent(this);
        setEntity(null);
    }
    default void componentUninstaller() {}
}
