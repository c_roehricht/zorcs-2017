package work.connor.zorcs.server.game.component;

import work.connor.zorcs.server.grid.general.Grid;

public abstract class DefaultBodyMappedComponent<G extends Grid<G>> extends DefaultComponent<G>
        implements BodyComponent.Mapped<G> {
    private BodyComponent<G> bodyComp;
    public DefaultBodyMappedComponent(BodyComponent<G> bodyComp) {this.bodyComp = bodyComp;}
    @Override public BodyComponent<G> getBodyComp() {return bodyComp;}
    @Override public void setBodyComp(BodyComponent<G> bodyComp) {this.bodyComp = bodyComp;}
}
