package work.connor.zorcs.server.grids.hex;

public interface SpatialValue<V extends SpatialValue<V>>
        extends work.connor.zorcs.server.grids.quadhex.SpatialValue<Hex,V,CellCoord,GapCoord,FCoord,Angle>,
                Hex.Based {
}
