package work.connor.zorcs.server.grids.common;

import work.connor.zorcs.server.grid.absolute.Sys;

public interface FCoord<G  extends CommonGrid<G,CC,GC,FC,D,O>,
                        CC extends CellCoord<G,CC,GC,FC,D,O>,
                        GC extends GapCoord<G,CC,GC,FC,D,O>,
                        FC extends FCoord<G,CC,GC,FC,D,O>,
                        D  extends Direction<G,CC,GC,FC,D,O>,
                        O  extends Orientation<G,CC,GC,FC,D,O>>
        extends work.connor.zorcs.server.grid.general.FCoord<G,FC>,
                Coord<G,FC,CC,GC,FC,D,O> {
    @Override default FC toAbsolute(Sys<?,?> sy) {return Coord.super.toAbsolute(sy);}
    @Override default FC toRelative(Sys<G,?> sy) {return Coord.super.toRelative(sy);}
    @Override default FC floating() {return work.connor.zorcs.server.grid.general.FCoord.super.floating();}
    @SuppressWarnings("unchecked")
    @Override default FC add(work.connor.zorcs.server.grid.FCoord<G,?> fc) {return add((FC) fc);}
    FC add(FC fc);
}
