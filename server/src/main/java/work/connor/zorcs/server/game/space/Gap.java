package work.connor.zorcs.server.game.space;

import work.connor.zorcs.server.grid.general.Grid;
import work.connor.zorcs.server.grid.absolute.*;

public interface Gap<G extends Grid<G>> extends Location<G> {
    GapCoord<G,?> getGapCoord();
    void setSpaceCoord(Space<G> space, GapCoord<G,?> coord);
    @Override default LocationCoord<G,?> getCoord() {return getGapCoord();}
    default void installAsGap(Space<G> space, GapCoord<G,?> coord) {
        setSpaceCoord(space, coord);
        space.addGap(this, coord);
    }
    default void uninstallAsGap() {
        getSpace().removeGap(this);
        setSpaceCoord(null, null);
    }

    interface Based<G extends Grid<G>> extends Location.Based<G> {
        Gap<G> getGap();
        @Override default Location<G> getLocation() {return getGap();}
    }

    interface Mapped<G extends Grid<G>> extends Based<G> {
        void setGap(Gap<G> gap);
    }
}
