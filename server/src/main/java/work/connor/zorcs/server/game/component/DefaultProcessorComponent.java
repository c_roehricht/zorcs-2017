package work.connor.zorcs.server.game.component;

import work.connor.zorcs.server.game.processor.StimulusProcessor;
import work.connor.zorcs.server.game.stimulus.Stimulus;
import work.connor.zorcs.server.grid.general.Grid;

public abstract class DefaultProcessorComponent<G extends Grid<G>, S extends Stimulus<G>>
        extends DefaultBrainBodyMappedComponent<G,S>
        implements ProcessorComponent<G>,
                   StimulusProcessor<G,S> {
    public DefaultProcessorComponent(BrainComponent<G, ? super S> brainComp,
                                     BodyComponent<G> bodyComp) {super(brainComp, bodyComp);}
    @Override public Sink<G, ? super S> getSink() {return getBrainComp();}
}
