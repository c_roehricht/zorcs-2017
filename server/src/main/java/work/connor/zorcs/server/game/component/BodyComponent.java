package work.connor.zorcs.server.game.component;

import work.connor.zorcs.server.grid.general.Grid;
import work.connor.zorcs.server.game.space.Body;

public interface BodyComponent<G extends Grid<G>>
        extends Component<G>,
                Body<G> {
    interface Based<G extends Grid<G>> extends Body.Based<G> {
        BodyComponent<G> getBodyComp();
        @Override default Body<G> getBody() {return getBodyComp();}
    }
    interface Mapped<G extends Grid<G>> extends Based<G> {
        void setBodyComp(BodyComponent<G> bodyComp);
    }
}
