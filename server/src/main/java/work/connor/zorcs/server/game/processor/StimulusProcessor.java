package work.connor.zorcs.server.game.processor;

import work.connor.zorcs.server.game.stimulus.Stimulus;
import work.connor.zorcs.server.grid.general.Grid;

public interface StimulusProcessor<G extends Grid<G>,
                                   S extends Stimulus<G>> extends Processor<G>  {
    interface Sink<G extends Grid<G>,
                   S extends Stimulus<G>> {
        void accept(S stimulus);
    }
    Sink<G,? super S> getSink();
    default void output(S stimulus) {getSink().accept(stimulus);}
}
