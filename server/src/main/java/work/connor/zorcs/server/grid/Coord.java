package work.connor.zorcs.server.grid;

public interface Coord<G extends Grid<G>,
                       C extends Coord<G,C>>
        extends SpatialValue<G,C> {
    FCoord<G,?> floating();
    Coord<G,?> multiply(int factor);
    FCoord<G,?> multiply(double factor);
    default FCoord<G,?> add(Coord<G,?> c) {return floating().add(c.floating());}
    C add(CellCoord<G,?> cc);
    C negate();
    default Coord<G,?> subtract(Coord<G,?> c) {return add(c.negate());}
    default C subtract(CellCoord<G,?> c) {return add(c.negate());}
    double floatingManhattan();
    double euclidean();
    double floatingChebyshev();
}
