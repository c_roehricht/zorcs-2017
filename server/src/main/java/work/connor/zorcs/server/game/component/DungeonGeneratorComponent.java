package work.connor.zorcs.server.game.component;

import work.connor.zorcs.server.game.actor.Actor;
import work.connor.zorcs.server.game.intent.Intent;
import work.connor.zorcs.server.game.space.GroundCell;
import work.connor.zorcs.server.game.space.GroundGap;
import work.connor.zorcs.server.grid.absolute.*;
import work.connor.zorcs.server.grid.general.Grid;

public interface DungeonGeneratorComponent<G extends Grid<G>>
        extends Component<G>,
                Actor<G> {
    static <G extends Grid<G>> Intent<G> spawnCell(CellCoord<G,?> coord) {
        return Intent.lambda(space -> {
            new GroundCell<>(space, coord, "stone");
            for (GapCoord<G,?> gapCoord : coord.neighbourGaps()) {
                if (!space.hasGap(gapCoord)) new GroundGap<>(space, gapCoord, "stone");
            }
        });
    }
    static <G extends Grid<G>> Intent<G> removeCell(CellCoord<G,?> coord) {
        return Intent.lambda(space -> {
            for (Direction<G,?> d : space.g().directions()) {
                if (!space.hasCell(coord.neighbour(d))) space.removeGap(coord.nextGap(d));
            }
            space.removeCell(coord);
        });
    }
}
