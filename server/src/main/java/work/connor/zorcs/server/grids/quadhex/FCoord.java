package work.connor.zorcs.server.grids.quadhex;

import work.connor.zorcs.server.grids.flat.*;

public abstract class FCoord<G  extends QuadHexGrid<G,CC,GC,FC,A>,
                             CC extends CellCoord<G,CC,GC,FC,A>,
                             GC extends GapCoord<G,CC,GC,FC,A>,
                             FC extends FCoord<G,CC,GC,FC,A>,
                             A  extends Angle<G,CC,GC,FC,A>>
        implements work.connor.zorcs.server.grids.common.FCoord<G,CC,GC,FC,A,A>,
                   Coord<G,FC,CC,GC,FC,A> {
    protected final double a, b;
    protected FCoord(double a, double b) {this.a = a; this.b = b;}
    @Override public FC multiply(double factor) {return g().fCoord(a * factor, b * factor);}
    @Override public FC add(FC fc) {return g().fCoord(a + fc.a, b + fc.b);}
    @Override public FC negate() {return g().fCoord(-a, -b);}
}
