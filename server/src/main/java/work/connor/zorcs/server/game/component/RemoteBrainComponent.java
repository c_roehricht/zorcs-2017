package work.connor.zorcs.server.game.component;

import org.json.JSONObject;
import work.connor.zorcs.server.game.action.ActionFailedException.FailureCause;
import work.connor.zorcs.server.game.intent.Intent;
import work.connor.zorcs.server.game.space.Space;
import work.connor.zorcs.server.game.stimulus.ActionFailedStimulus;
import work.connor.zorcs.server.game.stimulus.MayActStimulus;
import work.connor.zorcs.server.game.stimulus.Stimulus;
import work.connor.zorcs.server.grid.general.Grid;

import java.util.function.Consumer;
import java.util.function.Supplier;

public class RemoteBrainComponent<G extends Grid<G>> extends DefaultBrainComponent<G, Stimulus<G>> {
    private Supplier<JSONObject> inputs;
    private Consumer<JSONObject> outputs;
    public RemoteBrainComponent(Supplier<JSONObject> inputs, Consumer<JSONObject> outputs) {
        super();
        this.inputs = inputs;
        this.outputs = outputs;
    }
    @Override public void accept(Stimulus<G> stimulus) {outputs.accept(stimulus.toRemote(null));}
    @Override public void act(Space<G> space, Consumer<Intent<G>> intents) {
        accept(new MayActStimulus<>());
        while (true) {
            JSONObject input = inputs.get();
            String command = input.getString("action");
            if (command.equals("yield")) break;
            act(intents, getActionComponent(command), input);
        }
        intents.accept(Intent.reschedule(this, 2));
    }
    private void act(Consumer<Intent<G>> intents, ActionComponent<G> component, JSONObject command) {
        if (component == null) {accept(new ActionFailedStimulus<>(command, FailureCause.NoComponent)); return;}
        component.act(intents, command);
    }
}
