package work.connor.zorcs.server.grids.hex;

public class GapCoord
        extends work.connor.zorcs.server.grids.quadhex.GapCoord<Hex,CellCoord, GapCoord,FCoord,Angle>
        implements LocationCoord<GapCoord> {
    public GapCoord(CellCoord cellCoord) {super(cellCoord);}
}
