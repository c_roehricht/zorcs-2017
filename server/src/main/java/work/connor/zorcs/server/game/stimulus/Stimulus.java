package work.connor.zorcs.server.game.stimulus;

import org.json.JSONObject;
import work.connor.zorcs.server.game.protocol.RemoteOutput;
import work.connor.zorcs.server.grid.Grid;

public interface Stimulus<G extends Grid<G>> extends RemoteOutput {
    String getName();
    @Override default JSONObject toRemote(JSONObject output) {
        output = RemoteOutput.super.toRemote(output);
        return output.put("name", getName());
    }
}
