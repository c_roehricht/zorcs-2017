package work.connor.zorcs.server.game.action;

import org.json.JSONObject;
import work.connor.zorcs.server.game.protocol.RemoteOutput;

public class ActionFailedException extends Exception
        implements RemoteOutput {
    public enum FailureCause {
        NoComponent("no-component"),
        InvalidParams("invalid-parameters"),
        Collision("collision");
        private String name;
        FailureCause(String name) {this.name = name;}
        public String getName() {return name;}
    }
    private FailureCause failureCause;
    public ActionFailedException(FailureCause failureCause) {this.failureCause = failureCause;}
    public FailureCause getFailureCause() {return failureCause;}
    @Override public JSONObject toRemote(JSONObject output) {
        output = RemoteOutput.super.toRemote(output);
        return output.put("name", failureCause.getName());
    }
}
