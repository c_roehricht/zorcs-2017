package work.connor.zorcs.server.game.component;

import org.json.JSONObject;
import work.connor.zorcs.server.game.action.Action;
import work.connor.zorcs.server.game.action.ActionFailedException;
import work.connor.zorcs.server.game.intent.BrainIntent;
import work.connor.zorcs.server.game.intent.Intent;
import work.connor.zorcs.server.game.stimulus.ActionFailedStimulus;
import work.connor.zorcs.server.game.stimulus.ActionStimulus;
import work.connor.zorcs.server.grid.general.Grid;

import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;

public abstract class DefaultBodyActionComponent<G extends Grid<G>>
        extends DefaultActionComponent<G>
        implements BodyComponent.Mapped<G> {
    private List<String> actionNames;
    private BodyComponent<G> bodyComp;
    public DefaultBodyActionComponent(BrainComponent<G,? super ActionStimulus<G>> brainComp,
                                      BodyComponent<G> bodyComp, String... actionNames) {
        super(brainComp);
        this.bodyComp = bodyComp;
        this.actionNames = Arrays.asList(actionNames);
    }
    @Override public BodyComponent<G> getBodyComp() {return bodyComp;}
    @Override public void setBodyComp(BodyComponent<G> bodyComp) {this.bodyComp = bodyComp;}
    @Override public List<String> getActionNames() {return actionNames;}
    @Override public void act(Consumer<Intent<G>> intents, JSONObject command) {
        try {
            Action<G> action = makeAction(command);
            intents.accept(new BrainIntent<>(getBrainComp(), action, command));
        } catch (ActionFailedException e) {
            getBrainComp().accept(new ActionFailedStimulus<>(command, e));
        }
    }
    protected abstract Action<G> makeAction(JSONObject command) throws ActionFailedException;
}
