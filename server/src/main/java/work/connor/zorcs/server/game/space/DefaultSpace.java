package work.connor.zorcs.server.game.space;

import work.connor.zorcs.server.game.action.ActionFailedException;
import work.connor.zorcs.server.game.actor.Actor;
import work.connor.zorcs.server.game.emitter.Emitter;
import work.connor.zorcs.server.game.intent.Intent;
import work.connor.zorcs.server.game.processor.Processor;
import work.connor.zorcs.server.grid.general.Grid;
import work.connor.zorcs.server.grid.absolute.*;

import java.util.*;

public class DefaultSpace<G extends Grid<G>> implements Space<G> {
    private G grid;
    private Map<CellCoord<G,?>, Cell<G>> cells = new LinkedHashMap<>();
    private Map<GapCoord<G,?>, Gap<G>> gaps = new LinkedHashMap<>();
    private Set<Emitter<G>> emitters = new LinkedHashSet<>();
    private Set<Processor<G>> processors = new LinkedHashSet<>();
    private SortedMap<Integer,Set<Actor<G>>> schedule = new TreeMap<>();
    private List<Intent> intents = new ArrayList<>();
    private Random random = new Random(0);
    private int tick = 0;

    public DefaultSpace(G grid) {this.grid = grid;}

    @Override public Random getRandom() {return random;}
    @Override public int getTick() {return tick;}
    @Override public void nextTick() {
        tick++;
        cleanup();
        emitEvents();
        process();
        letAct();
        execute();
    }

    private void emitEvents() {for (Emitter<G> emitter : emitters) emitter.emitEvents(this);}
    private void process() {for (Processor<G> processor : processors) processor.process(this);}
    private void letAct() {
        List<Actor> actors = new ArrayList<>(getSchedule(tick));
        Collections.shuffle(actors, random);
        actors.forEach(this::letAct);
    }
    private void letAct(Actor<G> actor) {actor.act(this, intents::add);}
    private void execute() {intents.forEach(this::execute);}
    private void execute(Intent<G> intent) {
        try {
            intent.getAction().perform(this);
            if (intent.isTracked()) intent.complete(null);
        } catch (ActionFailedException cause) {
            if (intent.isTracked()) intent.complete(cause);
        }
    }
    private void cleanup() {
        forEach(Location::clearEvents);
        schedule.headMap(tick).clear();
        intents.clear();
    }

    @Override public Cell<G> getCell(CellCoord<G,?> coord) {return cells.get(coord);}
    @Override public Gap<G> getGap(GapCoord<G,?> coord) {return gaps.get(coord);}
    @Override public Iterable<Cell<G>> cells() {return cells.values();}
    @Override public Iterable<Gap<G>> gaps() {return gaps.values();}
    @Override public void addCell(Cell<G> cell, CellCoord<G,?> coord) {cells.put(coord, cell);}
    @Override public void removeCell(Cell<G> cell) {if (cell != null) cells.remove(cell.getCellCoord());}
    @Override public void addGap(Gap<G> gap, GapCoord<G,?> coord) {gaps.put(coord, gap);}
    @Override public void removeGap(Gap<G> gap) {if (gap != null) gaps.remove(gap.getGapCoord());}
    @Override public void addProcessor(Processor<G> processor) {processors.add(processor);}
    @Override public void removeProcessor(Processor<G> processor) {processors.remove(processor);}
    @Override public void addEmitter(Emitter<G> emitter) {emitters.add(emitter);}
    @Override public void removeEmitter(Emitter<G> emitter) {emitters.remove(emitter);}

    @Override public void schedule(Actor<G> actor, int delay) {
        if (actor == null) throw new IllegalArgumentException();
        if (delay < 1) throw new IllegalArgumentException();
        getSchedule(tick + delay).add(actor);
    }
    private Set<Actor<G>> getSchedule(int tick) {return schedule.computeIfAbsent(tick, k -> new LinkedHashSet<>());}

    @Override public G g() {return grid;}
}
