package work.connor.zorcs.server.game.stimulus;

import work.connor.zorcs.server.game.event.VisualEvent;
import work.connor.zorcs.server.game.space.LocalObserver;
import work.connor.zorcs.server.grid.general.Grid;
import work.connor.zorcs.server.grid.relative.LocationCoord;
import work.connor.zorcs.server.grid.relative.Orientation;

import java.util.Map;

public class SimpleVisualStimulus<G extends Grid<G>> implements VisualStimulus<G> {
    private LocationCoord<G,?> coord;
    private Orientation<G,?> orientation;
    private String type, appearance;
    private Map<String,String> details;
    public SimpleVisualStimulus(VisualEvent<G> event, LocalObserver<G> observer) {
        this.orientation  = observer.getSys().relative(event.getOrientation());
        this.type = event.getType();
        this.appearance = event.getAppearance();
        this.details = event.getDetails();
    }
    @Override public LocationCoord<G,?> getCoord() {return coord;}
    @Override public void setCoord(LocationCoord<G,?> coord) {this.coord = coord;}
    @Override public Orientation<G,?> getOrientation() {return orientation;}
    @Override public String getType() {return type;}
    @Override public String getAppearance() {return appearance;}
    @Override public Map<String,String> getDetails() {return details;}
}
