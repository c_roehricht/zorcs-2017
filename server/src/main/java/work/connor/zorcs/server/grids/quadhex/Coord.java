package work.connor.zorcs.server.grids.quadhex;

import work.connor.zorcs.server.grids.flat.*;

public interface Coord<G  extends QuadHexGrid<G,CC,GC,FC,A>,
                       C  extends Coord<G,C,CC,GC,FC,A>,
                       CC extends CellCoord<G,CC,GC,FC,A>,
                       GC extends GapCoord<G,CC,GC,FC,A>,
                       FC extends FCoord<G,CC,GC,FC,A>,
                       A  extends Angle<G,CC,GC,FC,A>>
        extends work.connor.zorcs.server.grids.flat.Coord<G,C,CC,GC,FC,A>,
                SpatialValue<G,C,CC,GC,FC,A> {
}
