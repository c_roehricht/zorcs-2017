package work.connor.zorcs.server.game.space;

import work.connor.zorcs.server.grid.general.Grid;
import work.connor.zorcs.server.grid.absolute.*;

public class DefaultGap<G extends Grid<G>> extends DefaultLocation<G>
        implements Gap<G> {
    protected GapCoord<G,?> coord;
    public DefaultGap(Space<G> space, GapCoord<G,?> coord) {
        super(space);
        installAsGap(space, coord);
    }
    @Override public GapCoord<G,?> getGapCoord() {return coord;}
    @Override public void setSpaceCoord(Space<G> space, GapCoord<G,?> coord) {
        this.space = space;
        this.coord = coord;
    }
}
