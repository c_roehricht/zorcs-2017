package work.connor.zorcs.server.grid.general;

public interface FCoord<G  extends Grid<G>,
                        FC extends FCoord<G,FC>>
        extends work.connor.zorcs.server.grid.absolute.FCoord<G,FC>,
                work.connor.zorcs.server.grid.relative.FCoord<G,FC>,
                Coord<G,FC> {
}
