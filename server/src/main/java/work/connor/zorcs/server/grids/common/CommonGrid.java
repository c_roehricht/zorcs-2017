package work.connor.zorcs.server.grids.common;

import org.json.JSONObject;
import work.connor.zorcs.server.grid.general.Grid;
import work.connor.zorcs.server.utility.SetU;
import work.connor.zorcs.server.utility.StreamU;

import java.util.Set;
import java.util.stream.Stream;

public interface CommonGrid<G  extends CommonGrid<G,CC,GC,FC,D,O>,
                            CC extends CellCoord<G,CC,GC,FC,D,O>,
                            GC extends GapCoord<G,CC,GC,FC,D,O>,
                            FC extends FCoord<G,CC,GC,FC,D,O>,
                            D  extends Direction<G,CC,GC,FC,D,O>,
                            O  extends Orientation<G,CC,GC,FC,D,O>>
        extends Grid<G> {
    interface Based<G  extends CommonGrid<G,CC,GC,FC,D,O>,
                    CC extends CellCoord<G,CC,GC,FC,D,O>,
                    GC extends GapCoord<G,CC,GC,FC,D,O>,
                    FC extends FCoord<G,CC,GC,FC,D,O>,
                    D  extends Direction<G,CC,GC,FC,D,O>,
                    O  extends Orientation<G,CC,GC,FC,D,O>>
            extends Grid.Based<G> {}
    @Override CC unitForward();
    @Override D forward();
    @Override default D behind() {return forward().opposite();}
    @Override O noRotation();
    GC gapCoord(CC cc);
    boolean gInvariant(CC cc);
    @Override CC origin();
    @Override default Set<? extends D> directions() {return forward().congruences();}
    @Override Set<? extends O> orientations();
    @Override Set<? extends CC> euclideanSquareSector(int dSq);
    @Override Set<? extends CC> manhattanSector(int d);
    @Override Set<? extends CC> chebyshevSector(int d);
    @Override default Stream<? extends CC> euclideanBox() {
        return StreamU.flatMapToObj(StreamU.naturals(),
                i -> SetU.flatMap(euclideanSquareSector(i), CellCoord::congruences).stream());
    }
    @Override default Stream<? extends CC> euclideanBox(double max) {
        return euclideanBox().takeWhile(cc -> Double.compare(cc.euclidean(), max) <= 0);
    }
    @Override default Stream<? extends LocationCoord<G,?,CC,GC,FC,D,O>> euclideanCoordBox(double max) {
        return euclideanBox(max * 2).flatMap(cc -> cc.half().stream());
    }
    default Stream<? extends CC> manhattanBox() {
        return StreamU.flatMapToObj(StreamU.naturals(),
                i -> SetU.flatMap(manhattanSector(i), CellCoord::congruences).stream());
    }
    default Stream<? extends CC> manhattanBox(int max) {return manhattanBox().takeWhile(cc -> cc.manhattan() <= max);}
    default Stream<? extends LocationCoord<G,?,CC,GC,FC,D,O>> manhattanCoordBox(int max) {
        return manhattanBox(max * 2).flatMap(cc -> cc.half().stream());
    }
    default Stream<? extends CC> chebyshevBox() {
        return StreamU.flatMapToObj(StreamU.naturals(),
                i -> SetU.flatMap(chebyshevSector(i), CellCoord::congruences).stream());
    }
    default Stream<? extends CC> chebyshevBox(int max) {return chebyshevBox().takeWhile(cc -> cc.chebyshev() <= max);}
    default Stream<? extends LocationCoord<G,?,CC,GC,FC,D,O>> chebyshevCoordBox(int max) {
        return chebyshevBox(max * 2).flatMap(cc -> cc.half().stream());
    }
    @SuppressWarnings("unchecked")
    @Override default Sys<G,?,CC,GC,FC,D,O> system(work.connor.zorcs.server.grid.absolute.CellCoord<G, ?> cc,
                                                   work.connor.zorcs.server.grid.absolute.Orientation<G, ?> o) {
        return system((CC) cc, (O) o);
    }
    Sys<G,?,CC,GC,FC,D,O> system(CC cc, O o);
    @Override CC cellCoord(JSONObject input);
    @Override default GC gapCoord(JSONObject input) {return gapCoord(cellCoord(input.getJSONObject("cc")));}
}
