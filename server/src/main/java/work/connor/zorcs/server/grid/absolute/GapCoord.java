package work.connor.zorcs.server.grid.absolute;

public interface GapCoord<G  extends Grid<G>,
                          GC extends GapCoord<G,GC>>
        extends work.connor.zorcs.server.grid.GapCoord<G,GC>,
                LocationCoord<G,GC> {
    @Override work.connor.zorcs.server.grid.relative.GapCoord<?,?> toRelative(Sys<G,?> sy);
}
