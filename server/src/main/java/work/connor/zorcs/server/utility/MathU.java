package work.connor.zorcs.server.utility;

import java.util.Optional;

public class MathU {
    public static int max(int... values) {
        if (values.length == 0) throw new IllegalArgumentException();
        int max = values[0];
        for (int i = 1; i < values.length; i++) max = Math.max(max, values[i]);
        return max;
    }
    public static int min(int... values) {
        if (values.length == 0) throw new IllegalArgumentException();
        int min = values[0];
        for (int i = 1; i < values.length; i++) min = Math.min(min, values[i]);
        return min;
    }
    public static double max(double... values) {
        if (values.length == 0) throw new IllegalArgumentException();
        double max = values[0];
        for (int i = 1; i < values.length; i++) max = Math.max(max, values[i]);
        return max;
    }
    public static double min(double... values) {
        if (values.length == 0) throw new IllegalArgumentException();
        double min = values[0];
        for (int i = 1; i < values.length; i++) min = Math.min(min, values[i]);
        return min;
    }
    public static int isqrt(double value) {return (int) Math.sqrt(value);}
    public static Optional<Integer> intSqrt(int value) {
        int root = isqrt(value);
        return Optional.ofNullable(root * root == value ? root : null);
    }
}
