package work.connor.zorcs.server.grid.absolute;

public interface Sys<G  extends Grid<G>,
                     Sy extends Sys<G,Sy>>
        extends SpatialValue<G,Sy> {
    CellCoord<G,?> cellCoord();
    Orientation<G,?> orientation();
    Sy negate();
    Coord<G,?> absolute(work.connor.zorcs.server.grid.relative.Coord<?,?> cr);
    LocationCoord<G,?> absolute(work.connor.zorcs.server.grid.relative.LocationCoord<?,?> lcr);
    CellCoord<G,?> absolute(work.connor.zorcs.server.grid.relative.CellCoord<?,?> ccr);
    GapCoord<G,?> absolute(work.connor.zorcs.server.grid.relative.GapCoord<?,?> gcr);
    FCoord<G,?> absolute(work.connor.zorcs.server.grid.relative.FCoord<?,?> fcr);
    Direction<G,?> absolute(work.connor.zorcs.server.grid.relative.Direction<?,?> dr);
    Orientation<G,?> absolute(work.connor.zorcs.server.grid.relative.Orientation<?,?> or);
    work.connor.zorcs.server.grid.relative.Coord<?,?> relative(Coord<G,?> ca);
    work.connor.zorcs.server.grid.relative.LocationCoord<?,?> relative(LocationCoord<G,?> lca);
    work.connor.zorcs.server.grid.relative.CellCoord<?,?> relative(CellCoord<G,?> cca);
    work.connor.zorcs.server.grid.relative.GapCoord<?,?> relative(GapCoord<G,?> gca);
    work.connor.zorcs.server.grid.relative.FCoord<?,?> relative(FCoord<G,?> fca);
    work.connor.zorcs.server.grid.relative.Direction<?,?> relative(Direction<G,?> da);
    work.connor.zorcs.server.grid.relative.Orientation<?,?> relative(Orientation<G,?> oa);

    interface Based<G extends Grid<G>> {Sys<G,?> getSys();}
}
