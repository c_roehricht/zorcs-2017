package work.connor.zorcs.server.game.stimulus;

import org.json.JSONObject;
import work.connor.zorcs.server.grid.Grid;

public class ActionCompleteStimulus<G extends Grid<G>> extends ActionStimulus<G> {
    public ActionCompleteStimulus(JSONObject command) {super(command);}
    @Override public String getName() {return "action-complete";}
}
