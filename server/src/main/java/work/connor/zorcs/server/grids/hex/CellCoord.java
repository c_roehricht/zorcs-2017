package work.connor.zorcs.server.grids.hex;

import work.connor.zorcs.server.utility.MathU;

public class CellCoord
        extends work.connor.zorcs.server.grids.quadhex.CellCoord<Hex, CellCoord,GapCoord,FCoord,Angle>
        implements LocationCoord<CellCoord> {
    public CellCoord(int a, int b) {super(a, b);}
    public int getC() {return -a - b;}
    @Override public int manhattan() {return (Math.abs(a) + Math.abs(b) + Math.abs(-a - b)) / 2;}
    @Override public int euclideanSquare() {return a * a + b * b + a * b;}
    @Override public int chebyshev() {
        return MathU.min(Math.max(Math.abs(a), Math.abs(b)), Math.max(Math.abs(a), Math.abs(getC())),
            Math.max(Math.abs(b), Math.abs(getC())));
    }
    @Override public CellCoord rotateOne() {return new CellCoord(-b, a + b);}
    @Override public CellCoord mirror() {return new CellCoord(a + b, -b);}
}
