package work.connor.zorcs.server.game.event;

import work.connor.zorcs.server.grid.absolute.*;

import java.util.Map;

public class VisualEvent<G extends Grid<G>> implements Event<G> {
    private Orientation<G,?> orientation = null;
    private String type;
    private String appearance;
    private Map<String,String> details;
    public VisualEvent(Orientation<G,?> orientation, String type, String appearance, Map<String,String> details) {
        this.orientation = orientation;
        this.type = type;
        this.appearance = appearance;
        this.details = details;
    }
    public Orientation<G,?> getOrientation() {return orientation;}
    public String getType() {return type;}
    public String getAppearance() {return appearance;}
    public Map<String, String> getDetails() {return details;}
}
