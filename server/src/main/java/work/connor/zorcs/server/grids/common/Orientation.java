package work.connor.zorcs.server.grids.common;

import work.connor.zorcs.server.grid.absolute.Sys;

public interface Orientation<G  extends CommonGrid<G,CC,GC,FC,D,O>,
                             CC extends CellCoord<G,CC,GC,FC,D,O>,
                             GC extends GapCoord<G,CC,GC,FC,D,O>,
                             FC extends FCoord<G,CC,GC,FC,D,O>,
                             D  extends Direction<G,CC,GC,FC,D,O>,
                             O  extends Orientation<G,CC,GC,FC,D,O>>
        extends work.connor.zorcs.server.grid.general.Orientation<G,O>,
                SpatialValue<G,O,CC,GC,FC,D,O> {
    @Override default O toAbsolute(Sys<?,?> sy) {return SpatialValue.super.toAbsolute(sy);}
    @Override default O toRelative(Sys<G,?> sy) {return SpatialValue.super.toRelative(sy);}
    @Override default O translate(CC cc) {return concrete();}
}
