package work.connor.zorcs.server.grids.tape;

public class GapCoord
        extends work.connor.zorcs.server.grids.flat.GapCoord<Tape,CellCoord,GapCoord,FCoord,Angle>
        implements LocationCoord<GapCoord> {
    public GapCoord(CellCoord cellCoord) {super(cellCoord);}
}
