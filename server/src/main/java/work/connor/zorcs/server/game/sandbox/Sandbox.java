package work.connor.zorcs.server.game.sandbox;

import org.json.JSONObject;
import work.connor.zorcs.server.game.actor.Actor;
import work.connor.zorcs.server.game.component.*;
import work.connor.zorcs.server.game.entity.DefaultEntity;
import work.connor.zorcs.server.game.entity.Entity;
import work.connor.zorcs.server.game.intent.Intent;
import work.connor.zorcs.server.game.space.DefaultSpace;
import work.connor.zorcs.server.game.space.GroundCell;
import work.connor.zorcs.server.game.space.GroundGap;
import work.connor.zorcs.server.game.space.Space;
import work.connor.zorcs.server.grid.general.Grid;
import work.connor.zorcs.server.grid.absolute.CellCoord;
import work.connor.zorcs.server.grid.absolute.GapCoord;
import work.connor.zorcs.server.grids.quad.Quad;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Random;
import java.util.stream.Collectors;

public class Sandbox<G extends Grid<G>>
        implements Quad.Based {
    static class DungeonGenerator {
        public static boolean[][] generate(int aWidth, int bWidth,
                                           double aliveProb, int birthThreshold, int survivalTreshold,
                                           int iterations) {
            boolean[][] map = initialize(aWidth, bWidth, aliveProb);
            for (int i = 0; i < iterations; i++) map = grow(map, birthThreshold, survivalTreshold);
            return map;
        }

        private static boolean[][] initialize(int aWidth, int bWidth, double aliveProb) {
            boolean[][] map = new boolean[aWidth][bWidth];
            Random random = new Random(0);
            for (int a = 0; a < map.length; a++) {
                for (int b = 0; b < map[a].length; b++) {
                    map[a][b] = random.nextDouble() < aliveProb;
                }
            }
            return map;
        }

        private static boolean[][] grow(boolean[][] map, int birthThreshold, int survivalTreshold) {
            boolean[][] map2 = new boolean[map.length][map[0].length];
            Random random = new Random(0);
            for (int a = 0; a < map.length; a++) {
                for (int b = 0; b < map[a].length; b++) {
                    int count = 0;
                    if (testCell(map, a - 1, b - 1)) count++;
                    if (testCell(map, a - 1, b + 0)) count++;
                    if (testCell(map, a - 1, b + 1)) count++;
                    if (testCell(map, a + 0, b - 1)) count++;
                    if (testCell(map, a + 0, b + 1)) count++;
                    if (testCell(map, a + 1, b - 1)) count++;
                    if (testCell(map, a + 1, b + 0)) count++;
                    if (testCell(map, a + 1, b + 1)) count++;
                    if (map[a][b]) map2[a][b] = count >= survivalTreshold;
                    else map2[a][b] = count >= birthThreshold;
                }
            }
            return map2;
        }

        private static boolean testCell(boolean[][] map, int a, int b) {
            if (a >= 0 && a < map.length && b >= 0 && b < map[a].length) return map[a][b];
            return true;
        }
    }

    public static <G extends Grid<G>> Intent<G> spawnCell(CellCoord<G,?> coord) {
        return Intent.lambda(space -> {new GroundCell<>(space, coord, "stone");});
    }
    public static <G extends Grid<G>> Intent<G> spawnGap(GapCoord<G,?> coord) {
        return Intent.lambda(space -> {new GroundGap<>(space, coord, "stone");});
    }

    public static void main(String[] args) {
        new Sandbox<>().go(args);
    }
    public void go(String[] args) {
        Space<Quad> space = new DefaultSpace<>(Quad.QUAD);
        Actor<Quad> generator = (space1, intents) -> {
            boolean[][] map = DungeonGenerator.generate(70, 70, 0.47, 5, 4, 5);
            for (int a = 0; a < map.length; a++) {
                for (int b = 0; b < map[a].length; b++) {
                    if (map[a][b]) continue;
                    CellCoord coord = g().cellCoord(a, b);
                    if (a == 0 || !map[a - 1][b]) intents.accept(spawnGap(coord.nextGap(g().behind())));
                    if (b == 0 || !map[a][b - 1]) intents.accept(spawnGap(coord.nextGap(g().left())));
                    intents.accept(spawnCell(coord));
                    intents.accept(spawnGap(coord.nextGap(g().forward())));
                    intents.accept(spawnGap(coord.nextGap(g().right())));
                }
            }
        };
        space.schedule(generator, 1);
        space.nextTick();

        BodyComponent<Quad> body = new SimpleBodyComponent<>(space.cells().iterator().next(), g().forward(), "blue");
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        RemoteBrainComponent<Quad> brain = new RemoteBrainComponent<>(
                () -> {
                    try {return new JSONObject(in.readLine());}
                    catch (IOException e) {throw new RuntimeException(e);}},
                // Yes, we can only read single-line JSON because this library sucks
                System.out::println);
        TimeComponent<Quad> time = new TimeComponent<>(brain, body);
        SimpleVisionComponent<Quad> vision = new SimpleVisionComponent<>(brain, body,
                g().euclideanCoordBox(2).collect(Collectors.toSet()));
        WalkComponent<Quad> walk = new WalkComponent<>(brain, body);
        Entity<Quad> player = new DefaultEntity<>(body, time, brain, vision, walk);
        space.schedule(brain, 2);

        while (true) {
            space.nextTick();
        }
    }
}
