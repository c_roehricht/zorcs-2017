package work.connor.zorcs.server.game.space;

import work.connor.zorcs.server.grid.general.Grid;
import work.connor.zorcs.server.grid.absolute.*;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

public class DefaultCell<G extends Grid<G>> extends DefaultLocation<G>
        implements Cell<G> {
    protected CellCoord<G,?> coord;
    private Map<Body<G>, Orientation<G,?>> bodies = new LinkedHashMap<>();
    public DefaultCell(Space<G> space, CellCoord<G,?> coord) {
        super(space);
        installAsCell(space, coord);
    }
    @Override public CellCoord<G,?> getCellCoord() {return coord;}
    @Override public void setSpaceCoord(Space<G> space, CellCoord<G,?> coord) {
        this.space = space;
        this.coord = coord;
    }
    @Override public Set<Body<G>> getBodies() {return bodies.keySet();}
    @Override public void addBody(Body<G> body, Orientation<G,?> o) {if (o != null) bodies.put(body, o);}
    @Override public void removeBody(Body<G> body) {bodies.remove(body);}
    @Override public Orientation<G,?> getOrientation(Body<G> body) {return bodies.get(body);}
}
