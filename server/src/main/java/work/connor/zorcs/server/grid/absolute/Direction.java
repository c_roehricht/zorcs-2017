package work.connor.zorcs.server.grid.absolute;

public interface Direction<G extends Grid<G>,
                           D extends Direction<G,D>>
        extends work.connor.zorcs.server.grid.Direction<G,D>,
                SpatialValue<G,D> {
    @Override work.connor.zorcs.server.grid.relative.Direction<?,?> toRelative(Sys<G,?> sy);
}
