package work.connor.zorcs.server.grids.common;

import work.connor.zorcs.server.grid.absolute.Sys;
import work.connor.zorcs.server.utility.OptU;
import work.connor.zorcs.server.utility.SetU;

import java.util.Optional;
import java.util.Set;

public interface CellCoord<G  extends CommonGrid<G,CC,GC,FC,D,O>,
                           CC extends CellCoord<G,CC,GC,FC,D,O>,
                           GC extends GapCoord<G,CC,GC,FC,D,O>,
                           FC extends FCoord<G,CC,GC,FC,D,O>,
                           D  extends Direction<G,CC,GC,FC,D,O>,
                           O  extends Orientation<G,CC,GC,FC,D,O>>
        extends work.connor.zorcs.server.grid.general.CellCoord<G,CC>,
                LocationCoord<G,CC,CC,GC,FC,D,O> {
    @Override default CC toAbsolute(Sys<?,?> sy) {return LocationCoord.super.toAbsolute(sy);}
    @Override default CC toRelative(Sys<G,?> sy) {return LocationCoord.super.toRelative(sy);}
    @Override default Optional<? extends LocationCoord<G,?,CC,GC,FC,D,O>> half() {
        return OptU.or(halfCell(), this::halfGap);
    }
    @Override default Optional<? extends GC> halfGap() {
        if (!g().gInvariant(concrete())) return Optional.empty();
        else return Optional.of(g().gapCoord(concrete()));
    }
    @SuppressWarnings("unchecked")
    @Override default GC nextGap(work.connor.zorcs.server.grid.Direction<G,?> d) {return nextGap((D) d);}
    default GC nextGap(D d) {return d.unitGap().add(this);}
    @Override default Set<? extends GC> neighbourGaps(Set<? extends work.connor.zorcs.server.grid.Direction<G, ?>> ds) {
        return SetU.map(ds, this::nextGap);
    }
    @Override default Set<? extends GC> neighbourGaps() {return neighbourGaps(g().directions());}
}
