package work.connor.zorcs.server.grid.general;

public interface Coord<G extends Grid<G>,
                       C extends Coord<G,C>>
        extends work.connor.zorcs.server.grid.absolute.Coord<G,C>,
                work.connor.zorcs.server.grid.relative.Coord<G,C>,
                SpatialValue<G,C> {
}
