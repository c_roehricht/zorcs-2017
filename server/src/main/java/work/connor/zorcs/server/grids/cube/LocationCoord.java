package work.connor.zorcs.server.grids.cube;

public interface LocationCoord<LC extends LocationCoord<LC>>
        extends work.connor.zorcs.server.grids.common.LocationCoord<Cube,LC,CellCoord,GapCoord,FCoord,Direction,Orientation>,
        Coord<LC> {
}
