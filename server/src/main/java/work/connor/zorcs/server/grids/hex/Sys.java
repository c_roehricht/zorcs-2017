package work.connor.zorcs.server.grids.hex;

public class Sys
        extends work.connor.zorcs.server.grids.common.Sys<Hex,Sys,CellCoord,GapCoord,FCoord,Angle,Angle>
        implements Hex.Based {
    public Sys(CellCoord cc, Angle a) {super(cc, a);}
}
