package work.connor.zorcs.server.game.sandbox;

import work.connor.zorcs.server.game.component.BodyComponent;
import work.connor.zorcs.server.game.entity.DefaultEntity;
import work.connor.zorcs.server.grid.general.Grid;

public class SimpleItem<G extends Grid<G>> extends DefaultEntity<G> {
    public SimpleItem(BodyComponent<G> component) {super(component);}

}
