package work.connor.zorcs.server.grids.quadhex;

import org.json.JSONObject;
import work.connor.zorcs.server.grids.flat.Angle;

import java.util.Objects;
import java.util.Optional;

public abstract class CellCoord<G  extends QuadHexGrid<G,CC,GC,FC,A>,
                                CC extends CellCoord<G,CC,GC,FC,A>,
                                GC extends GapCoord<G,CC,GC,FC,A>,
                                FC extends FCoord<G,CC,GC,FC,A>,
                                A  extends Angle<G,CC,GC,FC,A>>
        implements work.connor.zorcs.server.grids.flat.CellCoord<G,CC,GC,FC,A>,
                   LocationCoord<G,CC,CC,GC,FC,A> {
    public final int a, b;
    protected CellCoord(int a, int b) {this.a = a; this.b = b;}
    @Override public CC multiply(int factor) {return g().cellCoord(a * factor, b * factor);}
    @Override public Optional<? extends CC> halfCell() {
        if (a % 2 == 0 && b % 2 == 0) return Optional.of(g().cellCoord(a / 2, b / 2));
        else return Optional.empty();
    }
    @Override public FC floating() {return g().fCoord(a, b);}
    @Override public CC add(CC cc) {return g().cellCoord(a + cc.a, b + cc.b);}
    @Override public CC negate() {return g().cellCoord(-a, -b);}
    @Override public boolean equals(Object o) {if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CellCoord<?, ?, ?, ?, ?> cellCoord = (CellCoord<?, ?, ?, ?, ?>) o;
        return a == cellCoord.a &&
                b == cellCoord.b;
    }
    @Override public int hashCode() {return Objects.hash(a, b);}
    @Override public JSONObject toRemote(JSONObject output) {
        output = LocationCoord.super.toRemote(output);
        output.put("a", a);
        return output.put("b", b);
    }
}
