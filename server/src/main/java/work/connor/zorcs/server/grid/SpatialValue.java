package work.connor.zorcs.server.grid;

import work.connor.zorcs.server.utility.SetU;

import java.util.Set;

public interface SpatialValue<G extends Grid<G>,
                              V extends SpatialValue<G,V>>
        extends Grid.Based<G> {
    default V concrete() {return concrete(this);}
    @SuppressWarnings("unchecked")
    default V concrete(SpatialValue<G,?> v) {return (V) v;}
    V translate(CellCoord<G,?> cc);
    V rotate(Orientation<G,?> o);
    V mirror();
    default Set<? extends V> mirrors() {return SetU.ofDupls(this.concrete(), mirror());}
    default Set<? extends V> congruences() {
        return SetU.flatMap(mirrors(), r -> SetU.map(g().orientations(), r::rotate));
    }
}
