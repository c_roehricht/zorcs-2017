package work.connor.zorcs.server.grid.relative;

import work.connor.zorcs.server.game.protocol.RemoteOutput;
import work.connor.zorcs.server.grid.absolute.Sys;

public interface Direction<G extends Grid<G>,
                           D extends Direction<G,D>>
        extends work.connor.zorcs.server.grid.Direction<G,D>,
                SpatialValue<G,D>,
                RemoteOutput {
    @Override work.connor.zorcs.server.grid.absolute.Direction<?,?> toAbsolute(Sys<?,?> sy);
    @Override CellCoord<G,?> unit();
    @Override GapCoord<G,?> unitGap();
}
