package work.connor.zorcs.server.grids.quad;

public class CellCoord
        extends work.connor.zorcs.server.grids.quadhex.CellCoord<Quad,CellCoord,GapCoord,FCoord,Angle>
        implements LocationCoord<CellCoord> {
    public CellCoord(int a, int b) {super(a, b);}
    @Override public int manhattan() {return Math.abs(a) + Math.abs(b);}
    @Override public int euclideanSquare() {return a * a + b * b;}
    @Override public int chebyshev() {return Math.max(Math.abs(a), Math.abs(b));}
    @Override public CellCoord rotateOne() {return new CellCoord(-b, a);}
    @Override public CellCoord mirror() {return new CellCoord(a, -b);}
}
