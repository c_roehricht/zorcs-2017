package work.connor.zorcs.server.grids.plane_containing;

import work.connor.zorcs.server.grids.common.*;

public interface PlaneContainingGrid<G  extends PlaneContainingGrid<G,CC,GC,FC,D,O>,
                                     CC extends CellCoord<G,CC,GC,FC,D,O>,
                                     GC extends GapCoord<G,CC,GC,FC,D,O>,
                                     FC extends FCoord<G,CC,GC,FC,D,O>,
                                     D  extends Direction<G,CC,GC,FC,D,O>,
                                     O  extends Orientation<G,CC,GC,FC,D,O>>
        extends CommonGrid<G,CC,GC,FC,D,O> {
    interface Based<G  extends PlaneContainingGrid<G,CC,GC,FC,D,O>,
                    CC extends CellCoord<G,CC,GC,FC,D,O>,
                    GC extends GapCoord<G,CC,GC,FC,D,O>,
                    FC extends FCoord<G,CC,GC,FC,D,O>,
                    D  extends Direction<G,CC,GC,FC,D,O>,
                    O  extends Orientation<G,CC,GC,FC,D,O>>
            extends CommonGrid.Based<G,CC,GC,FC,D,O> {}
    O oLeft();
    default O oRight() {return oLeft().mirror();}
    default D left() {return forward().rotate(oLeft());}
    default D right() {return forward().rotate(oRight());}
}
