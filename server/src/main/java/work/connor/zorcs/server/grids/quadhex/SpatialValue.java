package work.connor.zorcs.server.grids.quadhex;

import work.connor.zorcs.server.grids.flat.*;

public interface SpatialValue<G  extends QuadHexGrid<G,CC,GC,FC,A>,
                              V  extends SpatialValue<G,V,CC,GC,FC,A>,
                              CC extends CellCoord<G,CC,GC,FC,A>,
                              GC extends GapCoord<G,CC,GC,FC,A>,
                              FC extends FCoord<G,CC,GC,FC,A>,
                              A  extends Angle<G,CC,GC,FC,A>>
        extends work.connor.zorcs.server.grids.flat.SpatialValue<G,V,CC,GC,FC,A>,
                QuadHexGrid.Based<G,CC,GC,FC,A> {
}
