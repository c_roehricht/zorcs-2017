package work.connor.zorcs.server.game.stimulus;

import org.json.JSONObject;
import work.connor.zorcs.server.grid.Grid;

public class TimeStimulus<G extends Grid<G>> implements Stimulus<G> {
    private int ticks;
    public TimeStimulus(int ticks) {this.ticks = ticks;}
    @Override public String getName() {return "time";}
    @Override public JSONObject toRemote(JSONObject output) {
        output = Stimulus.super.toRemote(output);
        return output.put("ticks", ticks);
    }
}
