package work.connor.zorcs.server.grids.flat;

import work.connor.zorcs.server.grids.common.*;

public abstract class GapCoord<G  extends FlatGrid<G,CC,GC,FC,A>,
                               CC extends CellCoord<G,CC,GC,FC,A>,
                               GC extends GapCoord<G,CC,GC,FC,A>,
                               FC extends FCoord<G,CC,GC,FC,A,A>,
                               A  extends Angle<G,CC,GC,FC,A>>
        extends work.connor.zorcs.server.grids.common.GapCoord<G,CC,GC,FC,A,A>
        implements SpatialValue<G,GC,CC,GC,FC,A> {
    protected GapCoord(CC cc) {super(cc);}
    @Override public GC rotateOne() {return g().gapCoord(cc.rotateOne());}
}
