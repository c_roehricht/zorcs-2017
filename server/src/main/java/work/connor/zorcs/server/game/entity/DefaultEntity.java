package work.connor.zorcs.server.game.entity;

import work.connor.zorcs.server.game.component.Component;
import work.connor.zorcs.server.grid.general.Grid;

import java.util.LinkedHashSet;
import java.util.Set;

public class DefaultEntity<G extends Grid<G>> implements Entity<G> {
    private Set<Component<G>> components = new LinkedHashSet<>();
    public DefaultEntity(Component<G>... components) {
        for (Component<G> component : components) component.installAsComponent(this);
    }
    @Override public Set<Component<G>> getComponents() {return components;}
    @Override public void addComponent(Component<G> component) {components.add(component);}
    @Override public void removeComponent(Component<G> component) {components.remove(component);}
}
