package work.connor.zorcs.server.game.component;

import work.connor.zorcs.server.game.actor.Actor;
import work.connor.zorcs.server.game.processor.StimulusProcessor;
import work.connor.zorcs.server.game.stimulus.Stimulus;
import work.connor.zorcs.server.grid.general.Grid;

import java.util.List;

public interface BrainComponent<G extends Grid<G>,
                                S extends Stimulus<G>>
    extends Component<G>,
            StimulusProcessor.Sink<G,S>,
            Actor<G> {
    void addActionComponent(List<String> actionNames, ActionComponent<G> component);
    void removeActionComponent(List<String> actionNames, ActionComponent<G> component);

    interface GenericBased<G extends Grid<G>> {
        BrainComponent<G,?> getBrainComp();
    }
    interface Based<G extends Grid<G>, S extends Stimulus<G>> {
        BrainComponent<G, ? super S> getBrainComp();
    }
    interface GenericMapped<G extends Grid<G>> extends GenericBased<G> {
        void setBrainComp(BrainComponent<G,?> brainComp);
    }
    interface Mapped<G extends Grid<G>, S extends Stimulus<G>> extends Based<G,S> {
        void setBrainComp(BrainComponent<G, ? super S> brainComp);
    }
}
