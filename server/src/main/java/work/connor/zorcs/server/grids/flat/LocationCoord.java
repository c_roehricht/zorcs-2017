package work.connor.zorcs.server.grids.flat;

import work.connor.zorcs.server.grids.common.*;

public interface LocationCoord<G   extends FlatGrid<G,CC,GC,FC,A>,
                               LCs extends LocationCoord<G,LCs,CC,GC,FC,A>,
                               CC  extends CellCoord<G,CC,GC,FC,A>,
                               GC  extends GapCoord<G,CC,GC,FC,A>,
                               FC  extends FCoord<G,CC,GC,FC,A,A>,
                               A   extends Angle<G,CC,GC,FC,A>>
        extends work.connor.zorcs.server.grids.common.LocationCoord<G,LCs,CC,GC,FC,A,A>,
                Coord<G,LCs,CC,GC,FC,A> {
}
