package work.connor.zorcs.server.grids.cube;

public interface Coord<C extends Coord<C>>
        extends work.connor.zorcs.server.grids.common.Coord<Cube,C,CellCoord,GapCoord,FCoord,Direction,Orientation>,
                SpatialValue<C> {
}
