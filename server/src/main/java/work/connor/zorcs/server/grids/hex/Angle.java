package work.connor.zorcs.server.grids.hex;

public class Angle extends work.connor.zorcs.server.grids.flat.Angle<Hex,CellCoord,GapCoord,FCoord, Angle>
        implements SpatialValue<Angle> {
    public Angle(int r) {super(r);}
}
