package work.connor.zorcs.server.utility;

import java.util.Iterator;

public class IterU {
    public static class Concat<T> implements Iterator<T> {
        Iterator<? extends T> i1, i2;
        public Concat(Iterator<? extends T> i1, Iterator<? extends T> i2) {this.i1 = i1; this.i2 = i2;}
        @Override public boolean hasNext() {return i1.hasNext() || i2.hasNext();}
        @Override public T next() {return i1.hasNext() ? i1.next() : i2.next();}
    }
    public static <T> Iterator<T> concat(Iterator<? extends T> i1, Iterator<? extends T> i2) {
        return new Concat<>(i1, i2);
    }
    public static <T> Iterator<T> concat(Iterable<? extends T> i1, Iterable<? extends T> i2) {
        return concat(i1.iterator(), i2.iterator());
    }
}
