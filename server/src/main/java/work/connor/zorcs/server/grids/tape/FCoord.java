package work.connor.zorcs.server.grids.tape;

public class FCoord
        implements work.connor.zorcs.server.grids.common.FCoord<Tape,CellCoord,GapCoord,FCoord,Angle,Angle>,
                   Coord<FCoord> {
    public final double a;
    public FCoord(double a) {this.a = a;}
    @Override public FCoord multiply(double factor) {return new FCoord(a * factor);}
    @Override public FCoord negate() {return new FCoord(-a);}
    @Override public double floatingManhattan() {return a;}
    @Override public double euclidean() {return a;}
    @Override public double floatingChebyshev() {return a;}
    @Override public FCoord add(FCoord fc) {return new FCoord(a + fc.a);}
    @Override public FCoord rotateOne() {return negate();}
    @Override public FCoord mirror() {return this;}
}
