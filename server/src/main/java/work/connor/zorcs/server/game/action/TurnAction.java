package work.connor.zorcs.server.game.action;

import work.connor.zorcs.server.game.component.BodyComponent;
import work.connor.zorcs.server.game.space.Space;
import work.connor.zorcs.server.grid.general.Grid;
import work.connor.zorcs.server.grid.relative.*;

public class TurnAction<G extends Grid<G>> extends BodyAction<G> {
    private Orientation<G,?> o;
    public TurnAction(BodyComponent<G> body, Orientation<G,?> o) {super(body); this.o = o;}
    @Override public void perform(Space<G> space) throws ActionFailedException {body.rotate(o);}
}
