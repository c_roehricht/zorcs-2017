package work.connor.zorcs.server.game.processor;

import work.connor.zorcs.server.game.event.VisualEvent;
import work.connor.zorcs.server.game.space.LocalObserver;
import work.connor.zorcs.server.game.space.Location;
import work.connor.zorcs.server.game.space.Space;
import work.connor.zorcs.server.game.stimulus.VisualStimulus;
import work.connor.zorcs.server.grid.general.Grid;
import work.connor.zorcs.server.grid.relative.*;

import java.util.Set;

public interface ViewProcessor<G extends Grid<G>>
        extends StimulusProcessor<G, VisualStimulus<G>>, LocalObserver.Based<G> {
    @Override default void process(Space<G> space) {
        for (LocationCoord<G,?> coord : fov()) {
            Location<G> location = getLocation(coord);
            if (location == null) continue;
            for (VisualEvent<G> event : location.getVisualEvents()) {
                VisualStimulus<G> stimulus = process(event);
                if (stimulus != null) {
                    stimulus.setCoord(coord);
                    output(stimulus);
                }
            }
        }
    }
    Set<LocationCoord<G,?>> fov();
    VisualStimulus<G> process(VisualEvent<G> event);
}
