package work.connor.zorcs.server.grids.cube;

public interface SpatialValue<V extends SpatialValue<V>>
        extends work.connor.zorcs.server.grids.common.SpatialValue<Cube,V,CellCoord,GapCoord,FCoord,Direction,Orientation>,
                Cube.Based {
}
