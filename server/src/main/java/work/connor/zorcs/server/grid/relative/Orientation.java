package work.connor.zorcs.server.grid.relative;

import work.connor.zorcs.server.game.protocol.RemoteOutput;
import work.connor.zorcs.server.grid.absolute.Sys;

public interface Orientation<G extends Grid<G>,
                             O extends Orientation<G,O>>
        extends work.connor.zorcs.server.grid.Orientation<G,O>,
                SpatialValue<G,O>,
                RemoteOutput {
    @Override work.connor.zorcs.server.grid.absolute.Orientation<?,?> toAbsolute(Sys<?,?> sy);
}
