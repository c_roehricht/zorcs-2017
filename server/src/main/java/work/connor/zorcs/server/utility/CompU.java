package work.connor.zorcs.server.utility;

import java.util.Comparator;
import java.util.Iterator;

public class CompU {
    public static <T> boolean isInOrder(Iterator<? extends T> iterator, Comparator<? super T> comparator) {
        T last = null;
        while (iterator.hasNext()) {
            T current = iterator.next();
            if (last != null && current != null && comparator.compare(last, current) > 0) return false;
        }
        return true;
    }
}
