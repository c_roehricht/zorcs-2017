package work.connor.zorcs.server.grids.flat;

import org.json.JSONObject;
import work.connor.zorcs.server.grids.common.*;

import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public interface FlatGrid<G  extends FlatGrid<G,CC,GC,FC,A>,
                          CC extends CellCoord<G,CC,GC,FC,A>,
                          GC extends GapCoord<G,CC,GC,FC,A>,
                          FC extends FCoord<G,CC,GC,FC,A,A>,
                          A  extends Angle<G,CC,GC,FC,A>>
        extends CommonGrid<G,CC,GC,FC,A,A> {
    interface Based<G  extends FlatGrid<G,CC,GC,FC,A>,
                    CC extends CellCoord<G,CC,GC,FC,A>,
                    GC extends GapCoord<G,CC,GC,FC,A>,
                    FC extends FCoord<G,CC,GC,FC,A,A>,
                    A  extends Angle<G,CC,GC,FC,A>>
            extends CommonGrid.Based<G,CC,GC,FC,A,A> {}
    A angle(int r);
    int circle();
    @Override default A forward() {return noRotation();}
    @Override default A noRotation() {return angle(0);}
    @Override default Set<? extends A> orientations() {return angles();}
    default Set<? extends A> angles() {
        return IntStream.rangeClosed(0, circle() - 1).mapToObj(this::angle).collect(Collectors.toSet());
    }
    @Override default A direction(JSONObject input) {return angle(input);}
    @Override default A orientation(JSONObject input) {return angle(input);}
    default A angle(JSONObject input) {return angle(input.getInt("r"));}
}
