package work.connor.zorcs.server.grid.absolute;

import work.connor.zorcs.server.grid.Direction;

import java.util.Set;

public interface CellCoord<G  extends Grid<G>,
                           CC extends CellCoord<G,CC>>
        extends work.connor.zorcs.server.grid.CellCoord<G,CC>,
                LocationCoord<G,CC> {
    @Override work.connor.zorcs.server.grid.relative.CellCoord<?,?> toRelative(Sys<G,?> sy);
    @Override GapCoord<G,?> nextGap(Direction<G,?> d);
    @Override Set<? extends GapCoord<G,?>> neighbourGaps();
}
