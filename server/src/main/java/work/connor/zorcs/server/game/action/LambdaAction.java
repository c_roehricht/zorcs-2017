package work.connor.zorcs.server.game.action;

import work.connor.zorcs.server.grid.general.Grid;
import work.connor.zorcs.server.game.space.Space;

import java.util.function.Consumer;

public class LambdaAction<G extends Grid<G>> implements Action<G> {
    private Consumer<Space<G>> lambda;
    public LambdaAction(Consumer<Space<G>> lambda) {this.lambda = lambda;}
    @Override public void perform(Space<G> space) throws ActionFailedException {lambda.accept(space);}
}
