package work.connor.zorcs.server.grids.tape;

public interface LocationCoord<LC extends LocationCoord<LC>>
        extends work.connor.zorcs.server.grids.flat.LocationCoord<Tape,LC,CellCoord,GapCoord,FCoord,Angle>,
                Coord<LC> {
}
