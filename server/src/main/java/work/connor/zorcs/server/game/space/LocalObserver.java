package work.connor.zorcs.server.game.space;

import work.connor.zorcs.server.grid.general.Grid;
import work.connor.zorcs.server.grid.general.Sys;
import work.connor.zorcs.server.grid.relative.*;

public interface LocalObserver<G extends Grid<G>> extends Sys.Based<G>,
                                                          Cell.Based<G> {
    work.connor.zorcs.server.grid.absolute.Orientation<G,?> getOrientation();
    @Override default Sys<G,?> getSys() {return g().system(getCell().getCellCoord(), getOrientation());}
    default Cell<G> getCell(CellCoord<G,?> coord) {return getSpace().getCell(getSys().absolute(coord));}
    default Gap<G> getGap(GapCoord<G,?> coord) {return getSpace().getGap(getSys().absolute(coord));}
    default Location<G> getLocation(LocationCoord<G,?> coord) {return getSpace().getLocation(getSys().absolute(coord));}

    interface Mobile<G extends Grid<G>> extends LocalObserver<G>,
                                                Cell.Mapped<G> {
        void setOrientation(work.connor.zorcs.server.grid.absolute.Orientation<G,?> orientation);
        default LocalObserver<G> move(CellCoord<G,?> coord) {
            setCell(getCell(coord));
            return this;
        }
        default LocalObserver<G> rotate(Orientation<G,?> o) { //TODO fix rel abs?
            setOrientation(getOrientation().rotate(o));
            return this;
        }
    }

    interface Based<G extends Grid<G>> extends LocalObserver<G> {
        LocalObserver<G> getLocalObserver();
        @Override default Cell<G> getCell() {return getLocalObserver().getCell();}
        @Override default work.connor.zorcs.server.grid.absolute.Orientation<G,?> getOrientation() {
            return getLocalObserver().getOrientation();
        }
    }
}
