package work.connor.zorcs.server.game.component;

import org.json.JSONObject;
import work.connor.zorcs.server.game.intent.Intent;
import work.connor.zorcs.server.game.stimulus.ActionStimulus;
import work.connor.zorcs.server.grid.general.Grid;

import java.util.List;
import java.util.function.Consumer;

public interface ActionComponent<G extends Grid<G>>
        extends Component<G>,
                BrainComponent.Mapped<G, ActionStimulus<G>> {
    List<String> getActionNames();
    void act(Consumer<Intent<G>> intents, JSONObject command);
    @Override default void componentInstaller() {getBrainComp().addActionComponent(getActionNames(),this);}
    @Override default void componentUninstaller() {getBrainComp().removeActionComponent(getActionNames(),this);}
}
