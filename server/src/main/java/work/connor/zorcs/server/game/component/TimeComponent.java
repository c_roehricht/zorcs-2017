package work.connor.zorcs.server.game.component;

import work.connor.zorcs.server.game.space.Space;
import work.connor.zorcs.server.game.stimulus.TimeStimulus;
import work.connor.zorcs.server.grid.general.Grid;

public class TimeComponent<G extends Grid<G>> extends DefaultProcessorComponent<G, TimeStimulus<G>> {
    private int lastTick;
    public TimeComponent(BrainComponent<G,? super TimeStimulus<G>> brainComp,
                         BodyComponent<G> bodyComp) {super(brainComp, bodyComp);}
    //TODO add priority system to ensure that this processor is called before others on the same entity (!)
    @Override public void process(Space<G> space) {
        int tick = space.getTick();
        output(new TimeStimulus<>(tick - lastTick));
        lastTick = tick;
    }
    @Override public void componentInstaller() {
        super.componentInstaller();
        lastTick = getSpace().getTick();
    }
}
