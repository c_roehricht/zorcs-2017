package work.connor.zorcs.server.grid.general;

import work.connor.zorcs.server.grid.absolute.Sys;

public interface SpatialValue<G extends Grid<G>,
                              V extends SpatialValue<G,V>>
        extends work.connor.zorcs.server.grid.absolute.SpatialValue<G,V>,
                work.connor.zorcs.server.grid.relative.SpatialValue<G,V>,
                Grid.Based<G> {
    @Override V toAbsolute(Sys<?,?> sy);
    @Override V toRelative(Sys<G,?> sy);
}
