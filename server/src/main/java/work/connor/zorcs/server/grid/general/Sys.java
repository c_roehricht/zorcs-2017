package work.connor.zorcs.server.grid.general;

public interface Sys<G  extends Grid<G>,
                     Sy extends Sys<G,Sy>>
        extends work.connor.zorcs.server.grid.absolute.Sys<G,Sy>,
                SpatialValue<G,Sy> {
    @Override default Sy toRelative(work.connor.zorcs.server.grid.absolute.Sys<G,?> sy) {
        throw new IllegalArgumentException("Systems are always absolute");
    }
    @SuppressWarnings("unchecked")
    default <V extends SpatialValue<G,?>> V toAbsolute(V v) {
        if (v == null) return null;
        return (V) v.toAbsolute(this);
    }
    @SuppressWarnings("unchecked")
    default <V extends SpatialValue<G,?>> V toRelative(V v) {
        if (v == null) return null;
        return (V) v.toRelative(this);
    }
    @SuppressWarnings("unchecked")
    @Override default Coord<G,?> absolute(work.connor.zorcs.server.grid.relative.Coord<?,?> cr) {
        return this.toAbsolute((Coord<G,?>) cr);
    }
    @SuppressWarnings("unchecked")
    @Override default LocationCoord<G,?> absolute(work.connor.zorcs.server.grid.relative.LocationCoord<?,?> lcr) {
        return this.toAbsolute((LocationCoord<G,?>) lcr);
    }
    @SuppressWarnings("unchecked")
    @Override default CellCoord<G,?> absolute(work.connor.zorcs.server.grid.relative.CellCoord<?,?> ccr) {
        return this.toAbsolute((CellCoord<G,?>) ccr);
    }
    @SuppressWarnings("unchecked")
    @Override default GapCoord<G,?> absolute(work.connor.zorcs.server.grid.relative.GapCoord<?,?> gcr) {
        return this.toAbsolute((GapCoord<G,?>) gcr);
    }
    @SuppressWarnings("unchecked")
    @Override default FCoord<G,?> absolute(work.connor.zorcs.server.grid.relative.FCoord<?,?> fcr) {
        return this.toAbsolute((FCoord<G,?>) fcr);
    }
    @SuppressWarnings("unchecked")
    @Override default Direction<G,?> absolute(work.connor.zorcs.server.grid.relative.Direction<?,?> dr) {
        return this.toAbsolute((Direction<G,?>) dr);
    }
    @SuppressWarnings("unchecked")
    @Override default Orientation<G,?> absolute(work.connor.zorcs.server.grid.relative.Orientation<?,?> or) {
        return this.toAbsolute((Orientation<G,?>) or);
    }
    @SuppressWarnings("unchecked")
    @Override default Coord<G,?> relative(work.connor.zorcs.server.grid.absolute.Coord<G,?> ca) {
        return this.toRelative((Coord<G,?>) ca);
    }
    @SuppressWarnings("unchecked")
    @Override default LocationCoord<G,?> relative(work.connor.zorcs.server.grid.absolute.LocationCoord<G,?> lca) {
        return this.toRelative((LocationCoord<G,?>) lca);
    }
    @SuppressWarnings("unchecked")
    @Override default CellCoord<G,?> relative(work.connor.zorcs.server.grid.absolute.CellCoord<G,?> cca) {
        return this.toRelative((CellCoord<G,?>) cca);
    }
    @SuppressWarnings("unchecked")
    @Override default GapCoord<G,?> relative(work.connor.zorcs.server.grid.absolute.GapCoord<G,?> gca) {
        return this.toRelative((GapCoord<G,?>) gca);
    }
    @SuppressWarnings("unchecked")
    @Override default FCoord<G,?> relative(work.connor.zorcs.server.grid.absolute.FCoord<G,?> fca) {
        return this.toRelative((FCoord<G,?>) fca);
    }
    @SuppressWarnings("unchecked")
    @Override default Direction<G,?> relative(work.connor.zorcs.server.grid.absolute.Direction<G,?> da) {
        return this.toRelative((Direction<G,?>) da);
    }
    @SuppressWarnings("unchecked")
    @Override default Orientation<G,?> relative(work.connor.zorcs.server.grid.absolute.Orientation<G,?> oa) {
        return this.toRelative((Orientation<G,?>) oa);
    }
}
