package work.connor.zorcs.server.grid.absolute;

public interface SpatialValue<G extends Grid<G>,
                              V extends SpatialValue<G,V>>
        extends work.connor.zorcs.server.grid.SpatialValue<G,V>,
                Grid.Based<G> {
    work.connor.zorcs.server.grid.relative.SpatialValue<?,?> toRelative(Sys<G,?> sy);
}
