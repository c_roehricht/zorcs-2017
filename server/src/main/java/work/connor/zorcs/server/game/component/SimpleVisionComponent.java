package work.connor.zorcs.server.game.component;

import work.connor.zorcs.server.game.event.VisualEvent;
import work.connor.zorcs.server.game.processor.ViewProcessor;
import work.connor.zorcs.server.game.stimulus.SimpleVisualStimulus;
import work.connor.zorcs.server.game.stimulus.VisualStimulus;
import work.connor.zorcs.server.grid.general.Grid;
import work.connor.zorcs.server.grid.relative.*;

import java.util.Set;

public class SimpleVisionComponent<G extends Grid<G>> extends DefaultProcessorComponent<G, VisualStimulus<G>>
        implements ViewProcessor<G> {
    private Set<LocationCoord<G,?>> fov;

    public SimpleVisionComponent(BrainComponent<G, ? super VisualStimulus<G>> brainComp,
                                 BodyComponent<G> bodyComp,
                                 Set<LocationCoord<G,?>> fov) {
        super(brainComp, bodyComp);
        this.fov = fov;
    }

    @Override public Set<LocationCoord<G,?>> fov() {return fov;}
    @Override public VisualStimulus<G> process(VisualEvent<G> event) {
        return new SimpleVisualStimulus<>(event, this);
    }
}
