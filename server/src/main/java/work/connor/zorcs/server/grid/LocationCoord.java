package work.connor.zorcs.server.grid;

public interface LocationCoord<G  extends Grid<G>,
                               LC extends LocationCoord<G,LC>>
        extends Coord<G,LC> {
    @Override default FCoord<G,?> multiply(double factor) {return floating().multiply(factor);}
    @Override default double floatingManhattan() {return floating().floatingManhattan();}
    @Override default double euclidean() {return floating().euclidean();}
    @Override default double floatingChebyshev() {return floating().floatingChebyshev();}
}
