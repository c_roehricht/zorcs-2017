package work.connor.zorcs.server.game.component;

import work.connor.zorcs.server.game.intent.Intent;
import work.connor.zorcs.server.game.space.Space;
import work.connor.zorcs.server.grid.absolute.*;
import work.connor.zorcs.server.grid.general.Grid;

import java.util.Set;
import java.util.function.Consumer;

public class CellularGeneratorComponent<G extends Grid<G>>
        extends DefaultComponent<G>
        implements DungeonGeneratorComponent<G> {
    private Set<? extends CellCoord<G,?>> field;
    private int iterations;
    private double cellProb;
    private int survivalThreshold, birthThreshold;
    private boolean randomizing = true;

    public CellularGeneratorComponent(Set<? extends CellCoord<G,?>> field, int iterations, double cellProb,
                                      int survivalThreshold, int birthThreshold) {
        super();
        this.field = field;
        this.iterations = iterations;
        this.cellProb = cellProb;
        this.survivalThreshold = survivalThreshold;
        this.birthThreshold = birthThreshold;
    }
    public void shallRandomize() {randomizing = true;}
    @Override public void act(Space<G> space, Consumer<Intent<G>> intents) {
        if (randomizing) {
            randomizing = false;
            randomize(space, intents);
        } else {
            iterate(space, intents);
        }
        iterations--;
        if (iterations > 0) intents.accept(Intent.reschedule(this, 1));
    }

    private void randomize(Space<G> space, Consumer<Intent<G>> intents) {
        for (CellCoord<G,?> coord : field) {
            if (space.getRandom().nextDouble() < cellProb) {
                intents.accept(DungeonGeneratorComponent.spawnCell(coord));
            } else {
                intents.accept(DungeonGeneratorComponent.removeCell(coord));
            }
        }
    }

    private void iterate(Space<G> space, Consumer<Intent<G>> intents) {
        for (CellCoord<G,?> coord : field) {
            int count = 0;
            for (CellCoord<G,?> neighbour : coord.chebyshevBox(1)) if (space.hasCell(neighbour)) count++;
            if (space.hasCell(coord)) {
                if (count < survivalThreshold) intents.accept(DungeonGeneratorComponent.removeCell(coord));
            } else {
                if (count >= birthThreshold) intents.accept(DungeonGeneratorComponent.spawnCell(coord));
            }
        }
    }
}
