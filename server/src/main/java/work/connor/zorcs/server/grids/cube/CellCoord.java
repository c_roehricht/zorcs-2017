package work.connor.zorcs.server.grids.cube;

import org.json.JSONObject;

import java.util.Objects;
import java.util.Optional;

public class CellCoord
        implements work.connor.zorcs.server.grids.common.CellCoord<Cube, CellCoord,GapCoord,FCoord,Direction,Orientation>,
                   LocationCoord<CellCoord> {
    public final int a, b, c;
    public CellCoord(int a, int b, int c) {this.a = a; this.b = b; this.c = c;}
    @Override public CellCoord multiply(int factor) {return new CellCoord(a * factor, b * factor, c * factor);}
    @Override public CellCoord negate() {return new CellCoord(-a, -b, -c);}
    @Override public Optional<? extends CellCoord> halfCell() {
        if ((a % 2 == 0) && (b % 2 == 0) && (c % 2 == 0)) return Optional.of(new CellCoord(a / 2, b / 2, c / 2));
        else return Optional.empty();
    }
    @Override public int manhattan() {return Math.abs(a) + Math.abs(b) + Math.abs(c);}
    @Override public int euclideanSquare() {return a * a + b * b + c * c;}
    @Override public int chebyshev() {return Math.max(Math.abs(a), Math.max(Math.abs(b), Math.abs(c)));}
    @Override public CellCoord add(CellCoord cc) {return new CellCoord(a + cc.a, b + cc.b, c + cc.c);}
    @Override public FCoord floating() {return new FCoord(a, b, c);}
    @Override public CellCoord rotate(Orientation o) {
        int[] vec = o.matrix.multiply(a, b, c);
        return new CellCoord(vec[0], vec[1], vec[2]);
    }
    @Override public CellCoord mirror() {return new CellCoord(a, -b, -c);}
    @Override public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CellCoord cellCoord = (CellCoord) o;
        return a == cellCoord.a &&
                b == cellCoord.b &&
                c == cellCoord.c;
    }
    @Override public int hashCode() {return Objects.hash(a, b, c);}
    @Override public JSONObject toRemote(JSONObject output) {
        output = LocationCoord.super.toRemote(output);
        output.put("a", a);
        output.put("b", b);
        return output.put("c", c);
    }
}
