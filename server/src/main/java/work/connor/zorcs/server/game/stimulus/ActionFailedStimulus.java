package work.connor.zorcs.server.game.stimulus;

import org.json.JSONObject;
import work.connor.zorcs.server.game.action.ActionFailedException;
import work.connor.zorcs.server.game.action.ActionFailedException.FailureCause;
import work.connor.zorcs.server.grid.Grid;

public class ActionFailedStimulus<G extends Grid<G>> extends ActionStimulus<G> {
    private ActionFailedException ex;
    public ActionFailedStimulus(JSONObject command, ActionFailedException ex) {super(command); this.ex = ex;}
    public ActionFailedStimulus(JSONObject command, FailureCause cause) {
        this(command, new ActionFailedException(cause));
    }
    @Override public String getName() {return "action-failed";}
    @Override public JSONObject toRemote(JSONObject output) {
        output = super.toRemote(output);
        return put(output, "cause", ex);
    }
}
