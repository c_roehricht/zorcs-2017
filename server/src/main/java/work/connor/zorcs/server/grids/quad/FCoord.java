package work.connor.zorcs.server.grids.quad;

public class FCoord
        extends work.connor.zorcs.server.grids.quadhex.FCoord<Quad,CellCoord,GapCoord,FCoord,Angle>
        implements Coord<FCoord> {
    public FCoord(double a, double b) {super(a, b);}
    @Override public double floatingManhattan() {return Math.abs(a) + Math.abs(b);}
    @Override public double euclidean() {return Math.sqrt(a * a + b * b);}
    @Override public double floatingChebyshev() {return Math.max(Math.abs(a), Math.abs(b));}
    @Override public FCoord rotateOne() {return new FCoord(-b, a);}
    @Override public FCoord mirror() {return new FCoord(a,-b);}
}
