package work.connor.zorcs.server.grid;

public interface Orientation<G extends Grid<G>,
                             O extends Orientation<G,O>>
        extends SpatialValue<G,O> {
    O negate();
    default Direction<G,?> direction() {return g().forward().rotate(this);}
}
