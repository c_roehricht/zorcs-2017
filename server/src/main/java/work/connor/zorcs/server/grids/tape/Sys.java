package work.connor.zorcs.server.grids.tape;

public class Sys
        extends work.connor.zorcs.server.grids.common.Sys<Tape,Sys,CellCoord,GapCoord,FCoord,Angle,Angle>
        implements Tape.Based {
    public Sys(CellCoord cc, Angle a) {super(cc, a);}
}
