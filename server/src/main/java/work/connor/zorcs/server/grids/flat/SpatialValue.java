package work.connor.zorcs.server.grids.flat;

import work.connor.zorcs.server.grids.common.*;

public interface SpatialValue<G  extends FlatGrid<G,CC,GC,FC,A>,
                              V  extends SpatialValue<G,V,CC,GC,FC,A>,
                              CC extends CellCoord<G,CC,GC,FC,A>,
                              GC extends GapCoord<G,CC,GC,FC,A>,
                              FC extends FCoord<G,CC,GC,FC,A,A>,
                              A  extends Angle<G,CC,GC,FC,A>>
        extends work.connor.zorcs.server.grids.common.SpatialValue<G,V,CC,GC,FC,A,A>,
                FlatGrid.Based<G,CC,GC,FC,A> {
    @Override default V rotate(A a) {
        if (a.r == 0) return concrete();
        return rotateOne().rotate(a.decrement());
    }
    V rotateOne();
}
