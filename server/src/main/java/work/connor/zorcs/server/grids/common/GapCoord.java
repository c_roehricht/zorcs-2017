package work.connor.zorcs.server.grids.common;

import org.json.JSONObject;
import work.connor.zorcs.server.grid.absolute.Sys;

import java.util.Objects;

public abstract class GapCoord<G  extends CommonGrid<G,CC,GC,FC,D,O>,
                               CC extends CellCoord<G,CC,GC,FC,D,O>,
                               GC extends GapCoord<G,CC,GC,FC,D,O>,
                               FC extends FCoord<G,CC,GC,FC,D,O>,
                               D  extends Direction<G,CC,GC,FC,D,O>,
                               O  extends Orientation<G,CC,GC,FC,D,O>>
        implements work.connor.zorcs.server.grid.general.GapCoord<G,GC>,
                   LocationCoord<G,GC,CC,GC,FC,D,O> {
    protected final CC cc;
    protected GapCoord(CC cc) {
        if (!g().gInvariant(cc)) throw new IllegalArgumentException();
        this.cc = cc;
    }
    @Override public GC toAbsolute(Sys<?,?> sy) {return LocationCoord.super.toAbsolute(sy);}
    @Override public GC toRelative(Sys<G,?> sy) {return LocationCoord.super.toRelative(sy);}
    @Override public FC floating() {return cc.floating().multiply(0.5);}
    @Override public GC add(CC c) {
        return g().gapCoord(cc.add(c.multiply(2)));
    }
    @Override public GC negate() {return g().gapCoord(cc.negate());}
    @Override public GC rotate(O o) {
        return g().gapCoord(cc.rotate(o));
    }
    @Override public GC mirror() {return g().gapCoord(cc.mirror());}
    @Override public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GapCoord<?, ?, ?, ?, ?, ?> gapCoord = (GapCoord<?, ?, ?, ?, ?, ?>) o;
        return Objects.equals(cc, gapCoord.cc);
    }
    @Override public int hashCode() {return Objects.hash(cc);}
    @Override public JSONObject toRemote(JSONObject output) {
        output = LocationCoord.super.toRemote(output);
        return put(output, "cc", cc);
    }
}
