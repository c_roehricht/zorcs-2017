package work.connor.zorcs.server.grids.hex;

import work.connor.zorcs.server.utility.MathU;

public class FCoord
        extends work.connor.zorcs.server.grids.quadhex.FCoord<Hex,CellCoord,GapCoord, FCoord,Angle>
        implements Coord<FCoord> {
    public FCoord(double a, double b) {super(a, b);}
    public double getC() {return -a - b;}
    @Override public double floatingManhattan() {return (Math.abs(a) + Math.abs(b) + Math.abs(-a - b)) / 2;}
    @Override public double euclidean() {return Math.sqrt(a * a + b * b + a * b);}
    @Override public double floatingChebyshev() {
        return MathU.min(Math.max(Math.abs(a), Math.abs(b)), Math.max(Math.abs(a), Math.abs(getC())),
                Math.max(Math.abs(b), Math.abs(getC())));
    }
    @Override public FCoord rotateOne() {return new FCoord(-b, a + b);}
    @Override public FCoord mirror() {return new FCoord(a + b, -b);}
}
