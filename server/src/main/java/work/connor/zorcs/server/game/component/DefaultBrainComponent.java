package work.connor.zorcs.server.game.component;

import work.connor.zorcs.server.game.stimulus.Stimulus;
import work.connor.zorcs.server.grid.general.Grid;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class DefaultBrainComponent<G extends Grid<G>, S extends Stimulus<G>> extends DefaultComponent<G>
        implements BrainComponent<G,S> {
    private Map<String, ActionComponent<G>> actionComponents = new HashMap<>();
    //TODO ensure determinism everywhere: Correct map/set types on the outside! annotations?

    @Override public void addActionComponent(List<String> actionNames, ActionComponent<G> component) {
        for (String actionName : actionNames) actionComponents.put(actionName, component);
    }
    @Override public void removeActionComponent(List<String> actionNames, ActionComponent<G> component) {
        for (String actionName : actionNames) actionComponents.remove(actionName, component);
    }
    protected ActionComponent<G> getActionComponent(String actionName) {
        return actionComponents.getOrDefault(actionName, null);
    }
}
