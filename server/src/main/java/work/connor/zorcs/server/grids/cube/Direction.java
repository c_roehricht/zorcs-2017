package work.connor.zorcs.server.grids.cube;

import org.json.JSONObject;

import java.util.Objects;

public class Direction
        implements work.connor.zorcs.server.grids.common.Direction<Cube,CellCoord,GapCoord,FCoord,Direction,Orientation>,
                   SpatialValue<Direction> {
    public final CellCoord cc;
    public Direction(CellCoord cc) {this.cc = cc;} //TODO invariant?
    @Override public Direction opposite() {return new Direction(cc.negate());}
    @Override public CellCoord unit() {return cc;}
    @Override public Direction rotate(Orientation o) {return new Direction(cc.rotate(o));}
    @Override public Direction mirror() {return new Direction(cc.mirror());}
    @Override public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Direction direction = (Direction) o;
        return Objects.equals(cc, direction.cc);
    }
    @Override public int hashCode() {return Objects.hash(cc);}
    @Override public JSONObject toRemote(JSONObject output) {
        output = work.connor.zorcs.server.grids.common.Direction.super.toRemote(output);
        return output.put("cc", cc);
    }
}
