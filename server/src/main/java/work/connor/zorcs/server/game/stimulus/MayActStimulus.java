package work.connor.zorcs.server.game.stimulus;

import work.connor.zorcs.server.grid.Grid;

public class MayActStimulus<G extends Grid<G>> implements Stimulus<G> {
    @Override public String getName() {return "may-act";}
}
