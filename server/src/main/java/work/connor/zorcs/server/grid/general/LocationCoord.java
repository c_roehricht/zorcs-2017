package work.connor.zorcs.server.grid.general;

public interface LocationCoord<G  extends Grid<G>,
                               LC extends LocationCoord<G,LC>>
        extends work.connor.zorcs.server.grid.absolute.LocationCoord<G,LC>,
                work.connor.zorcs.server.grid.relative.LocationCoord<G,LC>,
                Coord<G,LC> {
}
