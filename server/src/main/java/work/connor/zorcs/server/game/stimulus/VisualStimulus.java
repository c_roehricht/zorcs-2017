package work.connor.zorcs.server.game.stimulus;

import org.json.JSONObject;
import work.connor.zorcs.server.grid.relative.*;

import java.util.Map;

public interface VisualStimulus<G extends Grid<G>> extends LocalizedStimulus<G> {
    @Override default String getName() {return "visual";}
    Orientation<G,?> getOrientation();
    String getType();
    String getAppearance();
    Map<String,String> getDetails();
    @Override default JSONObject toRemote(JSONObject output) {
        output = LocalizedStimulus.super.toRemote(output);
        put(output, "orientation", getOrientation());
        output.put("type", getType());
        output.put("appearance", getAppearance());
        return output.put("details", getDetails());
    }
}
