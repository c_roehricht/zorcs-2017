package work.connor.zorcs.server.utility;

import java.util.List;
import java.util.Optional;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.IntFunction;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class StreamU {
    public static IntStream naturals() {return IntStream.range(0, Integer.MAX_VALUE);}
    public static <U> Stream<U> flatMapToObj(IntStream ints, IntFunction<Stream<? extends U>> mapper) {
        return ints.mapToObj(mapper).flatMap(Function.identity());
    }
    public static <R,A,B> Stream<R> cartesian(Stream<A> a, Stream<B> b, BiFunction<A,B,R> combinator) {
        List<B> bVals = b.collect(Collectors.toList());
        return a.flatMap(aVal -> bVals.stream().map(bVal -> combinator.apply(aVal,bVal)));
    }
    public static <T> Stream<T> union(Stream<? extends T> s1, Stream<? extends T> s2) {
        return SetU.union(s1.collect(Collectors.toSet()), s2.collect(Collectors.toSet())).stream();
    }
    public static <T> boolean containsAll(Stream<? extends T> outer, Stream<? extends T> inner) {
        return outer.collect(Collectors.toSet()).containsAll(inner.collect(Collectors.toList()));
    }
    public static <I, O> Stream<O> filterMap(Stream<I> input, Function<? super I, ? extends O> mapper) {
        return input.flatMap(i -> Optional.ofNullable(mapper.apply(i)).stream());
    }
    public static <U, D extends U> Stream<U> up(Stream<D> subtypeStream) {return subtypeStream.map(d -> (U) d);}
    @SuppressWarnings("unchecked")
    public static <U, D extends U> Stream<D> down(Stream<U> supertypeStream) {return supertypeStream.map(u -> (D) u);}
    public static <U, D extends U> Stream<D> filterDown(Stream<U> us, Predicate<? super U> filter) {
        return down(us.filter(filter));
    }
}
