package work.connor.zorcs.server.grids.hex;

public interface LocationCoord<LC extends LocationCoord<LC>>
        extends work.connor.zorcs.server.grids.quadhex.LocationCoord<Hex,LC,CellCoord,GapCoord,FCoord,Angle>,
        Coord<LC> {
}
