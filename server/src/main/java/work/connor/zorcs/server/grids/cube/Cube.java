package work.connor.zorcs.server.grids.cube;

import org.json.JSONObject;
import work.connor.zorcs.server.grids.common.CommonGrid;
import work.connor.zorcs.server.grids.plane_containing.PlaneContainingGrid;
import work.connor.zorcs.server.utility.IntMatrix3;
import work.connor.zorcs.server.utility.MathU;
import work.connor.zorcs.server.utility.StreamU;

import java.util.LinkedHashSet;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public interface Cube extends
        CommonGrid<Cube,CellCoord,GapCoord,FCoord,Direction,Orientation>,
        PlaneContainingGrid<Cube,CellCoord,GapCoord,FCoord,Direction,Orientation> {
    Cube CUBE = new Cube() {};
    interface Based
            extends CommonGrid.Based<Cube,CellCoord,GapCoord,FCoord,Direction,Orientation>,
                    PlaneContainingGrid.Based<Cube,CellCoord,GapCoord,FCoord,Direction,Orientation> {
        @Override default Cube g() {return CUBE;}
    }
    default CellCoord cellCoord(int a, int b, int c) {return new CellCoord(a, b, c);}
    @Override default GapCoord gapCoord(CellCoord cc) {return new GapCoord(cc);}
    default GapCoord gapCoord(int ga, int gb, int gc) {return gapCoord(cellCoord(ga, gb, gc));}
    @Override default boolean gInvariant(CellCoord cc) {
        return Math.floorMod(cc.a, 2) + Math.floorMod(cc.b, 2) + Math.floorMod(cc.c, 2) == 1;
    }
    default Direction direction(CellCoord cc) {return new Direction(cc);}
    default Orientation orientation(IntMatrix3 matrix) {return new Orientation(matrix);}
    @Override default CellCoord origin() {return new CellCoord(0,0,0);}
    @Override default CellCoord unitForward() {return new CellCoord(1,0,0);}
    @Override default Direction forward() {return new Direction(unitForward());}
    @Override default Orientation noRotation() {return Orientation.NO_ROTATION;}
    @Override default Orientation oLeft() {return Orientation.LEFT;}
    @Override default Set<? extends Orientation> orientations() {
        return Set.of(Orientation.NO_ROTATION, Orientation.RIGHT, Orientation.LEFT, Orientation.BEHIND,
                Orientation.CLOCKWISE, Orientation.COUNTERCLOCKWISE, Orientation.UPSIDEDOWN, Orientation.UP,
                Orientation.DOWN, Orientation.RIGHT_CLOCKWISE, Orientation.RIGHT_COUNTERCLOCKWISE,
                Orientation.RIGHT_UPSIDEDOWN, Orientation.LEFT_CLOCKWISE, Orientation.LEFT_COUNTERCLOCKWISE,
                Orientation.LEFT_UPSIDEDOWN, Orientation.BEHIND_CLOCKWISE, Orientation.BEHIND_COUNTERCLOCKWISE,
                Orientation.BEHIND_UPSIDEDOWN, Orientation.UP_CLOCKWISE, Orientation.UP_COUNTERCLOCKWISE,
                Orientation.UP_UPSIDEDOWN, Orientation.DOWN_CLOCKWISE, Orientation.DOWN_COUNTERCLOCKWISE,
                Orientation.DOWN_UPSIDEDOWN);
    }
    @Override default Set<? extends CellCoord> euclideanSquareSector(int dSq) {
        Set<CellCoord> result = new LinkedHashSet<>();
        for (int c = 0; c <= MathU.isqrt(dSq / 3.0); c++) {
            int ringDSq = dSq - c * c;
            int a = MathU.isqrt(ringDSq - c * c);
            int b = c;
            while (a >= b) {
                int eSq = a * a + b * b;
                if (eSq == ringDSq) result.add(new CellCoord(a, b, c));
                if (eSq >= ringDSq) a--;
                if (eSq <= ringDSq) b++;
            }
        }
        return result;
    }
    @Override default Set<? extends CellCoord> manhattanSector(int d) {
        return StreamU.flatMapToObj(IntStream.rangeClosed((d + 1) / 3, d),
                a -> IntStream.rangeClosed((d - a + 1) / 2, Math.min(d - a, a)).mapToObj(
                        b -> new CellCoord(a, b, d - a - b))).collect(Collectors.toSet());
    }
    @Override default Set<? extends CellCoord> chebyshevSector(int d) {
        return StreamU.flatMapToObj(IntStream.rangeClosed(0, d),
                b -> IntStream.rangeClosed(0, b).mapToObj(
                        c -> new CellCoord(d, b, c))).collect(Collectors.toSet());
    }
    @Override default Sys system(CellCoord cc, Orientation o) {return new Sys(cc,o);}
    @Override default CellCoord cellCoord(JSONObject input) {
        return cellCoord(input.getInt("a"), input.getInt("b"), input.getInt("c"));
    }
    @Override default Direction direction(JSONObject input) {return direction(cellCoord(input.getJSONObject("cc")));}
    @Override default Orientation orientation(JSONObject input) {
        return orientation(new IntMatrix3(input.getJSONArray("matrix")));
    }
}
