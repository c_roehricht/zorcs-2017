package work.connor.zorcs.server.grids.cube;

public class Sys
        extends work.connor.zorcs.server.grids.common.Sys<Cube,Sys,CellCoord,GapCoord,FCoord,Direction,Orientation>
        implements Cube.Based {
    public Sys(CellCoord cc, Orientation o) {super(cc, o);}
}
