package work.connor.zorcs.server.grids.hex;

import work.connor.zorcs.server.grids.quadhex.QuadHexGrid;

public interface Hex extends QuadHexGrid<Hex,CellCoord,GapCoord,FCoord,Angle> {
    Hex HEX = new Hex() {};
    interface Based extends QuadHexGrid.Based<Hex,CellCoord,GapCoord,FCoord,Angle> {
        @Override default Hex g() {return HEX;}
    }
    @Override default CellCoord cellCoord(int a, int b) {return new CellCoord(a,b);}
    @Override default FCoord fCoord(double a, double b) {return new FCoord(a,b);}
    @Override default Angle angle(int r) {return new Angle(r);}
    @Override default int circle() {return 6;}
    @Override default GapCoord gapCoord(CellCoord cc) {return new GapCoord(cc);}
    @Override default boolean gInvariant(CellCoord cc) {return (cc.a % 2 != 0) || (cc.b % 2 != 0);}
    @Override default Sys system(CellCoord cc, Angle a) {return new Sys(cc,a);}
}
