package work.connor.zorcs.server.grids.quadhex;

import org.json.JSONObject;
import work.connor.zorcs.server.grids.flat.*;
import work.connor.zorcs.server.grids.plane_containing.PlaneContainingGrid;
import work.connor.zorcs.server.utility.MathU;

import java.util.LinkedHashSet;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public interface QuadHexGrid<G  extends QuadHexGrid<G,CC,GC,FC,A>,
                             CC extends CellCoord<G,CC,GC,FC,A>,
                             GC extends GapCoord<G,CC,GC,FC,A>,
                             FC extends FCoord<G,CC,GC,FC,A>,
                             A  extends Angle<G,CC,GC,FC,A>>
        extends FlatGrid<G,CC,GC,FC,A>,
                PlaneContainingGrid<G,CC,GC,FC,A,A> {
    interface Based<G  extends QuadHexGrid<G,CC,GC,FC,A>,
                    CC extends CellCoord<G,CC,GC,FC,A>,
                    GC extends GapCoord<G,CC,GC,FC,A>,
                    FC extends FCoord<G,CC,GC,FC,A>,
                    A  extends Angle<G,CC,GC,FC,A>>
            extends FlatGrid.Based<G,CC,GC,FC,A>,
                    PlaneContainingGrid.Based<G,CC,GC,FC,A,A> {}
    CC cellCoord(int a, int b);
    default GC gapCoord(int ga, int gb) {return gapCoord(cellCoord(ga, gb));}
    FC fCoord(double a, double b);
    @Override default CC origin() {return cellCoord(0,0);}
    @Override default CC unitForward() {return cellCoord(1,0);}
    @Override default A oLeft() {return angle(-1);}
    @Override default Set<? extends CC> euclideanSquareSector(int dSq) {
        Set<CC> result = new LinkedHashSet<>();
        int a = MathU.isqrt(dSq);
        int b = 0;
        while (a >= b) {
            CC tested = cellCoord(a, b);
            int eSq = tested.euclideanSquare();
            if (eSq == dSq) result.add(tested);
            if (eSq >= dSq) a--;
            if (eSq <= dSq) b++;
        }
        return result;
    }
    @Override default Set<? extends CC> manhattanSector(int d) {
        return IntStream.rangeClosed((d + 1) / 2, d).mapToObj(a -> cellCoord(a, d - a)).collect(Collectors.toSet());
    }
    @Override default Set<? extends CC> chebyshevSector(int d) {
        return IntStream.rangeClosed(0, d).mapToObj(b -> cellCoord(d,b)).collect(Collectors.toSet());
    }
    @Override default CC cellCoord(JSONObject input) {return cellCoord(input.getInt("a"), input.getInt("b"));}
}
