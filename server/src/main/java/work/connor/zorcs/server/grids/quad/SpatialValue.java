package work.connor.zorcs.server.grids.quad;

public interface SpatialValue<V extends SpatialValue<V>>
        extends work.connor.zorcs.server.grids.quadhex.SpatialValue<Quad,V,CellCoord,GapCoord,FCoord,Angle>,
                Quad.Based {
}
