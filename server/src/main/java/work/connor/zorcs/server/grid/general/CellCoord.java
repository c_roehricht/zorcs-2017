package work.connor.zorcs.server.grid.general;

public interface CellCoord<G  extends Grid<G>,
                           CC extends CellCoord<G,CC>>
        extends work.connor.zorcs.server.grid.absolute.CellCoord<G,CC>,
                work.connor.zorcs.server.grid.relative.CellCoord<G,CC>,
                LocationCoord<G,CC> {
}
