package work.connor.zorcs.server.grids.common;

import work.connor.zorcs.server.grid.absolute.Sys;

public interface Coord<G  extends CommonGrid<G,CC,GC,FC,D,O>,
                       Cs extends Coord<G,Cs,CC,GC,FC,D,O>,
                       CC extends CellCoord<G,CC,GC,FC,D,O>,
                       GC extends GapCoord<G,CC,GC,FC,D,O>,
                       FC extends FCoord<G,CC,GC,FC,D,O>,
                       D  extends Direction<G,CC,GC,FC,D,O>,
                       O  extends Orientation<G,CC,GC,FC,D,O>>
        extends work.connor.zorcs.server.grid.general.Coord<G,Cs>,
                SpatialValue<G,Cs,CC,GC,FC,D,O> {
    @Override default Cs toAbsolute(Sys<?,?> sy) {return SpatialValue.super.toAbsolute(sy);}
    @Override default Cs toRelative(Sys<G,?> sy) {return SpatialValue.super.toRelative(sy);}
    @Override FC floating();
    @Override default Cs translate(CC cc) {return add(cc);}
    @Override FC multiply(double factor);
}
