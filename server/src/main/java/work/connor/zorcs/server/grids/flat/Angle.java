package work.connor.zorcs.server.grids.flat;

import org.json.JSONObject;
import work.connor.zorcs.server.grids.common.*;

import java.util.Objects;

public abstract class Angle<G  extends FlatGrid<G,CC,GC,FC,A>,
                            CC extends CellCoord<G,CC,GC,FC,A>,
                            GC extends GapCoord<G,CC,GC,FC,A>,
                            FC extends FCoord<G,CC,GC,FC,A,A>,
                            A  extends Angle<G,CC,GC,FC,A>>
        implements Direction<G,CC,GC,FC,A,A>,
                   Orientation<G,CC,GC,FC,A,A>,
                   SpatialValue<G,A,CC,GC,FC,A> {
    final int r;
    protected Angle(int r) {this.r = Math.floorMod(r, g().circle());}
    @Override public A opposite() {return g().angle(r + g().circle() / 2);}
    @Override public A negate() {return g().angle(-r);}
    @Override public A direction() {return this.concrete();}
    @Override public A mirror() {return negate();}
    @Override public A rotateOne() {return g().angle(r + 1);}
    @Override public A translate(CC cc) {
        return Direction.super.translate(cc);
    }
    @Override public CC unit() {return g().unitForward().rotate(this);}
    public A decrement() {return g().angle(r - 1);}

    @Override public A toAbsolute(work.connor.zorcs.server.grid.absolute.Sys<?,?> sy) {
        return Direction.super.toAbsolute(sy);
    }
    @Override public A toRelative(work.connor.zorcs.server.grid.absolute.Sys<G,?> sy) {
        return Direction.super.toRelative(sy);
    }
    @Override public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Angle<?, ?, ?, ?, ?> angle = (Angle<?, ?, ?, ?, ?>) o;
        return r == angle.r;
    }
    @Override public int hashCode() {return Objects.hash(r);}
    @Override public JSONObject toRemote(JSONObject output) {
        output = Direction.super.toRemote(output);
        return output.put("r", r);
    }
}
