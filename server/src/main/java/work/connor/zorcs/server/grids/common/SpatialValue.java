package work.connor.zorcs.server.grids.common;

public interface SpatialValue<G  extends CommonGrid<G,CC,GC,FC,D,O>,
                              V  extends SpatialValue<G,V,CC,GC,FC,D,O>,
                              CC extends CellCoord<G,CC,GC,FC,D,O>,
                              GC extends GapCoord<G,CC,GC,FC,D,O>,
                              FC extends FCoord<G,CC,GC,FC,D,O>,
                              D  extends Direction<G,CC,GC,FC,D,O>,
                              O  extends Orientation<G,CC,GC,FC,D,O>>
        extends work.connor.zorcs.server.grid.general.SpatialValue<G,V>,
                CommonGrid.Based<G,CC,GC,FC,D,O> {
    @SuppressWarnings("unchecked")
    @Override default V toAbsolute(work.connor.zorcs.server.grid.absolute.Sys<?,?> sy) {
        return toAbsolute((Sys<G,?,CC,GC,FC,D,O>) sy);
    }
    default V toAbsolute(Sys<G,?,CC,GC,FC,D,O> sy) {return rotate(sy.orientation()).translate(sy.cellCoord());}
    @SuppressWarnings("unchecked")
    @Override default V toRelative(work.connor.zorcs.server.grid.absolute.Sys<G,?> sy) {
        return toRelative((Sys<G,?,CC,GC,FC,D,O>) sy);
    }
    default V toRelative(Sys<G,?,CC,GC,FC,D,O> sy) {return toAbsolute(sy.negate());}
    @SuppressWarnings("unchecked")
    @Override default V translate(work.connor.zorcs.server.grid.CellCoord<G,?> cc) {return translate((CC) cc);}
    V translate(CC cc);
    @SuppressWarnings("unchecked")
    @Override default V rotate(work.connor.zorcs.server.grid.Orientation<G,?> o) {return rotate((O) o);}
    V rotate(O o);
}
