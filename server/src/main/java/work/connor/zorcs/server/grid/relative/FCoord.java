package work.connor.zorcs.server.grid.relative;

import work.connor.zorcs.server.grid.absolute.Sys;

public interface FCoord<G  extends Grid<G>,
                        FC extends FCoord<G,FC>>
        extends work.connor.zorcs.server.grid.FCoord<G,FC>,
                Coord<G,FC> {
    @Override work.connor.zorcs.server.grid.absolute.FCoord<?,?> toAbsolute(Sys<?,?> sy);
}
