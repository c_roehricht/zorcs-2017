package work.connor.zorcs.server.grid.absolute;

public interface LocationCoord<G  extends Grid<G>,
                               LC extends LocationCoord<G,LC>>
        extends work.connor.zorcs.server.grid.LocationCoord<G,LC>,
                Coord<G,LC> {
    @Override work.connor.zorcs.server.grid.relative.LocationCoord<?,?> toRelative(Sys<G,?> sy);
}
