package work.connor.zorcs.server.grid.absolute;

public interface Grid<G extends Grid<G>>
        extends work.connor.zorcs.server.grid.Grid<G> {
    interface Based<G extends Grid<G>> extends work.connor.zorcs.server.grid.Grid.Based<G> {}
    Sys<G,?> system(CellCoord<G,?> cc, Orientation<G,?> o);
}
