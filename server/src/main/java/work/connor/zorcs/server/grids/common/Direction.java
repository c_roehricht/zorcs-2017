package work.connor.zorcs.server.grids.common;

import work.connor.zorcs.server.grid.absolute.Sys;

public interface Direction<G  extends CommonGrid<G,CC,GC,FC,D,O>,
                           CC extends CellCoord<G,CC,GC,FC,D,O>,
                           GC extends GapCoord<G,CC,GC,FC,D,O>,
                           FC extends FCoord<G,CC,GC,FC,D,O>,
                           D  extends Direction<G,CC,GC,FC,D,O>,
                           O  extends Orientation<G,CC,GC,FC,D,O>>
        extends work.connor.zorcs.server.grid.general.Direction<G,D>,
                SpatialValue<G,D,CC,GC,FC,D,O> {
    @Override default D toAbsolute(Sys<?,?> sy) {return SpatialValue.super.toAbsolute(sy);}
    @Override default D toRelative(Sys<G,?> sy) {return SpatialValue.super.toRelative(sy);}
    @Override default D translate(CC cc) {return concrete();}
    @Override CC unit();
    @Override default GC unitGap() {return unit().halfGap().get();}
}
