package work.connor.zorcs.server.game.action;

import work.connor.zorcs.server.game.component.BodyComponent;
import work.connor.zorcs.server.game.space.Space;
import work.connor.zorcs.server.grid.general.Grid;
import work.connor.zorcs.server.grid.relative.CellCoord;
import work.connor.zorcs.server.grid.relative.Direction;
import work.connor.zorcs.server.grid.relative.GapCoord;

public class StepAction<G extends Grid<G>> extends BodyAction<G> {
    private Direction<G,?> d;
    public StepAction(BodyComponent<G> body, Direction<G,?> d) {super(body); this.d = d;}
    @Override public void perform(Space<G> space) throws ActionFailedException {
        CellCoord<G,?> cellCoord = d.unit();
        GapCoord<G,?> gapCoord = d.unitGap();
        //TODO check for wall or object in the way, check collision (passthrough/block)
        body.move(cellCoord);
    }
}
