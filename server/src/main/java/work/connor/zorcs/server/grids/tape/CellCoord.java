package work.connor.zorcs.server.grids.tape;

import org.json.JSONObject;

import java.util.Objects;
import java.util.Optional;
import java.util.Set;

public class CellCoord
        implements work.connor.zorcs.server.grids.flat.CellCoord<Tape,CellCoord,GapCoord,FCoord,Angle>,
                   LocationCoord<CellCoord> {
    public final int a;
    public CellCoord(int a) {this.a = a;}
    @Override public CellCoord multiply(int factor) {return new CellCoord(a * factor);}
    @Override public CellCoord negate() {return new CellCoord(-a);}
    @Override public Optional<? extends CellCoord> halfCell() {
        return (a % 2 == 0) ? Optional.of(new CellCoord(a / 2)) : Optional.empty();
    }
    @Override public int manhattan() {return a;}
    @Override public int euclideanSquare() {return a * a;}
    @Override public int chebyshev() {return a;}
    @Override public CellCoord add(CellCoord cc) {return new CellCoord(a + cc.a);}
    @Override public FCoord floating() {return new FCoord(a);}
    @Override public CellCoord rotateOne() {return negate();}
    @Override public CellCoord mirror() {return this;}
    @Override public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CellCoord cellCoord = (CellCoord) o;
        return a == cellCoord.a;
    }
    @Override public int hashCode() {return Objects.hash(a);}
    @Override public JSONObject toRemote(JSONObject output) {
        output = LocationCoord.super.toRemote(output);
        return output.put("a", a);
    }
}
