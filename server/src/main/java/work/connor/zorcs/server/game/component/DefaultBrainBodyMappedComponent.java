package work.connor.zorcs.server.game.component;

import work.connor.zorcs.server.game.stimulus.Stimulus;
import work.connor.zorcs.server.grid.general.Grid;

public abstract class DefaultBrainBodyMappedComponent<G extends Grid<G>, S extends Stimulus<G>>
        extends DefaultBodyMappedComponent<G>
        implements BrainComponent.Mapped<G,S> {
    private BrainComponent<G,? super S> brainComp;
    public DefaultBrainBodyMappedComponent(BrainComponent<G,? super S> brainComp,
                                           BodyComponent<G> bodyComp) {
        super(bodyComp);
        this.brainComp = brainComp;
    }
    @Override public BrainComponent<G,? super S> getBrainComp() {return brainComp;}
    @Override public void setBrainComp(BrainComponent<G, ? super S> brainComp) {this.brainComp = brainComp;}

    public static abstract class Generic<G extends Grid<G>> extends DefaultBodyMappedComponent<G>
            implements BrainComponent.GenericMapped<G> {
        private BrainComponent<G,?> brainComp;
        public Generic(BrainComponent<G,?> brainComp,
                       BodyComponent<G> bodyComp) {
            super(bodyComp);
            this.brainComp = brainComp;
        }
        @Override public BrainComponent<G,?> getBrainComp() {return brainComp;}
        @Override public void setBrainComp(BrainComponent<G,?> brainComp) {this.brainComp = brainComp;}
    }
}
