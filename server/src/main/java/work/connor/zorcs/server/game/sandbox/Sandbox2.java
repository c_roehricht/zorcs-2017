package work.connor.zorcs.server.game.sandbox;

import org.json.JSONObject;
import work.connor.zorcs.server.game.actor.Actor;
import work.connor.zorcs.server.game.component.*;
import work.connor.zorcs.server.game.entity.DefaultEntity;
import work.connor.zorcs.server.game.entity.Entity;
import work.connor.zorcs.server.game.space.Cell;
import work.connor.zorcs.server.game.space.DefaultSpace;
import work.connor.zorcs.server.game.space.GroundCell;
import work.connor.zorcs.server.game.space.Space;
import work.connor.zorcs.server.grid.general.Grid;
import work.connor.zorcs.server.grids.quad.Quad;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.stream.Collectors;

public class Sandbox2<G extends Grid<G>> {
    private G g;
    public Sandbox2(G g) {this.g = g;}

    public void go() throws IOException {
        Space<G> space = new DefaultSpace<>(g);
        Actor<G> generator = new CellularGeneratorComponent<>(
                g.euclideanBox(20).collect(Collectors.toSet()), 10, 0.53, 5, 4);
        space.schedule(generator, 1);

        Cell<G> watchCell = new GroundCell<>(space, g.unitForward().multiply(21), "stone");
        BodyComponent<G> body = new SimpleBodyComponent<>(watchCell, g.noRotation(), "blue");

        /*
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        RemoteBrainComponent<G> brain = new RemoteBrainComponent<>(
                () -> {
                    try {return new JSONObject(in.readLine());}
                    catch (IOException e) {throw new RuntimeException(e);}},
                // Yes, we can only read single-line JSON because this library sucks
                System.out::println);
                */
        ServerSocket servSocket = new ServerSocket(1234);
        Socket socket = servSocket.accept();
        BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
        RemoteBrainComponent<G> brain = new RemoteBrainComponent<>(
                () -> {
                    try {return new JSONObject(in.readLine());}
                    catch (IOException e) {throw new RuntimeException(e);}},
                // Yes, we can only read single-line JSON because this library sucks
                out::println);

        TimeComponent<G> time = new TimeComponent<>(brain, body);
        SimpleVisionComponent<G> vision = new SimpleVisionComponent<>(brain, body,
                g.euclideanCoordBox(40).collect(Collectors.toSet()));
        WalkComponent<G> walk = new WalkComponent<>(brain, body);
        Entity<G> player = new DefaultEntity<>(body, time, brain, vision, walk);
        space.schedule(brain, 1);

        while (true) {
            space.nextTick();
        }
    }
    public static void main(String[] args) throws IOException {
        new Sandbox2<>(Quad.QUAD).go();
    }
}
