package work.connor.zorcs.server.grid;

public interface FCoord<G  extends Grid<G>,
                        FC extends FCoord<G,FC>>
        extends Coord<G,FC> {
    @Override default FC floating() {return concrete();}
    @Override default FC multiply(int factor) {return multiply((double) factor);}
    @Override FC multiply(double factor);
    @Override default FC add(CellCoord<G,?> cc) {return add(cc.floating());}
    FC add(FCoord<G,?> fc);
}
