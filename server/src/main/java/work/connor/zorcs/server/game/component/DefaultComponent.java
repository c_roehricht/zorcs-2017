package work.connor.zorcs.server.game.component;

import work.connor.zorcs.server.game.entity.Entity;
import work.connor.zorcs.server.grid.general.Grid;

public abstract class DefaultComponent<G extends Grid<G>> implements Component<G> {
    protected Entity<G> entity;
    @Override public Entity<G> getEntity() {return entity;}
    @Override public void setEntity(Entity<G> entity) {this.entity = entity;}
}
