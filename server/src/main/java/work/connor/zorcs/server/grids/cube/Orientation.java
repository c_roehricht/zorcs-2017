package work.connor.zorcs.server.grids.cube;

import org.json.JSONObject;
import work.connor.zorcs.server.utility.IntMatrix3;

import java.util.Objects;

public class Orientation
        implements work.connor.zorcs.server.grids.common.Orientation<Cube,CellCoord,GapCoord,FCoord,Direction,Orientation>,
                   SpatialValue<Orientation> {
    public static final Orientation NO_ROTATION      = new Orientation(IntMatrix3.identity);
    public static final Orientation RIGHT            = new Orientation(new IntMatrix3( 0,-1, 0,
                                                                                       1, 0, 0,
                                                                                       0, 0, 1));
    public static final Orientation LEFT             = new Orientation(new IntMatrix3( 0, 1, 0,
                                                                                      -1, 0, 0,
                                                                                       0, 0, 1));
    public static final Orientation BEHIND           = new Orientation(new IntMatrix3(-1, 0, 0,
                                                                                       0,-1, 0,
                                                                                       0, 0, 1));
    public static final Orientation CLOCKWISE        = new Orientation(new IntMatrix3( 1, 0, 0,
                                                                                       0, 0, 1,
                                                                                       0,-1, 0));
    public static final Orientation COUNTERCLOCKWISE = new Orientation(new IntMatrix3( 1, 0, 0,
                                                                                       0, 0,-1,
                                                                                       0, 1, 0));
    public static final Orientation UPSIDEDOWN       = new Orientation(new IntMatrix3( 1, 0, 0,
                                                                                       0,-1, 0,
                                                                                       0, 0,-1));
    public static final Orientation UP               = new Orientation(new IntMatrix3( 0, 0, 1,
                                                                                       0, 1, 0,
                                                                                      -1, 0, 0));
    public static final Orientation DOWN             = new Orientation(new IntMatrix3( 0, 0,-1,
                                                                                       0, 1, 0,
                                                                                       1, 0, 0));
    public static final Orientation LEFT_CLOCKWISE           = CLOCKWISE.rotate(LEFT);
    public static final Orientation LEFT_COUNTERCLOCKWISE    = COUNTERCLOCKWISE.rotate(LEFT);
    public static final Orientation LEFT_UPSIDEDOWN          = UPSIDEDOWN.rotate(LEFT);
    public static final Orientation RIGHT_CLOCKWISE          = CLOCKWISE.rotate(RIGHT);
    public static final Orientation RIGHT_COUNTERCLOCKWISE   = COUNTERCLOCKWISE.rotate(RIGHT);
    public static final Orientation RIGHT_UPSIDEDOWN         = UPSIDEDOWN.rotate(RIGHT);
    public static final Orientation BEHIND_CLOCKWISE         = CLOCKWISE.rotate(BEHIND);
    public static final Orientation BEHIND_COUNTERCLOCKWISE  = COUNTERCLOCKWISE.rotate(BEHIND);
    public static final Orientation BEHIND_UPSIDEDOWN        = UPSIDEDOWN.rotate(BEHIND);
    public static final Orientation UP_CLOCKWISE             = CLOCKWISE.rotate(UP);
    public static final Orientation UP_COUNTERCLOCKWISE      = COUNTERCLOCKWISE.rotate(UP);
    public static final Orientation UP_UPSIDEDOWN            = UPSIDEDOWN.rotate(UP);
    public static final Orientation DOWN_CLOCKWISE           = CLOCKWISE.rotate(DOWN);
    public static final Orientation DOWN_COUNTERCLOCKWISE    = COUNTERCLOCKWISE.rotate(DOWN);
    public static final Orientation DOWN_UPSIDEDOWN          = UPSIDEDOWN.rotate(DOWN);

    public final IntMatrix3 matrix;
    public Orientation(IntMatrix3 matrix) {this.matrix = matrix;} //TODO invariant
    @Override public Orientation rotate(Orientation o) {return new Orientation(o.matrix.multiply(matrix));}
    @Override public Orientation negate() {return new Orientation(matrix.transpose());}
    private static IntMatrix3 mirror = new IntMatrix3( 1, 0, 0,
                                                       0,-1, 0,
                                                       0, 0,-1);
    @Override public Orientation mirror() {return new Orientation(mirror.multiply(matrix).multiply(mirror));}
    @Override public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Orientation that = (Orientation) o;
        return Objects.equals(matrix, that.matrix);
    }
    @Override public int hashCode() {return Objects.hash(matrix);}
    @Override public JSONObject toRemote(JSONObject output) {
        output = work.connor.zorcs.server.grids.common.Orientation.super.toRemote(output);
        return output.put("matrix", matrix.toJSONArray());
    }
}
