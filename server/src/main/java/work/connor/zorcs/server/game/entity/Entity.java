package work.connor.zorcs.server.game.entity;

import work.connor.zorcs.server.game.component.Component;
import work.connor.zorcs.server.grid.general.Grid;

import java.util.Set;

public interface Entity<G extends Grid<G>> {
    //TODO "thing/creature"
    Set<Component<G>> getComponents();
    void addComponent(Component<G> component);
    void removeComponent(Component<G> component);
}
