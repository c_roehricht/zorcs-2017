package work.connor.zorcs.server.game.processor;

import work.connor.zorcs.server.grid.general.Grid;
import work.connor.zorcs.server.game.space.Space;

public interface Processor<G extends Grid<G>> {
    void process(Space<G> space);
    default void installAsProcessor(Space<G> space) {space.addProcessor(this);}
    default void uninstallAsEmitter(Space<G> space) {space.removeProcessor(this);}
}
