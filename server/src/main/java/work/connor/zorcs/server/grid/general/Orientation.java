package work.connor.zorcs.server.grid.general;

public interface Orientation<G extends Grid<G>,
                             O extends Orientation<G,O>>
        extends work.connor.zorcs.server.grid.absolute.Orientation<G,O>,
                work.connor.zorcs.server.grid.relative.Orientation<G,O>,
                SpatialValue<G,O> {
}
