package work.connor.zorcs.server.game.component;

import work.connor.zorcs.server.game.processor.Processor;
import work.connor.zorcs.server.game.space.Space;
import work.connor.zorcs.server.grid.general.Grid;

public interface ProcessorComponent<G extends Grid<G>> extends Component<G>,
                                                               Processor<G>,
                                                               Space.Based<G> {
    @Override default void componentInstaller() {getSpace().addProcessor(this);}
    @Override default void componentUninstaller() {getSpace().removeProcessor(this);}
}
