package work.connor.zorcs.server.game.emitter;

import work.connor.zorcs.server.game.space.Space;
import work.connor.zorcs.server.grid.general.Grid;

public interface Emitter<G extends Grid<G>> {
    void emitEvents(Space<G> space);
    default void installAsEmitter(Space<G> space) {space.addEmitter(this);}
    default void uninstallAsEmitter(Space<G> space) {space.removeEmitter(this);}
}
