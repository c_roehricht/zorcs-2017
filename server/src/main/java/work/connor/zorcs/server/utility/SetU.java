package work.connor.zorcs.server.utility;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class SetU {
    @SuppressWarnings("OptionalUsedAsFieldOrParameterType")
    public static <T> Set<T> ofOpt(Optional<? extends T> elem) {return elem.stream().collect(Collectors.toSet());}
    @SafeVarargs public static <T> Set<T> ofDupls(T... elems) {return new LinkedHashSet<>(List.of(elems));}
    public static <T> Set<T> union(Set<? extends T> s1, Set<? extends T> s2) {
        Set<T> result = new LinkedHashSet<>(s1);
        result.addAll(s2);
        return result;
    }
    public static <I, O> Set<O> map(Set<I> input, Function<? super I, ? extends O> mapper) {
        return input.stream().map(mapper).collect(Collectors.toSet());
    }
    public static <I, O> Set<O> flatMap(Set<I> input, Function<? super I, Set<? extends O>> mapper) {
        return input.stream().flatMap(i -> mapper.apply(i).stream()).collect(Collectors.toSet());
    }
    public static <I, O> Set<O> filterMap(Set<I> input, Function<? super I, ? extends O> mapper) {
        return StreamU.filterMap(input.stream(), mapper).collect(Collectors.toSet());
    }
    public static <U, D extends U> Set<D> filterDown(Set<U> us, Predicate<? super U> filter) {
        return StreamU.<U,D>down(us.stream().filter(filter)).collect(Collectors.toSet());
    }
}
