package work.connor.zorcs.server.grid.absolute;

public interface Coord<G extends Grid<G>,
                       C extends Coord<G,C>>
        extends work.connor.zorcs.server.grid.Coord<G,C>,
                SpatialValue<G,C> {
    @Override work.connor.zorcs.server.grid.relative.Coord<?,?> toRelative(Sys<G,?> sy);
}
