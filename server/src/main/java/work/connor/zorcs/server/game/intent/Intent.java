package work.connor.zorcs.server.game.intent;

import work.connor.zorcs.server.game.action.Action;
import work.connor.zorcs.server.game.action.ActionFailedException;
import work.connor.zorcs.server.game.action.LambdaAction;
import work.connor.zorcs.server.game.actor.Actor;
import work.connor.zorcs.server.game.space.Space;
import work.connor.zorcs.server.grid.general.Grid;

import java.util.function.Consumer;

public interface Intent<G extends Grid<G>> {
    Action<G> getAction();
    default boolean isTracked() {return false;}
    default void complete(ActionFailedException cause) {}

    static <G extends Grid<G>> Intent<G> lambda(Consumer<Space<G>> lambda) {
        return new SimpleIntent<>(new LambdaAction<>(lambda));
    }
    static <G extends Grid<G>> Intent<G> reschedule(Actor<G> actor, int delay) {
        return lambda((space) -> space.schedule(actor, delay));
    }
}
