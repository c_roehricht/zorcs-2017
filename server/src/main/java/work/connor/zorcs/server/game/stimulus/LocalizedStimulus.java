package work.connor.zorcs.server.game.stimulus;

import org.json.JSONObject;
import work.connor.zorcs.server.grid.relative.*;

public interface LocalizedStimulus<G extends Grid<G>> extends Stimulus<G> {
    LocationCoord<G,?> getCoord();
    void setCoord(LocationCoord<G,?> coord);
    @Override default JSONObject toRemote(JSONObject output) {
        output = Stimulus.super.toRemote(output);
        return put(output, "coordinate", getCoord());
    }
}
