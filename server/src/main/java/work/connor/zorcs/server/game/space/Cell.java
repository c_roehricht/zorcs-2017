package work.connor.zorcs.server.game.space;

import work.connor.zorcs.server.grid.general.Grid;
import work.connor.zorcs.server.grid.absolute.CellCoord;
import work.connor.zorcs.server.grid.absolute.LocationCoord;
import work.connor.zorcs.server.grid.absolute.Orientation;

import java.util.Set;

public interface Cell<G extends Grid<G>> extends Location<G> {
    CellCoord<G,?> getCellCoord();
    void setSpaceCoord(Space<G> space, CellCoord<G,?> coord);
    @Override default LocationCoord<G,?> getCoord() {return getCellCoord();}
    Set<Body<G>> getBodies();
    void addBody(Body<G> body, Orientation<G,?> o);
    void removeBody(Body<G> body);
    Orientation<G,?> getOrientation(Body<G> body);
    default void installAsCell(Space<G> space, CellCoord<G,?> coord) {
        setSpaceCoord(space, coord);
        space.addCell(this, coord);
    }
    default void uninstallAsCell() {
        getSpace().removeCell(this);
        setSpaceCoord(null, null);
    }

    interface Based<G extends Grid<G>> extends Location.Based<G> {
        Cell<G> getCell();
        @Override default Location<G> getLocation() {return getCell();}
    }
    interface Mapped<G extends Grid<G>> extends Based<G> {
        void setCell(Cell<G> cell);
    }
}
