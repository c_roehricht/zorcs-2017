package work.connor.zorcs.server.grids.quad;

public interface LocationCoord<LC extends LocationCoord<LC>>
        extends work.connor.zorcs.server.grids.quadhex.LocationCoord<Quad,LC,CellCoord,GapCoord,FCoord,Angle>,
                Coord<LC> {
}
