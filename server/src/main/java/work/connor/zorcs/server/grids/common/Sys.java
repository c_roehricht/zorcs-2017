package work.connor.zorcs.server.grids.common;

import java.util.Objects;

public abstract class Sys<G  extends CommonGrid<G,CC,GC,FC,D,O>,
                          Sy extends Sys<G,Sy,CC,GC,FC,D,O>,
                          CC extends CellCoord<G,CC,GC,FC,D,O>,
                          GC extends GapCoord<G,CC,GC,FC,D,O>,
                          FC extends FCoord<G,CC,GC,FC,D,O>,
                          D  extends Direction<G,CC,GC,FC,D,O>,
                          O  extends Orientation<G,CC,GC,FC,D,O>>
        implements work.connor.zorcs.server.grid.general.Sys<G,Sy>,
                   SpatialValue<G,Sy,CC,GC,FC,D,O> {
    public final CC cc;
    public final O o;
    protected Sys(CC cc, O o) {this.cc = cc; this.o = o;}
    @Override public CC cellCoord() {return cc;}
    @Override public O orientation() {return o;}
    @Override public Sy negate() {return concrete(g().system(cc.negate().rotate(o.negate()), o.negate()));}
    @Override public Sy rotate(O o2) {return concrete(g().system(cc.rotate(o2), o.rotate(o2)));}
    @Override public Sy translate(CC cc2) {return concrete(g().system(cc.translate(cc2), o.translate(cc2)));}
    @Override public Sy mirror() {return concrete(g().system(cc.mirror(), o.mirror()));}
    @Override public Sy toRelative(work.connor.zorcs.server.grid.absolute.Sys<G, ?> sy) {
        return work.connor.zorcs.server.grid.general.Sys.super.toRelative(sy);
    }
    @Override public boolean equals(Object o1) {
        if (this == o1) return true;
        if (o1 == null || getClass() != o1.getClass()) return false;
        Sys<?, ?, ?, ?, ?, ?, ?> sys = (Sys<?, ?, ?, ?, ?, ?, ?>) o1;
        return Objects.equals(cc, sys.cc) &&
                Objects.equals(o, sys.o);
    }
    @Override public int hashCode() {return Objects.hash(cc, o);}
}
