﻿using System;
using Newtonsoft.Json.Linq;
using UnityEngine;
using Zorcs.Client.Grid;

namespace Zorcs.Client
{
    public abstract class Stimuli<G> : GridBased<G> where G : ZGrid<G>, new()
    {
        public Stimulus<G> Stimulus(JToken input)
        {
            string stimulus = input["stimulus"].ToObject<string>();
            switch (stimulus)
            {
                case "visual" : return new VisualStimulus<G>(input);
                case "time"   : return new TimeStimulus<G>(input);
                default       : return null;      
            }
        }
        public abstract G g();
    }
}
