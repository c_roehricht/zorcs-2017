﻿using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Collections.Concurrent;
using Newtonsoft.Json.Linq;
using UnityEngine;
using Zorcs.Client;
using Zorcs.Client.Grid;

public abstract class Brain<G> : MonoBehaviour where G : ZGrid<G>, new() {
    // TODO add queues for read and write
    // TODO add poll method that returns a stimulus if possible
    // TODO add coroutine that polls and processes stimuli

    private BlockingCollection<JObject> incoming =
        new BlockingCollection<JObject>(new ConcurrentQueue<JObject>());
    private BlockingCollection<JObject> outgoing =
        new BlockingCollection<JObject>(new ConcurrentQueue<JObject>());
    private TcpClient tcpClient;
    private Thread receiver;
    private Thread sender;

    public MapperMapper visuals;
    public Transform origin;
    public string host;
    public int port;

	// Use this for initialization
	public virtual void Start () {
		
	}
	
	// Update is called once per frame
	public virtual void Update () {
		
	}

    public GameObject VisualPrototype(string type, string appearance) {return visuals[type][appearance];}
    /*
    private Stimulus<G> PollStimulus()
    {
        JObject json = null;
        incoming.TryTake(out json);
        if (json == null) return null;
        return stimulus; //TODO
    }

    private void SetupAndReceive()
    {
        tcpClient = new TcpClient(host, port);
        sender = new Thread(Send);
        sender.Start();
        Receive();
    }

    private void Send() // TODO write a JSON
    {
        byte[] buffer = new byte[10000];
        byte[] lengthData = new byte[4];
        while (true)
        {
            JObject json = outgoing.Take();
            string s = json.ToString();
            int bufferSize = Encoding.ASCII.GetBytes(s, 0, s.Length, buffer, 0);
            PutBigInt32(lengthData, 0, bufferSize);
            tcpClient.GetStream().Write(lengthData, 0, 4);
            tcpClient.GetStream().Write(buffer, 0, bufferSize);
        }
    }

    private void Receive() // TODO read a JSON
    {
        byte[] buffer = new byte[100000];
        while (true)
        {
            int bufferSize = 0;
            while (bufferSize < 4)
                bufferSize += tcpClient.GetStream().Read(buffer, bufferSize, 4 - bufferSize);
            int length = TakeBigInt32(buffer, 0);
            bufferSize = 0;
            while (bufferSize < length)
                bufferSize += tcpClient.GetStream().Read(buffer, bufferSize, length - bufferSize);
            string json = Encoding.ASCII.GetString(buffer, 0, bufferSize);
            GenerateStimuli(json);
        }
    }
    */
}
