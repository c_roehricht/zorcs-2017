﻿using System;
using UnityEngine;
using Zorcs.Client.Grid;

namespace Zorcs.Client
{
    public abstract class Stimulus<G> : GridType<G> where G : ZGrid<G>, new()
    {
        public virtual void Enter(Brain<G> brain) { }
    }
}
