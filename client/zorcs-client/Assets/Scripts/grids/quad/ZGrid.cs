﻿using UnityEngine;
using Zorcs.Client.Grid.QuadHex;

namespace Zorcs.Client.Grid.Quad
{
    public class Quad : QuadHex<Quad,CellCoord,GapCoord,Angle>
    {
        public override CellCoord CellCoord(int a, int b) {return new CellCoord(a, b);}
        public override GapCoord GapCoord(CellCoord cc) {return new GapCoord(cc);}
        public override Angle Angle(int r) {return new Angle(r);}
        public override int Circle() {return 4;}
    }

    public class CellCoord : CellCoord<Quad,CellCoord,GapCoord,Angle>
    {
        public CellCoord(int a, int b) : base(a, b) {}
        public override CellCoord RotateOne() {return g().CellCoord(-b, a);}
        public override Vector3 Locate() {return new Vector3(b, 0, a);}
    }

    public class GapCoord : GapCoord<Quad,CellCoord,GapCoord,Angle>
    {
        public GapCoord(CellCoord cc) : base(cc) {}
    }

    public class Angle : Angle<Quad,CellCoord,GapCoord,Angle>
    {
        public Angle(int r) : base(r) {}
    }
}
