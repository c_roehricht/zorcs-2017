﻿using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using UnityEngine;
using Zorcs.Client;
using Zorcs.Client.Grid;
using Zorcs.Client.Grid.Quad;

public class QuadBrain : Brain<Quad>
{
    // Use this for initialization
    public override void Start()
    {
        JToken input1 = JToken.Parse("{'stimulus':'visual','coordinate':{'a':2,'b':5},'type':'cell','appearance':'stone'}");
        JToken input2 = JToken.Parse("{'stimulus':'visual','coordinate':{'cc': {'a':3,'b':4}},'type':'gap','appearance':'stone'}");
        VisualStimulus<Quad> stimulus1 = new VisualStimulus<Quad>(input1);
        VisualStimulus<Quad> stimulus2 = new VisualStimulus<Quad>(input2);
        stimulus1.Enter(this);
        stimulus2.Enter(this);
    }

    // Update is called once per frame
    public override void Update()
    {

    }
}
