﻿using Newtonsoft.Json.Linq;
using UnityEngine;
using Zorcs.Client.Grid.Base;

namespace Zorcs.Client.Grid.Cube
{
    public class Cube : Base<Cube,CellCoord,GapCoord,Orientation,Direction>
    {
        public CellCoord CellCoord(int a, int b, int c) {return new CellCoord(a, b, c);}
        public override GapCoord GapCoord(CellCoord cc) {return new GapCoord(cc);}
        public Orientation Orientation(IntMatrix3 matrix) {return new Orientation(matrix);}
        public Direction Direction(CellCoord cc) {return new Direction(cc);}
        public override CellCoord<Cube> OriginCellCoord() {return CellCoord(0, 0, 0);}
        public override Orientation<Cube> OriginOrientation() {return Orientation(IntMatrix3.identity);}
        public override CellCoord<Cube> CellCoord(JToken input)
        {
            if (input["a"] == null || input["b"] == null || input["c"] == null) return null;
            return CellCoord(input["a"].ToObject<int>(),
                             input["b"].ToObject<int>(),
                             input["c"].ToObject<int>());
        }
        public override Orientation<Cube> Orientation(JToken input)
        {
            if (input["matrix"] == null) return null;
            return Orientation(new IntMatrix3(input["matrix"] as JArray));
        }
        public override Direction<Cube> Direction(JToken input)
        {
            if (input["cc"] == null) return null;
            CellCoord cc = (CellCoord) CellCoord(input["cc"]);
            return cc != null ? Direction(cc) : null;
        }
    }

    public class CellCoord : CellCoord<Cube,CellCoord,GapCoord,Orientation,Direction>
    {
        public int a, b, c;
        public CellCoord(int a, int b, int c) {this.a = a; this.b = b; this.c = c;}
        public override CellCoord Translate(CellCoord cc) {return g().CellCoord(a + cc.a, b + cc.b, c + cc.c);}
        public override CellCoord Rotate(Orientation o)
        {
            int[] vec = o.matrix.Multiply(a, b, c);
            return new CellCoord(vec[0], vec[1], vec[2]);
        }
        public override Vector3 Locate() {return new Vector3(a, c, b);}
    }

    public class GapCoord : GapCoord<Cube,CellCoord,GapCoord,Orientation,Direction>
    {
        public GapCoord(CellCoord cc) : base(cc) {}
    }

    public class Orientation : GridType<Cube>, Orientation<Cube,CellCoord,GapCoord,Orientation,Direction>
    {
        public IntMatrix3 matrix;
        public Orientation(IntMatrix3 matrix) {this.matrix = matrix;}
        public Orientation<Cube> Rotate(Orientation<Cube> o) {return Rotate((Orientation) o);}
        public Orientation Rotate(Orientation o) {return g().Orientation(o.matrix.Multiply(matrix));}
        public Quaternion Locate() {return matrix.ToQuaternion();}
    }

    public class Direction : GridType<Cube>, Direction<Cube,CellCoord,GapCoord,Orientation,Direction>
    {
        public CellCoord cc;
        public Direction(CellCoord cc) {this.cc = cc;}
        public Direction<Cube> Rotate(Orientation<Cube> o) {return Rotate((Orientation) o);}
        public Direction Rotate(Orientation o) {return g().Direction(cc.Rotate(o));}
        public Vector3 Locate() {return cc.Locate();}
    }

    public class IntMatrix3 {
        public static readonly IntMatrix3 identity = new IntMatrix3( 1, 0, 0,
                                                                     0, 1, 0,
                                                                     0, 0, 1); 
        public int[,] values = new int[3,3];
        private IntMatrix3() {}
        public IntMatrix3(int a00, int a01, int a02,
                          int a10, int a11, int a12,
                          int a20, int a21, int a22)
        {
            values[0,0] = a00; values[0,1] = a01; values[0,2] = a02;
            values[1,0] = a10; values[1,1] = a11; values[1,2] = a12;
            values[2,0] = a20; values[2,1] = a21; values[2,2] = a22;
        }
        public IntMatrix3(JArray input) : this(
            input[0].ToObject<int>(), input[1].ToObject<int>(), input[2].ToObject<int>(),
            input[3].ToObject<int>(), input[4].ToObject<int>(), input[5].ToObject<int>(), 
            input[6].ToObject<int>(), input[7].ToObject<int>(), input[8].ToObject<int>()) {}
        public IntMatrix3 Multiply(IntMatrix3 other)
        {
            IntMatrix3 result = new IntMatrix3();
            for (int row = 0; row < 3; row++)
            {
                for (int col = 0; col < 3; col++)
                {
                    result.values[row,col] = 0;
                    for (int i = 0; i < 3; i++) result.values[row,col] += values[row,i] * other.values[i,col];
                }
            }
            return result;
        }

        public int[] Multiply(int v0, int v1, int v2)
        {
            int[] result = new int[3];
            for (int i = 0; i < 3; i++) result[i] = values[i,0] * v0
                                                  + values[i,1] * v1
                                                  + values[i,2] * v2;
            return result;
        }

        public Quaternion ToQuaternion() {
            // Adapted from https://answers.unity.com/questions/11363/converting-matrix4x4-to-quaternion-vector3.html
            Quaternion q = new Quaternion //TODO fix: axes swapped (a -> z, b -> x, c -> y)
            {
                w = Mathf.Sqrt(Mathf.Max(0, 1 + values[0, 0] + values[1, 1] + values[2, 2])) / 2,
                x = Mathf.Sqrt(Mathf.Max(0, 1 + values[0, 0] - values[1, 1] - values[2, 2])) / 2,
                y = Mathf.Sqrt(Mathf.Max(0, 1 - values[0, 0] + values[1, 1] - values[2, 2])) / 2,
                z = Mathf.Sqrt(Mathf.Max(0, 1 - values[0, 0] - values[1, 1] + values[2, 2])) / 2
            };
            q.x *= Mathf.Sign(q.x * (values[2, 1] - values[1, 2]));
            q.y *= Mathf.Sign(q.y * (values[0, 2] - values[2, 0]));
            q.z *= Mathf.Sign(q.z * (values[1, 0] - values[0, 1]));
            return q;
        }
    }
}
