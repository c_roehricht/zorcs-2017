﻿using Newtonsoft.Json.Linq;
using Zorcs.Client.Grid.Flat;

namespace Zorcs.Client.Grid.QuadHex
{
    public abstract class QuadHex<G,CC,GC,A> : Flat<G,CC,GC,A>
        where G  : QuadHex<G,CC,GC,A>, new()
        where CC : CellCoord<G,CC,GC,A>
        where GC : GapCoord<G,CC,GC,A>
        where A  : Angle<G,CC,GC,A>
    {
        public abstract CC CellCoord(int a, int b);
        public override CellCoord<G> OriginCellCoord() {return CellCoord(0, 0);}
        public override CC UnitForward() {return CellCoord(1, 0);}
        public override CellCoord<G> CellCoord(JToken input)
        {
            if (input["a"] == null || input["b"] == null) return null;
            return CellCoord(input["a"].ToObject<int>(),
                             input["b"].ToObject<int>());
        }
    }

    public abstract class CellCoord<G,CC,GC,A> : Flat.CellCoord<G,CC,GC,A>
        where G  : QuadHex<G,CC,GC,A>, new()
        where CC : CellCoord<G,CC,GC,A>
        where GC : GapCoord<G,CC,GC,A>
        where A  : Angle<G,CC,GC,A>
    {
        public int a, b;
        protected CellCoord(int a, int b) {this.a = a; this.b = b;}
        public override CC Translate(CC cc) {return g().CellCoord(a + cc.a, b + cc.b);}
    }

    public abstract class GapCoord<G,CC,GC,A> : Flat.GapCoord<G,CC,GC,A>
        where G  : QuadHex<G,CC,GC,A>, new()
        where CC : CellCoord<G,CC,GC,A>
        where GC : GapCoord<G,CC,GC,A>
        where A  : Angle<G,CC,GC,A>
    {
        protected GapCoord(CC cc) : base(cc) { }
    }

    public abstract class Angle<G,CC,GC,A> : Flat.Angle<G,CC,GC,A>
        where G  : QuadHex<G,CC,GC,A>, new()
        where CC : CellCoord<G,CC,GC,A>
        where GC : GapCoord<G,CC,GC,A>
        where A  : Angle<G,CC,GC,A>
    {
        protected Angle(int r) : base(r) {}
    }
}
