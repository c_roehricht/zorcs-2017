﻿using UnityEngine;
using Zorcs.Client.Grid.QuadHex;

namespace Zorcs.Client.Grid.Hex
{
    public class Hex : QuadHex<Hex,CellCoord,GapCoord,Angle>
    {
        public override CellCoord CellCoord(int a, int b) {return new CellCoord(a, b);}
        public override GapCoord GapCoord(CellCoord cc) {return new GapCoord(cc);}
        public override Angle Angle(int r) {return new Angle(r);}
        public override int Circle() {return 6;}
    }

    public class CellCoord : CellCoord<Hex,CellCoord,GapCoord,Angle>
    {
        public CellCoord(int a, int b) : base(a, b) {}
        public override CellCoord RotateOne() {return g().CellCoord(-b, a + b);}
        public override Vector3 Locate() {return new Vector3(b * Mathf.Sqrt(3) / 2f, 0, a + b / 2f);}
    }

    public class GapCoord : GapCoord<Hex,CellCoord,GapCoord,Angle>
    {
        public GapCoord(CellCoord cc) : base(cc) {}
    }

    public class Angle : Angle<Hex,CellCoord,GapCoord,Angle>
    {
        public Angle(int r) : base(r) {}
    }
}
