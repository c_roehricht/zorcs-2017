﻿using System;
using Newtonsoft.Json.Linq;
using UnityEngine;
using Zorcs.Client.Grid.Base;

namespace Zorcs.Client.Grid.Flat
{
    public abstract class Flat<G,CC,GC,A> : Base<G,CC,GC,A,A>
        where G  : Flat<G,CC,GC,A>, new()
        where CC : CellCoord<G,CC,GC,A>
        where GC : GapCoord<G,CC,GC,A>
        where A  : Angle<G,CC,GC,A>
    {
        public abstract A Angle(int r);
        public abstract int Circle();
        public abstract CC UnitForward();
        public override Orientation<G> OriginOrientation() {return Angle(0);}
        public override Orientation<G> Orientation(JToken input) {return Angle(input);}
        public override Direction<G> Direction(JToken input) {return Angle(input);}
        public A Angle(JToken input)
        {
            if (input["r"] == null) return null;
            return Angle(input["r"].ToObject<int>());
        }
    }

    public abstract class CellCoord<G,CC,GC,A> : CellCoord<G,CC,GC,A,A>
        where G  : Flat<G,CC,GC,A>, new()
        where CC : CellCoord<G,CC,GC,A>
        where GC : GapCoord<G,CC,GC,A>
        where A  : Angle<G,CC,GC,A>
    {
        public override CC Rotate(A a) {return a.r == 0 ? (CC) this : RotateOne().Rotate(g().Angle(a.r - 1));}
        public abstract CC RotateOne();
    }

    public abstract class GapCoord<G,CC,GC,A> : GapCoord<G,CC,GC,A,A>
        where G  : Flat<G,CC,GC,A>, new()
        where CC : CellCoord<G,CC,GC,A>
        where GC : GapCoord<G,CC,GC,A>
        where A  : Angle<G,CC,GC,A>
    {
        protected GapCoord(CC cc) : base(cc) { }
    }

    public abstract class Angle<G,CC,GC,A> : GridType<G>, Orientation<G,CC,GC,A,A>, Direction<G,CC,GC,A,A> 
        where G  : Flat<G,CC,GC,A>, new()
        where CC : CellCoord<G,CC,GC,A>
        where GC : GapCoord<G,CC,GC,A>
        where A  : Angle<G,CC,GC,A>
    {
        static int Mod(int a, int b) {return ((a %= b) < 0) ? a + b : a;}
        public int r;
        protected Angle(int r) {this.r = Mod(r, g().Circle());}
        public Quaternion Locate() {return Quaternion.Euler(0, 360f * r / g().Circle(), 0);}
        Orientation<G> Orientation<G>.Rotate(Orientation<G> o) {return Rotate((A) o);}
        Direction<G> Direction<G>.Rotate(Orientation<G> o) {return Rotate((A) o);}
        public A Rotate(A a) {return g().Angle(r + a.r);}
        public A Concrete() {return (A) this;}
        Vector3 Direction<G>.Locate() {return g().UnitForward().Rotate(this).Locate();}
    }
}
