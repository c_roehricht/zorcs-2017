﻿using Newtonsoft.Json.Linq;
using UnityEngine;

namespace Zorcs.Client.Grid.Base
{
    public abstract class Base<G,CC,GC,O,D> : ZGrid<G>
        where G  : Base<G,CC,GC,O,D>, new()
        where CC : CellCoord<G,CC,GC,O,D>
        where GC : GapCoord<G,CC,GC,O,D>
        where O  : Orientation<G,CC,GC,O,D>
        where D  : Direction<G,CC,GC,O,D>
    {
        public abstract GC GapCoord(CC cc);
        public override GapCoord<G> GapCoord(JToken input)
        {
            if (input["cc"] == null) return null;
            CC cc = (CC) CellCoord(input["cc"]);
            return cc != null ? GapCoord(cc) : null;
        }
    }

    public abstract class CellCoord<G,CC,GC,O,D> : GridType<G>, CellCoord<G>
        where G  : Base<G,CC,GC,O,D>, new()
        where CC : CellCoord<G,CC,GC,O,D>
        where GC : GapCoord<G,CC,GC,O,D>
        where O  : Orientation<G,CC,GC,O,D>
        where D  : Direction<G,CC,GC,O,D>
    {
        public CellCoord<G> Translate(CellCoord<G> cc) {return Translate((CC) cc);}
        public abstract CC Translate(CC cc);
        public CellCoord<G> Rotate(Orientation<G> o) {return Rotate((O) o);}
        public abstract CC Rotate(O o);
        public abstract Vector3 Locate();
        public CC Concrete() {return (CC) this;}
    }

    public abstract class GapCoord<G,CC,GC,O,D> : GridType<G>, GapCoord<G>
        where G  : Base<G,CC,GC,O,D>, new()
        where CC : CellCoord<G,CC,GC,O,D>
        where GC : GapCoord<G,CC,GC,O,D>
        where O  : Orientation<G,CC,GC,O,D>
        where D  : Direction<G,CC,GC,O,D>
    {
        public CC cc;
        public GapCoord(CC cc) {this.cc = cc;}
        public GapCoord<G> Translate(CellCoord<G> cc) { return Translate((CC)cc); }
        public GC Translate(CC cc) {return g().GapCoord(this.cc.Translate(cc));}
        public GapCoord<G> Rotate(Orientation<G> o) { return Rotate((O)o); }
        public GC Rotate(O o) {return g().GapCoord(cc.Rotate(o));}
        public Vector3 Locate() {return cc.Locate() / 2f;}
        public GC Concrete() {return (GC) this;}
    }

    public interface Orientation<G,CC,GC,O,D> : Orientation<G>
        where G  : Base<G,CC,GC,O,D>, new()
        where CC : CellCoord<G,CC,GC,O,D>
        where GC : GapCoord<G,CC,GC,O,D>
        where O  : Orientation<G,CC,GC,O,D>
        where D  : Direction<G,CC,GC,O,D>
    {
        O Rotate(O o);
    }

    public interface Direction<G,CC,GC,O,D> : Direction<G>
        where G  : Base<G,CC,GC,O,D>, new()
        where CC : CellCoord<G,CC,GC,O,D>
        where GC : GapCoord<G,CC,GC,O,D>
        where O  : Orientation<G,CC,GC,O,D>
        where D  : Direction<G,CC,GC,O,D>
    {
        D Rotate(O o);
    }
}
