﻿using Newtonsoft.Json.Linq;
using UnityEngine;
using Zorcs.Client.Grid.Flat;

namespace Zorcs.Client.Grid.Tape
{
    public class Tape : Flat<Tape,CellCoord,GapCoord,Angle>
    {
        public CellCoord CellCoord(int a) {return new CellCoord(a);}
        public override GapCoord GapCoord(CellCoord cc) {return new GapCoord(cc);}
        public override Angle Angle(int r) {return new Angle(r);}
        public override int Circle() {return 2;}
        public override CellCoord<Tape> OriginCellCoord() {return CellCoord(0);}
        public override CellCoord UnitForward() {return CellCoord(1);}
        public override CellCoord<Tape> CellCoord(JToken input)
        {
            if (input["a"] == null) return null;
            return CellCoord(input["a"].ToObject<int>());
        }
    }

    public class CellCoord : CellCoord<Tape,CellCoord,GapCoord,Angle>
    {
        public int a;
        public CellCoord(int a) {this.a = a;}
        public override CellCoord Translate(CellCoord cc) {return g().CellCoord(a + cc.a);}
        public override CellCoord RotateOne() {return g().CellCoord(-a);}
        public override Vector3 Locate() {return new Vector3(0, 0, a);}
    }

    public class GapCoord : GapCoord<Tape,CellCoord,GapCoord,Angle>
    {
        public GapCoord(CellCoord cc) : base(cc) {}
    }

    public class Angle : Angle<Tape,CellCoord,GapCoord,Angle>
    {
        public Angle(int r) : base(r) {}
    }
}
