﻿using Newtonsoft.Json.Linq;
using UnityEngine;

namespace Zorcs.Client.Grid
{
    public abstract class ZGrid<G> where G : ZGrid<G>
    {
        public abstract CellCoord<G> OriginCellCoord();
        public abstract Orientation<G> OriginOrientation();
        public Coord<G> Coord(JToken input) {return CellCoord(input) ?? (Coord<G>) GapCoord(input);}
        public abstract CellCoord<G> CellCoord(JToken input);
        public abstract GapCoord<G> GapCoord(JToken input);
        public abstract Orientation<G> Orientation(JToken input);
        public abstract Direction<G> Direction(JToken input);
    }

    public interface GridBased<G> where G : ZGrid<G>
    {
        G g();
    }

    public abstract class GridType<G> : GridBased<G> where G : ZGrid<G>, new()
    {
        public G g() {return new G();}
    }

    public interface Coord<G> : GridBased<G> where G : ZGrid<G>
    {
        Vector3 Locate();
    }

    public interface CellCoord<G> : Coord<G> where G : ZGrid<G>
    {
        CellCoord<G> Translate(CellCoord<G> cc);
        CellCoord<G> Rotate(Orientation<G> o);
    }

    public interface GapCoord<G> : Coord<G> where G : ZGrid<G>
    {
        GapCoord<G> Translate(CellCoord<G> cc);
        GapCoord<G> Rotate(Orientation<G> o);
    }

    public interface Orientation<G> : GridBased<G> where G : ZGrid<G>
    {
        Quaternion Locate();
        Orientation<G> Rotate(Orientation<G> o);
    }

    public interface Direction<G> : GridBased<G> where G : ZGrid<G>
    {
        Vector3 Locate();
        Direction<G> Rotate(Orientation<G> o);
    }
}
