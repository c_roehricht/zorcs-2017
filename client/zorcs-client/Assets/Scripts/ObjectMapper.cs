﻿using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class ObjectMapper : MonoBehaviour
{
    [System.Serializable]
    public class Entry
    {
        public string appearance;
        public GameObject prototype;
    }
    public GameObject unknown;
    public Entry[] map;

    private Dictionary<string, GameObject> dict;
    void Awake() { dict = map.ToDictionary(e => e.appearance, e => e.prototype); }

    public GameObject this[string key]
    {
        get
        {
            if (key == null) return unknown;
            try {return dict[key];}
            catch (KeyNotFoundException) {return unknown;}
        }
    }
}
