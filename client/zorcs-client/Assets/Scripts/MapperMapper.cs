﻿using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class MapperMapper : MonoBehaviour
{
    [System.Serializable]
    public class Entry
    {
        public string appearance;
        public ObjectMapper objectMapper;
    }
    public ObjectMapper unknown;
    public Entry[] map;

    private Dictionary<string, ObjectMapper> dict;
    void Awake() { dict = map.ToDictionary(e => e.appearance, e => e.objectMapper); }

    public ObjectMapper this[string key]
    {
        get
        {
            if (key == null) return unknown;
            try { return dict[key]; }
            catch (KeyNotFoundException) { return unknown; }
        }
    }
}
