﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using UnityEngine;
using Zorcs.Client.Grid;

namespace Zorcs.Client
{
    public class VisualStimulus<G> : Stimulus<G> where G : ZGrid<G>, new()
    {
        public Coord<G> coordinate;
        public Orientation<G> orientation;
        public string type, appearance;
        public Dictionary<string,string> details;

        public GameObject gameObject;

        public VisualStimulus(JToken input)
        {
            if (input["coordinate"] == null) throw new ArgumentException();
            coordinate  = g().Coord(input["coordinate"]);
            if (input["orientation"] != null) orientation = g().Orientation(input["orientation"]);
            else orientation = g().OriginOrientation();
            if (input["type"      ] != null) type       = input["type"      ].ToObject<string>();
            if (input["appearance"] != null) appearance = input["appearance"].ToObject<string>();
            if (input["details"   ] != null) details    = input["details"   ].ToObject<Dictionary<string,string>>();
        }

        public override void Enter(Brain<G> brain)
        {
            base.Enter(brain);
            GameObject prototype = brain.VisualPrototype(type, appearance);
            gameObject = UnityEngine.Object.Instantiate(prototype.gameObject, brain.origin);
            gameObject.transform.localPosition = coordinate.Locate();
            gameObject.transform.localRotation = orientation.Locate();
        }
    }
}
