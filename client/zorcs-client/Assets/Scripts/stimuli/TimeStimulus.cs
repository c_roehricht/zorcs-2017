﻿using System;
using Newtonsoft.Json.Linq;
using UnityEngine;
using Zorcs.Client.Grid;

namespace Zorcs.Client
{
    public class TimeStimulus<G> : Stimulus<G> where G : ZGrid<G>, new()
    {
        public int ticks;
        public TimeStimulus(JToken input)
        {
            ticks = input["ticks"].ToObject<int>(); 
        }
    }
}
